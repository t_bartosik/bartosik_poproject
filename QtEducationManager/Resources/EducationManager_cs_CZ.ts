<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../Forms/aboutwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../Forms/aboutwindow.ui" line="81"/>
        <source>Education Manager</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Sources/aboutwindow.cpp" line="20"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../Sources/aboutwindow.cpp" line="30"/>
        <source>Education Manager ©2021, Tomáš Bartošík, TBU in Zlin</source>
        <translation>Education Manager ©2021, Tomáš Bartošík, UTB ve Zlíně</translation>
    </message>
</context>
<context>
    <name>DialogWindow</name>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="34"/>
        <source>Select course and employee for assignment</source>
        <translation>Vyberte kurz a zaměstnance pro přiřazení</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="54"/>
        <location filename="../Forms/dialogwindow.ui" line="120"/>
        <source>Clear selection</source>
        <translation>Zrušit výběr</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="100"/>
        <source>Select group(s) for assignment</source>
        <translation>Vyberte skupiny pro přiřazení</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="44"/>
        <source>Description: L = lecture, P = practice, S = seminar, A = atelier</source>
        <translation>Legenda: P = přednáška, C = cvičení, S = seminář, A = ateliér</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="46"/>
        <source>Confirm assignment</source>
        <translation>Potvrdit výběr</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="47"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="62"/>
        <source>%1) %2, %3. year (%4, %5)
    summer semester students: %6, winter semester students: %7</source>
        <translation>%1) %2, %3. ročník (%4, %5)
    studentů letního semestru: %6, studentů zimního semestru: %7</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="95"/>
        <source>%1) %2 (%3)
    course ending: %4 (credits: %5)
    language: %6, scheduled weeks: %7
    semester: %8
    scheduled hours  ~  L: %9    P: %10    S: %11    A: %12
    course limits  ~  L: %13    P: %14    S: %15    A: %16</source>
        <translation>%1) %2 (%3)
    ukončení kurzu: %4 (kredity: %5)
    jazyk: %6, plánované týdny: %7
    semestr: %8
    plánované hodiny  ~  P: %9    C: %10    S: %11    A: %12
    limity kurzu  ~  P: %13    C: %14    S: %15    A: %16</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="135"/>
        <source>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6</source>
        <translation>%1) %2 (%3)
    e-mail: %4
    kancelář: %5    doktorand: %6</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="180"/>
        <source>Assignment error</source>
        <translation>Chyba přiřazení</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="206"/>
        <source>Both a course and an employee have to be selected or none at all.</source>
        <translation>Pro přiřazení musí být vybrán kurz i zaměstnanec nebo žádný z nich.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Forms/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="35"/>
        <source>Work label list</source>
        <translation>Seznam pracovních štítků</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="73"/>
        <source>Automatically assign work labels</source>
        <translation>Automaticky přiřadit pracovní štítky</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="80"/>
        <source>Assign selected work label</source>
        <translation>Přiřadit vybraný pracovní štítek</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="96"/>
        <location filename="../Sources/mainwindow.cpp" line="1881"/>
        <source>Add new work label</source>
        <translation>Přidat pracovní štítek</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="103"/>
        <source>Remove selected work label(s)</source>
        <translation>Odstranit vybrané štítky</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="131"/>
        <source>Student group list</source>
        <translation>Seznam skupin studentů</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="148"/>
        <location filename="../Sources/mainwindow.cpp" line="1922"/>
        <source>Add new group</source>
        <translation>Přidat novou skupinu</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="155"/>
        <location filename="../Sources/mainwindow.cpp" line="1924"/>
        <source>Update selected group</source>
        <translation>Upravit vybranou skupinu</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="162"/>
        <source>Remove selected group(s)</source>
        <translation>Odstranit vybrané skupiny</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="190"/>
        <source>Employee list</source>
        <translation>Seznam zaměstnanců</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="212"/>
        <source>Calculate work points</source>
        <translation>Spočítat pracovní body</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="228"/>
        <location filename="../Sources/mainwindow.cpp" line="1850"/>
        <source>Add new employee</source>
        <translation>Přidat zaměstnance</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="235"/>
        <source>Remove selected employee(s)</source>
        <translation>Odstranit vybrané zaměstnance</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="263"/>
        <source>Course list</source>
        <translation>Seznam kurzů</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="297"/>
        <source>Manage student groups</source>
        <translation>Spravovat studentské skupiny</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="313"/>
        <location filename="../Sources/mainwindow.cpp" line="1818"/>
        <source>Add new course</source>
        <translation>Přidat kurz</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="320"/>
        <source>Remove selected course(s)</source>
        <translation>Odstranit vybrané kurzy</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="363"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="377"/>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <location filename="../Sources/mainwindow.cpp" line="130"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="384"/>
        <source>Data Source</source>
        <translation>Datový Zdroj</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="396"/>
        <source>Quit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="404"/>
        <source>Student groups</source>
        <translation>Studentské skupiny</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="412"/>
        <source>Employees</source>
        <translation>Zaměstnanci</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="420"/>
        <source>Courses</source>
        <translation>Kurzy</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="428"/>
        <source>Work labels</source>
        <translation>Pracovní štítky</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="439"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="447"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="455"/>
        <source>XML files</source>
        <translation>Soubory XML</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="463"/>
        <source>Local database</source>
        <translation>Místní databáze</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="468"/>
        <source>Notification center</source>
        <translation>Centrum upozornění</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="473"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="65"/>
        <source>Settings file error</source>
        <translation>Chyba souboru nastavení</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="66"/>
        <source>Work point settings do not match required format</source>
        <translation>Nastavení pracovních bodů neodpovídá očekávanému formátu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="87"/>
        <source>Education Manager</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Course
shortcut</source>
        <translation>Zkratka
kurzu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Course name</source>
        <translation>Název kurzu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Lect.
limit</source>
        <translation>Limit
př.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Prac.
limit</source>
        <translation>Limit
cv.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Sem.
limit</source>
        <translation>Limit
sem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>At.
limit</source>
        <translation>Limit
at.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Lecture
hours</source>
        <translation>Hodin
př.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Practice
hours</source>
        <translation>Hodin
cv.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Seminar
hours</source>
        <translation>Hodin
sem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Atelier
hours</source>
        <translation>Hodin
at.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Weeks</source>
        <translation>Týdny</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Credits</source>
        <translation>Kredity</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Semester</source>
        <translation>Semestr</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Ending</source>
        <translation>Ukončení</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Name with titles</source>
        <translation>Jméno s tituly</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Function</source>
        <translation>Funkce</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Private
phone number</source>
        <translation>Soukromé
tel. číslo</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Is doctoral?</source>
        <translation>Je doktorand?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Workroom</source>
        <translation>Kancelář</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Workroom
phone number</source>
        <translation>Kancelář
tel. číslo</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Work label name</source>
        <translation>Název štítku</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Scheduled
weeks</source>
        <translation>Plánované
týdny</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Scheduled
hours (total)</source>
        <translation>Plánované
hodiny (celk.)</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Student count</source>
        <translation>Počet studentů</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Work points</source>
        <translation>Pracovní body</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Label type</source>
        <translation>Typ štítku</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Course type</source>
        <translation>Typ kurzu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="123"/>
        <source>all labels</source>
        <translation>všechny štítky</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="123"/>
        <source>fully unassigned labels</source>
        <translation>zcela nepřiřazené štítky</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="124"/>
        <source>assigned to course only</source>
        <translation>štítky přiřazené kurzům</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="124"/>
        <source>fully assigned labels</source>
        <translation>zcela přiřazené štítky</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Specialization</source>
        <translation>Specializace</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Year</source>
        <translation>Ročník</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Summer semester
students</source>
        <translation>Studentů letního
semestru</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Winter semester
students</source>
        <translation>Studentů zimního
semestru</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Study type</source>
        <translation>Typ studia</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Study form</source>
        <translation>Forma studia</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="283"/>
        <source>Entry error</source>
        <translation>Chyba na vstupu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="293"/>
        <source>summer semester</source>
        <translation>letní semestr</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="293"/>
        <source>winter semester</source>
        <translation>zimní semestr</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>credit test</source>
        <translation>zápočet</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <source>classified
 credit test</source>
        <translation>klasifikovaný
 zápočet</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>exam</source>
        <translation>zkouška</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <source>credit test
 &amp; exam</source>
        <translation>zápočet
 &amp; zkouška</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="310"/>
        <source>Confirm new course entry</source>
        <translation>Potvrdit nový záznam kurzu</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="389"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="389"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="400"/>
        <source>Confirm new employee entry</source>
        <translation>Potvrdit nový záznam zaměstnance</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>lecture</source>
        <translation>přednáška</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>practice</source>
        <translation>cvičení</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>seminar</source>
        <translation>seminář</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>atelier</source>
        <translation>ateliér</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>classified credit test</source>
        <translation>klasifikovaný zápočet</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>standard course</source>
        <translation>základní kurz</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>foreign language</source>
        <translation>v cizím jazyce</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>doctoral course</source>
        <translation>doktorský kurz</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="484"/>
        <source>Confirm new work label entry</source>
        <translation>Potvrdit nový záznam štítku</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="562"/>
        <source>full-time</source>
        <translation>prezenční</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="562"/>
        <source>combined</source>
        <translation>kombinovaná</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="577"/>
        <source>Confirm new group entry</source>
        <translation>Potvrdit nový záznam skupiny</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="659"/>
        <source>Course shortcut should contain at least one character.</source>
        <translation>Zkratka kurzu musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="663"/>
        <source>Course name should contain at least one character.</source>
        <translation>Název kurzu musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="667"/>
        <source>Lecture capacity should be defined as a non-negative integral number.</source>
        <translation>Kapacita přednášky musí být definována jako celé nezáporné číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="677"/>
        <source>Practice capacity should be defined as a non-negative integral number.</source>
        <translation>Kapacita cvičení musí být definována jako nezáporné celé číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="687"/>
        <source>Seminar capacity should be defined as a non-negative integral number.</source>
        <translation>Kapacita semináře musí být definována jako nezáporné celé číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="697"/>
        <source>Atelier capacity should be defined as a non-negative integral number.</source>
        <translation>Kapacita ateliéru musí být definována jako nezáporné celé číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="707"/>
        <source>Lecture hours are represented by a non-negative integral number.</source>
        <translation>Hodiny přednášek musí být reprezentovány nezáporným celým číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="717"/>
        <source>Practice hours are represented by a non-negative integral number.</source>
        <translation>Hodiny cvičení musí být reprezentovány nezáporným celým číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="727"/>
        <source>Seminar hours are represented by a non-negative integral number.</source>
        <translation>Hodiny semináře musí být reprezentovány nezáporným celým číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="737"/>
        <source>Atelier hours are represented by a non-negative integral number.</source>
        <translation>Hodiny ateliéru musí být reprezentovány nezáporným celým číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="747"/>
        <source>Number of course weeks should be defined as a natural number.</source>
        <translation>Počet týdnů kurzu musí být uveden jako přirozené číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="757"/>
        <source>Number of course credits should match a non-negative integral number.</source>
        <translation>Počet kreditů kurzu musí být uveden jako nezáporné celé číslo.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="769"/>
        <location filename="../Sources/mainwindow.cpp" line="805"/>
        <location filename="../Sources/mainwindow.cpp" line="848"/>
        <location filename="../Sources/mainwindow.cpp" line="891"/>
        <source>Some columns have not been filled.</source>
        <translation>Některé sloupce nebyly vyplněny.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="776"/>
        <source>Employee name should contain at least one character.</source>
        <translation>Jméno zaměstnance musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="780"/>
        <source>Employee function should contain at least one character.</source>
        <translation>Funkce zaměstnance musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="784"/>
        <source>E-mail format does not match requirements.</source>
        <translation>E-mailová adresa neodpovídá řádnému formátu.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="789"/>
        <source>Private phone number does not match required format.</source>
        <translation>Soukromé telefonní číslo neodpovídá platnému formátu.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="795"/>
        <source>Workroom label should contain at least one character.</source>
        <translation>Označení místnosti musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="799"/>
        <source>Workroom phone number does not match required format.</source>
        <translation>Telefonní číslo místnosti neodpovídá platnému formátu.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="812"/>
        <source>Work label name should be defined by at least one character.</source>
        <translation>Název pracovního štítku musí být definován alespoň jedním znakem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="816"/>
        <source>Number of scheduled weeks should be defined by a natural number.</source>
        <translation>Počet plánovaných týdnů musí být určen přirozeným číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="826"/>
        <source>Number of scheduled hours should be defined by a natural number.</source>
        <translation>Počet plánovaných hodin musí být definován přirozeným číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="836"/>
        <source>Number of students should be defined by a natural number.</source>
        <translation>Počet studentů musí být definován přirozeným číslem.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="855"/>
        <source>Specialization should contain at least one character.</source>
        <translation>Specializace musí obsahovat alespoň jeden znak.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="859"/>
        <source>A year is represented by an integral number in range from 1 to 4.</source>
        <translation>Rok musí být definován jako celé číslo v rozsahu 1 až 4.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="869"/>
        <source>Number of summer semester students does not match an integral number.</source>
        <translation>Počet studentů letního semestru neodpovídá celému číslu.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="879"/>
        <source>Number of winter semester students does not match an integral number.</source>
        <translation>Počet studentů zimního semestru neodpovídá celému číslu.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="894"/>
        <source>Unexpected behavior.</source>
        <translation>Neočekávané chování.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="903"/>
        <source>Removal error</source>
        <translation>Chyba v odstranění</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="904"/>
        <source>Select one or more rows in order to remove them.</source>
        <translation>Vyberte jeden nebo více řádků pro jejich odstranění.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="912"/>
        <location filename="../Sources/mainwindow.cpp" line="945"/>
        <location filename="../Sources/mainwindow.cpp" line="978"/>
        <location filename="../Sources/mainwindow.cpp" line="1018"/>
        <source>Removal confirmation</source>
        <translation>Potvrzení odstranění</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="913"/>
        <location filename="../Sources/mainwindow.cpp" line="946"/>
        <location filename="../Sources/mainwindow.cpp" line="979"/>
        <location filename="../Sources/mainwindow.cpp" line="1019"/>
        <source>Do you really want to remove selected row(s)?</source>
        <translation>Opravdu si přejete odstranit vybrané řádky?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1051"/>
        <location filename="../Sources/mainwindow.cpp" line="1195"/>
        <source>Selection error</source>
        <translation>Chyba výběru</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1052"/>
        <source>Select one row in order to update it.</source>
        <translation>Pro úpravu vyberte jeden řádek.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1076"/>
        <source>Confirm row changes</source>
        <translation>Potvrdit změny řádku</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1202"/>
        <source>Select one course in order to assign groups.</source>
        <translation>Pro přiřazení skupin vyberte jeden kurz.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1257"/>
        <source>Work labels creation</source>
        <translation>Tvorba pracovních štítků</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1258"/>
        <source>Would you like to automatically create work label(s)?</source>
        <translation>Přejete si automaticky vytvořit pracovní štítky?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1282"/>
        <source>Select one work label in order to assign its course and employee.</source>
        <translation>Vyberte jeden štítek pro přiřazení příslušného kurzu a zaměstnance.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1370"/>
        <source>Total work points</source>
        <translation>Celkové pracovní body</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1375"/>
        <source>Select one employee in order to calculate their work points.</source>
        <translation>Vyberte jednoho zaměstnance pro výpočet pracovních bodů.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1392"/>
        <source>Total work points of %1: %2</source>
        <translation>Celkové pracovní body zaměstnance %1: %2</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1692"/>
        <source>Assignment error</source>
        <translation>Chyba přiřazení</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1707"/>
        <source>There are no employees to be assigned.</source>
        <translation>Pro přiřazení nejsou k dispozici žádní zaměstnanci.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1742"/>
        <source>Work labels previously unassigned to employees were distributed automatically.</source>
        <translation>Dříve nepřiřazené pracovní štítky byly rozloženy mezi zaměstnance.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1989"/>
        <source>Notification status</source>
        <translation>Stav upozornění</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1999"/>
        <source>Education Manager Status Report</source>
        <translation>Education Manager Status Report</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="2000"/>
        <source>Dear recipient, Your working schedule is currently worth %1 work points. Please don&apos;t respond to this message, it was generated automatically.</source>
        <translation>Vážený příjemce, Vaše pracovní náplň zahrnuje %1 pracovních bodů. Na tuto zprávu prosím neodpovídejte, byla vygenerována automaticky.</translation>
    </message>
</context>
<context>
    <name>NotificationWindow</name>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="31"/>
        <source>Employee notification center</source>
        <translation>Správce upozornění zaměstnanců</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="51"/>
        <source>Clear selection</source>
        <translation>Zrušit výběr</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="65"/>
        <source>Include XML attachment with work labels</source>
        <translation>Připojit XML přílohu s pracovními štítky</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="34"/>
        <source>Send e-mail(s)</source>
        <translation>Odeslat e-mail(y)</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="40"/>
        <source>Internet connection unavailable.</source>
        <translation>Bez připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="53"/>
        <source>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6    work points: %7</source>
        <translation>%1) %2 (%3)
    e-mail: %4
    kancelář: %5    doktorand: %6    pracovní body: %7</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="77"/>
        <source>Assignment error</source>
        <translation>Chyba přiřazení</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="78"/>
        <source>At least one recipient has to be selected.</source>
        <translation>Je třeba vybrat alespoň jednoho příjemce.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sources/course.cpp" line="43"/>
        <location filename="../Sources/group.cpp" line="55"/>
        <source>CZ</source>
        <translation>CZ</translation>
    </message>
    <message>
        <location filename="../Sources/course.cpp" line="44"/>
        <location filename="../Sources/group.cpp" line="56"/>
        <source>EN</source>
        <translation>EN</translation>
    </message>
    <message>
        <location filename="../Sources/dbparser.cpp" line="17"/>
        <source>Connection error</source>
        <translation>Chyba připojení</translation>
    </message>
    <message>
        <location filename="../Sources/dbparser.cpp" line="26"/>
        <source>Database file could not be opened.</source>
        <translation>Soubor databáze se nepodařilo otevřít.</translation>
    </message>
    <message>
        <location filename="../Sources/employee.cpp" line="34"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../Sources/employee.cpp" line="35"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="32"/>
        <source>Bc.</source>
        <translation>Bc.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="33"/>
        <source>Mgr.</source>
        <translation>Mgr.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="34"/>
        <source>Ph. D.</source>
        <translation>Ph. D.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="44"/>
        <source>full-time</source>
        <translation>prezenční</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="45"/>
        <source>combined</source>
        <translation>kombinovaná</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="34"/>
        <source>summer semester</source>
        <translation>letní semestr</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="35"/>
        <source>winter semester</source>
        <translation>zimní semestr</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="45"/>
        <location filename="../Sources/worklabel.cpp" line="40"/>
        <source>credit test</source>
        <translation>zápočet</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="46"/>
        <location filename="../Sources/worklabel.cpp" line="41"/>
        <source>classified credit test</source>
        <translation>klasifikovaný zápočet</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="47"/>
        <location filename="../Sources/worklabel.cpp" line="42"/>
        <source>exam</source>
        <translation>zkouška</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="48"/>
        <source>credit test &amp; exam</source>
        <translation>zápočet &amp; zkouška</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="36"/>
        <source>lecture</source>
        <translation>přednáška</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="37"/>
        <source>practice</source>
        <translation>cvičení</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="38"/>
        <source>seminar</source>
        <translation>seminář</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="39"/>
        <source>atelier</source>
        <translation>ateliér</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="52"/>
        <source>standard course</source>
        <translation>základní kurz</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="53"/>
        <source>foreign language</source>
        <translation>v cizím jazyce</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="54"/>
        <source>doctoral course</source>
        <translation>doktorský kurz</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="65"/>
        <location filename="../Sources/xmlparser.cpp" line="251"/>
        <location filename="../Sources/xmlparser.cpp" line="356"/>
        <location filename="../Sources/xmlparser.cpp" line="417"/>
        <location filename="../Sources/xmlparser.cpp" line="514"/>
        <source>XML loading error</source>
        <translation>Potíže načtení souboru XML</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="66"/>
        <source>XML file could not be opened in order to load application settings</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem načtení aplikační nastavení</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="252"/>
        <source>XML file could not be opened in order to load course data</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem načení dat kurzů</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="357"/>
        <source>XML file could not be opened in order to load employee data</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem načení dat zaměstnanců</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="418"/>
        <source>XML file could not be opened in order to load work label data</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem načení dat pracovních štítků</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="515"/>
        <source>XML file could not be opened in order to load group data</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem načení dat skupin</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="586"/>
        <location filename="../Sources/xmlparser.cpp" line="641"/>
        <location filename="../Sources/xmlparser.cpp" line="676"/>
        <location filename="../Sources/xmlparser.cpp" line="722"/>
        <location filename="../Sources/xmlparser.cpp" line="765"/>
        <source>XML saving error</source>
        <translation>Potíže uložení souboru XML</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="587"/>
        <location filename="../Sources/xmlparser.cpp" line="766"/>
        <source>XML file could not be opened in order to save group changes</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem uložení změn skupin</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="642"/>
        <source>XML file could not be opened in order to save employee changes</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem uložení změn zaměstnanců</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="677"/>
        <source>XML file could not be opened in order to save work labels for employee</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem uložení změn pracovních štítků zaměstnance</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="723"/>
        <source>XML file could not be opened in order to save work label changes</source>
        <translation>Soubor XML se nepodařilo otevřít za účelem uložení změn pracovních štítků</translation>
    </message>
</context>
<context>
    <name>Smtp</name>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="66"/>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="257"/>
        <source>Qt Simple SMTP client</source>
        <translation>Jednoduchý Qt SMTP klient</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="66"/>
        <source>Couldn&apos;t open the file

</source>
        <translation>Nepodařilo se otevřít soubor

</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="247"/>
        <source>Message sent</source>
        <translation>Zpráva odeslána</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="257"/>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Neočekávaná odpověď SMTP serveru:

</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="259"/>
        <source>Failed to send message</source>
        <translation>Zprávu se nepodařilo odeslat</translation>
    </message>
</context>
</TS>
