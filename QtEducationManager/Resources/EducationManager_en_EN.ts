<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../Forms/aboutwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../Forms/aboutwindow.ui" line="81"/>
        <source>Education Manager</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Sources/aboutwindow.cpp" line="20"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../Sources/aboutwindow.cpp" line="30"/>
        <source>Education Manager ©2021, Tomáš Bartošík, TBU in Zlin</source>
        <translation>Education Manager ©2021, Tomáš Bartošík, TBU in Zlin</translation>
    </message>
</context>
<context>
    <name>DialogWindow</name>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="34"/>
        <source>Select course and employee for assignment</source>
        <translation>Select course and employee for assignment</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="54"/>
        <location filename="../Forms/dialogwindow.ui" line="120"/>
        <source>Clear selection</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../Forms/dialogwindow.ui" line="100"/>
        <source>Select group(s) for assignment</source>
        <translation>Select group(s) for assignment</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="44"/>
        <source>Description: L = lecture, P = practice, S = seminar, A = atelier</source>
        <translation>Description: L = lecture, P = practice, S = seminar, A = atelier</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="46"/>
        <source>Confirm assignment</source>
        <translation>Confirm assignment</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="47"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="62"/>
        <source>%1) %2, %3. year (%4, %5)
    summer semester students: %6, winter semester students: %7</source>
        <translation>%1) %2, %3. year (%4, %5)
    summer semester students: %6, winter semester students: %7</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="95"/>
        <source>%1) %2 (%3)
    course ending: %4 (credits: %5)
    language: %6, scheduled weeks: %7
    semester: %8
    scheduled hours  ~  L: %9    P: %10    S: %11    A: %12
    course limits  ~  L: %13    P: %14    S: %15    A: %16</source>
        <translation>%1) %2 (%3)
    course ending: %4 (credits: %5)
    language: %6, scheduled weeks: %7
    semester: %8
    scheduled hours  ~  L: %9    P: %10    S: %11    A: %12
    course limits  ~  L: %13    P: %14    S: %15    A: %16</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="135"/>
        <source>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6</source>
        <translation>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="180"/>
        <source>Assignment error</source>
        <translation>Assignment error</translation>
    </message>
    <message>
        <location filename="../Sources/dialogwindow.cpp" line="206"/>
        <source>Both a course and an employee have to be selected or none at all.</source>
        <translation>Both a course and an employee have to be selected or none at all.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Forms/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="35"/>
        <source>Work label list</source>
        <translation>Work label list</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="73"/>
        <source>Automatically assign work labels</source>
        <translation>Automatically assign work labels</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="80"/>
        <source>Assign selected work label</source>
        <translation>Assign selected work label</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="96"/>
        <location filename="../Sources/mainwindow.cpp" line="1881"/>
        <source>Add new work label</source>
        <translation>Add new work label</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="103"/>
        <source>Remove selected work label(s)</source>
        <translation>Remove selected work label(s)</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="131"/>
        <source>Student group list</source>
        <translation>Student group list</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="148"/>
        <location filename="../Sources/mainwindow.cpp" line="1922"/>
        <source>Add new group</source>
        <translation>Add new group</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="155"/>
        <location filename="../Sources/mainwindow.cpp" line="1924"/>
        <source>Update selected group</source>
        <translation>Update selected group</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="162"/>
        <source>Remove selected group(s)</source>
        <translation>Remove selected group(s)</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="190"/>
        <source>Employee list</source>
        <translation>Employee list</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="212"/>
        <source>Calculate work points</source>
        <translation>Calculate work points</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="228"/>
        <location filename="../Sources/mainwindow.cpp" line="1850"/>
        <source>Add new employee</source>
        <translation>Add new employee</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="235"/>
        <source>Remove selected employee(s)</source>
        <translation>Remove selected employee(s)</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="263"/>
        <source>Course list</source>
        <translation>Course list</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="297"/>
        <source>Manage student groups</source>
        <translation>Manage student groups</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="313"/>
        <location filename="../Sources/mainwindow.cpp" line="1818"/>
        <source>Add new course</source>
        <translation>Add new course</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="320"/>
        <source>Remove selected course(s)</source>
        <translation>Remove selected course(s)</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="363"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="377"/>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <location filename="../Sources/mainwindow.cpp" line="130"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="384"/>
        <source>Data Source</source>
        <translation>Data Source</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="396"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="404"/>
        <source>Student groups</source>
        <translation>Student groups</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="412"/>
        <source>Employees</source>
        <translation>Employees</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="420"/>
        <source>Courses</source>
        <translation>Courses</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="428"/>
        <source>Work labels</source>
        <translation>Work labels</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="439"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="447"/>
        <source>Czech</source>
        <translation>Czech</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="455"/>
        <source>XML files</source>
        <translation>XML files</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="463"/>
        <source>Local database</source>
        <translation>Local database</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="468"/>
        <source>Notification center</source>
        <translation>Notification center</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="473"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="65"/>
        <source>Settings file error</source>
        <translation>Settings file error</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="66"/>
        <source>Work point settings do not match required format</source>
        <translation>Work point settings do not match required format</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="87"/>
        <source>Education Manager</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Course
shortcut</source>
        <translation>Course
shortcut</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Course name</source>
        <translation>Course name</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Lect.
limit</source>
        <translation>Lect.
limit</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Prac.
limit</source>
        <translation>Prac.
limit</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="100"/>
        <source>Sem.
limit</source>
        <translation>Sem.
limit</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>At.
limit</source>
        <translation>At.
limit</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Lecture
hours</source>
        <translation>Lecture
hours</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Practice
hours</source>
        <translation>Practice
hours</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Seminar
hours</source>
        <translation>Seminar
hours</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="101"/>
        <source>Atelier
hours</source>
        <translation>Atelier
hours</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Weeks</source>
        <translation>Weeks</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Credits</source>
        <translation>Credits</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Semester</source>
        <translation>Semester</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="102"/>
        <source>Ending</source>
        <translation>Ending</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Name with titles</source>
        <translation>Name with titles</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Function</source>
        <translation>Function</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="109"/>
        <source>Private
phone number</source>
        <translation>Private
phone number</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Is doctoral?</source>
        <translation>Is doctoral?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Workroom</source>
        <translation>Workroom</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="110"/>
        <source>Workroom
phone number</source>
        <translation>Workroom
phone number</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Work label name</source>
        <translation>Work label name</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Scheduled
weeks</source>
        <translation>Scheduled
weeks</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="117"/>
        <source>Scheduled
hours (total)</source>
        <translation>Scheduled
hours (total)</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Student count</source>
        <translation>Student count</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Work points</source>
        <translation>Work points</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Label type</source>
        <translation>Label type</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="118"/>
        <source>Course type</source>
        <translation>Course type</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="123"/>
        <source>all labels</source>
        <translation>all labels</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="123"/>
        <source>fully unassigned labels</source>
        <translation>fully unassigned labels</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="124"/>
        <source>assigned to course only</source>
        <translation>assigned to course only</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="124"/>
        <source>fully assigned labels</source>
        <translation>fully assigned labels</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Specialization</source>
        <translation>Specialization</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Year</source>
        <translation>Year</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="128"/>
        <source>Summer semester
students</source>
        <translation>Summer semester
students</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Winter semester
students</source>
        <translation>Winter semester
students</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Study type</source>
        <translation>Study type</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="129"/>
        <source>Study form</source>
        <translation>Study form</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="283"/>
        <source>Entry error</source>
        <translation>Entry error</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="293"/>
        <source>summer semester</source>
        <translation>summer semester</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="293"/>
        <source>winter semester</source>
        <translation>winter semester</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>credit test</source>
        <translation>credit test</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <source>classified
 credit test</source>
        <translation>classified
 credit test</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>exam</source>
        <translation>exam</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="295"/>
        <source>credit test
 &amp; exam</source>
        <translation>credit test
 &amp; exam</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="310"/>
        <source>Confirm new course entry</source>
        <translation>Confirm new course entry</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="389"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="389"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="400"/>
        <source>Confirm new employee entry</source>
        <translation>Confirm new employee entry</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>lecture</source>
        <translation>lecture</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>practice</source>
        <translation>practice</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>seminar</source>
        <translation>seminar</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="464"/>
        <source>atelier</source>
        <translation>atelier</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="465"/>
        <source>classified credit test</source>
        <translation>classified credit test</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>standard course</source>
        <translation>standard course</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>foreign language</source>
        <translation>foreign language</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="467"/>
        <source>doctoral course</source>
        <translation>doctoral course</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="484"/>
        <source>Confirm new work label entry</source>
        <translation>Confirm new work label entry</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="562"/>
        <source>full-time</source>
        <translation>full-time</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="562"/>
        <source>combined</source>
        <translation>combined</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="577"/>
        <source>Confirm new group entry</source>
        <translation>Confirm new group entry</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="659"/>
        <source>Course shortcut should contain at least one character.</source>
        <translation>Course shortcut should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="663"/>
        <source>Course name should contain at least one character.</source>
        <translation>Course name should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="667"/>
        <source>Lecture capacity should be defined as a non-negative integral number.</source>
        <translation>Lecture capacity should be defined as a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="677"/>
        <source>Practice capacity should be defined as a non-negative integral number.</source>
        <translation>Practice capacity should be defined as a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="687"/>
        <source>Seminar capacity should be defined as a non-negative integral number.</source>
        <translation>Seminar capacity should be defined as a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="697"/>
        <source>Atelier capacity should be defined as a non-negative integral number.</source>
        <translation>Atelier capacity should be defined as a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="707"/>
        <source>Lecture hours are represented by a non-negative integral number.</source>
        <translation>Lecture hours are represented by a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="717"/>
        <source>Practice hours are represented by a non-negative integral number.</source>
        <translation>Practice hours are represented by a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="727"/>
        <source>Seminar hours are represented by a non-negative integral number.</source>
        <translation>Seminar hours are represented by a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="737"/>
        <source>Atelier hours are represented by a non-negative integral number.</source>
        <translation>Atelier hours are represented by a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="747"/>
        <source>Number of course weeks should be defined as a natural number.</source>
        <translation>Number of course weeks should be defined as a natural number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="757"/>
        <source>Number of course credits should match a non-negative integral number.</source>
        <translation>Number of course credits should match a non-negative integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="769"/>
        <location filename="../Sources/mainwindow.cpp" line="805"/>
        <location filename="../Sources/mainwindow.cpp" line="848"/>
        <location filename="../Sources/mainwindow.cpp" line="891"/>
        <source>Some columns have not been filled.</source>
        <translation>Some columns have not been filled.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="776"/>
        <source>Employee name should contain at least one character.</source>
        <translation>Employee name should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="780"/>
        <source>Employee function should contain at least one character.</source>
        <translation>Employee function should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="784"/>
        <source>E-mail format does not match requirements.</source>
        <translation>E-mail format does not match requirements.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="789"/>
        <source>Private phone number does not match required format.</source>
        <translation>Private phone number does not match required format.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="795"/>
        <source>Workroom label should contain at least one character.</source>
        <translation>Workroom label should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="799"/>
        <source>Workroom phone number does not match required format.</source>
        <translation>Workroom phone number does not match required format.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="812"/>
        <source>Work label name should be defined by at least one character.</source>
        <translation>Work label name should be defined by at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="816"/>
        <source>Number of scheduled weeks should be defined by a natural number.</source>
        <translation>Number of scheduled weeks should be defined by a natural number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="826"/>
        <source>Number of scheduled hours should be defined by a natural number.</source>
        <translation>Number of scheduled hours should be defined by a natural number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="836"/>
        <source>Number of students should be defined by a natural number.</source>
        <translation>Number of students should be defined by a natural number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="855"/>
        <source>Specialization should contain at least one character.</source>
        <translation>Specialization should contain at least one character.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="859"/>
        <source>A year is represented by an integral number in range from 1 to 4.</source>
        <translation>A year is represented by an integral number in range from 1 to 4.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="869"/>
        <source>Number of summer semester students does not match an integral number.</source>
        <translation>Number of summer semester students does not match an integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="879"/>
        <source>Number of winter semester students does not match an integral number.</source>
        <translation>Number of winter semester students does not match an integral number.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="894"/>
        <source>Unexpected behavior.</source>
        <translation>Unexpected behavior.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="903"/>
        <source>Removal error</source>
        <translation>Removal error</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="904"/>
        <source>Select one or more rows in order to remove them.</source>
        <translation>Select one or more rows in order to remove them.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="912"/>
        <location filename="../Sources/mainwindow.cpp" line="945"/>
        <location filename="../Sources/mainwindow.cpp" line="978"/>
        <location filename="../Sources/mainwindow.cpp" line="1018"/>
        <source>Removal confirmation</source>
        <translation>Removal confirmation</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="913"/>
        <location filename="../Sources/mainwindow.cpp" line="946"/>
        <location filename="../Sources/mainwindow.cpp" line="979"/>
        <location filename="../Sources/mainwindow.cpp" line="1019"/>
        <source>Do you really want to remove selected row(s)?</source>
        <translation>Do you really want to remove selected row(s)?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1051"/>
        <location filename="../Sources/mainwindow.cpp" line="1195"/>
        <source>Selection error</source>
        <translation>Selection error</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1052"/>
        <source>Select one row in order to update it.</source>
        <translation>Select one row in order to update it.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1076"/>
        <source>Confirm row changes</source>
        <translation>Confirm row changes</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1202"/>
        <source>Select one course in order to assign groups.</source>
        <translation>Select one course in order to assign groups.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1257"/>
        <source>Work labels creation</source>
        <translation>Work labels creation</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1258"/>
        <source>Would you like to automatically create work label(s)?</source>
        <translation>Would you like to automatically create work label(s)?</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1282"/>
        <source>Select one work label in order to assign its course and employee.</source>
        <translation>Select one work label in order to assign its course and employee.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1370"/>
        <source>Total work points</source>
        <translation>Total work points</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1375"/>
        <source>Select one employee in order to calculate their work points.</source>
        <translation>Select one employee in order to calculate their work points.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1392"/>
        <source>Total work points of %1: %2</source>
        <translation>Total work points of %1: %2</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1692"/>
        <source>Assignment error</source>
        <translation>Assignment error</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1707"/>
        <source>There are no employees to be assigned.</source>
        <translation>There are no employees to be assigned.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1742"/>
        <source>Work labels previously unassigned to employees were distributed automatically.</source>
        <translation>Work labels previously unassigned to employees were distributed automatically.</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1989"/>
        <source>Notification status</source>
        <translation>Notification status</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="1999"/>
        <source>Education Manager Status Report</source>
        <translation>Education Manager Status Report</translation>
    </message>
    <message>
        <location filename="../Sources/mainwindow.cpp" line="2000"/>
        <source>Dear recipient, Your working schedule is currently worth %1 work points. Please don&apos;t respond to this message, it was generated automatically.</source>
        <translation>Dear recipient, Your working schedule is currently worth %1 work points. Please don&apos;t respond to this message, it was generated automatically.</translation>
    </message>
</context>
<context>
    <name>NotificationWindow</name>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>Education Manager</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="31"/>
        <source>Employee notification center</source>
        <translation>Employee notification center</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="51"/>
        <source>Clear selection</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location filename="../Forms/notificationwindow.ui" line="65"/>
        <source>Include XML attachment with work labels</source>
        <translation>Include XML attachment with work labels</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="34"/>
        <source>Send e-mail(s)</source>
        <translation>Send e-mail(s)</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="40"/>
        <source>Internet connection unavailable.</source>
        <translation>Internet connection unavailable.</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="53"/>
        <source>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6    work points: %7</source>
        <translation>%1) %2 (%3)
    e-mail: %4
    workroom: %5    doctoral: %6    work points: %7</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="77"/>
        <source>Assignment error</source>
        <translation>Assignment error</translation>
    </message>
    <message>
        <location filename="../Sources/notificationwindow.cpp" line="78"/>
        <source>At least one recipient has to be selected.</source>
        <translation>At least one recipient has to be selected.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sources/course.cpp" line="43"/>
        <location filename="../Sources/group.cpp" line="55"/>
        <source>CZ</source>
        <translation>CZ</translation>
    </message>
    <message>
        <location filename="../Sources/course.cpp" line="44"/>
        <location filename="../Sources/group.cpp" line="56"/>
        <source>EN</source>
        <translation>EN</translation>
    </message>
    <message>
        <location filename="../Sources/dbparser.cpp" line="17"/>
        <source>Connection error</source>
        <translation>Connection error</translation>
    </message>
    <message>
        <location filename="../Sources/dbparser.cpp" line="26"/>
        <source>Database file could not be opened.</source>
        <translation>Database file could not be opened.</translation>
    </message>
    <message>
        <location filename="../Sources/employee.cpp" line="34"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../Sources/employee.cpp" line="35"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="32"/>
        <source>Bc.</source>
        <translation>Bc.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="33"/>
        <source>Mgr.</source>
        <translation>Mgr.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="34"/>
        <source>Ph. D.</source>
        <translation>Ph. D.</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="44"/>
        <source>full-time</source>
        <translation>full-time</translation>
    </message>
    <message>
        <location filename="../Sources/group.cpp" line="45"/>
        <source>combined</source>
        <translation>combined</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="34"/>
        <source>summer semester</source>
        <translation>summer semester</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="35"/>
        <source>winter semester</source>
        <translation>winter semester</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="45"/>
        <location filename="../Sources/worklabel.cpp" line="40"/>
        <source>credit test</source>
        <translation>credit test</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="46"/>
        <location filename="../Sources/worklabel.cpp" line="41"/>
        <source>classified credit test</source>
        <translation>classified credit test</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="47"/>
        <location filename="../Sources/worklabel.cpp" line="42"/>
        <source>exam</source>
        <translation>exam</translation>
    </message>
    <message>
        <location filename="../Sources/semester.cpp" line="48"/>
        <source>credit test &amp; exam</source>
        <translation>credit test &amp; exam</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="36"/>
        <source>lecture</source>
        <translation>lecture</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="37"/>
        <source>practice</source>
        <translation>practice</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="38"/>
        <source>seminar</source>
        <translation>seminar</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="39"/>
        <source>atelier</source>
        <translation>atelier</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="52"/>
        <source>standard course</source>
        <translation>standard course</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="53"/>
        <source>foreign language</source>
        <translation>foreign language</translation>
    </message>
    <message>
        <location filename="../Sources/worklabel.cpp" line="54"/>
        <source>doctoral course</source>
        <translation>doctoral course</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="65"/>
        <location filename="../Sources/xmlparser.cpp" line="251"/>
        <location filename="../Sources/xmlparser.cpp" line="356"/>
        <location filename="../Sources/xmlparser.cpp" line="417"/>
        <location filename="../Sources/xmlparser.cpp" line="514"/>
        <source>XML loading error</source>
        <translation>XML loading error</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="66"/>
        <source>XML file could not be opened in order to load application settings</source>
        <translation>XML file could not be opened in order to load application settings</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="252"/>
        <source>XML file could not be opened in order to load course data</source>
        <translation>XML file could not be opened in order to load course data</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="357"/>
        <source>XML file could not be opened in order to load employee data</source>
        <translation>XML file could not be opened in order to load employee data</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="418"/>
        <source>XML file could not be opened in order to load work label data</source>
        <translation>XML file could not be opened in order to load work label data</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="515"/>
        <source>XML file could not be opened in order to load group data</source>
        <translation>XML file could not be opened in order to load group data</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="586"/>
        <location filename="../Sources/xmlparser.cpp" line="641"/>
        <location filename="../Sources/xmlparser.cpp" line="676"/>
        <location filename="../Sources/xmlparser.cpp" line="722"/>
        <location filename="../Sources/xmlparser.cpp" line="765"/>
        <source>XML saving error</source>
        <translation>XML saving error</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="587"/>
        <location filename="../Sources/xmlparser.cpp" line="766"/>
        <source>XML file could not be opened in order to save group changes</source>
        <translation>XML file could not be opened in order to save group changes</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="642"/>
        <source>XML file could not be opened in order to save employee changes</source>
        <translation>XML file could not be opened in order to save employee changes</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="677"/>
        <source>XML file could not be opened in order to save work labels for employee</source>
        <translation>XML file could not be opened in order to save work labels for employee</translation>
    </message>
    <message>
        <location filename="../Sources/xmlparser.cpp" line="723"/>
        <source>XML file could not be opened in order to save work label changes</source>
        <translation>XML file could not be opened in order to save work label changes</translation>
    </message>
</context>
<context>
    <name>Smtp</name>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="66"/>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="257"/>
        <source>Qt Simple SMTP client</source>
        <translation>Qt Simple SMTP client</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="66"/>
        <source>Couldn&apos;t open the file

</source>
        <translation>Couldn&apos;t open the file\n\n</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="247"/>
        <source>Message sent</source>
        <translation>Message sent</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="257"/>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Unexpected reply from SMTP server:\n\n</translation>
    </message>
    <message>
        <location filename="../Libraries/SmtpClient/smtp.cpp" line="259"/>
        <source>Failed to send message</source>
        <translation>Failed to send message</translation>
    </message>
</context>
</TS>
