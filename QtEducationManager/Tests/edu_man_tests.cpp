/**
 * @file        edu_man_tests.cpp
 * @author      Tomas Bartosik
 * @date        07.03.2021
 * @brief       tests definition file for Education Manager project
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include <QtTest>
#include <QCoreApplication>
#include <QProcess>
#include "../Headers/dbparser.hpp"
#include "../Headers/workpoint.hpp"

/*Class definition: ---------------------------------------------------------*/
class EduManTest : public QObject
{
    Q_OBJECT

public:
    EduManTest();
    ~EduManTest();

private slots:
    void EMT_CourseCreationTest();
    void EMT_EmployeeCreationTest();
    void EMT_WorkLabelCreationTest();
    void EMT_GroupCreationTest();
    void EMT_MatchingForWorkLabelAndEmployeeTest();
    void EMT_MatchingForCourseTest();
    void EMT_CourseXmlProcessingTest();
    void EMT_CourseDbProcessingTest();
    void EMT_EmployeeXmlProcessingTest();
    void EMT_EmployeeDbProcessingTest();
    void EMT_WorkLabelXmlProcessingTest();
    void EMT_WorkLabelDbProcessingTest();
    void EMT_GroupXmlProcessingTest();
    void EMT_GroupDbProcessingTest();
    void EMT_WorkPointXmlProcessingTest();

    void summaryTestCase();

private:
    XmlParser mXmlParser;
    DbParser mDbParser;
    QProcess mExporter;
    unsigned int mAssertions;
};

EduManTest::EduManTest()
{
    mAssertions = 0;
    mExporter.setWorkingDirectory(QCoreApplication::applicationDirPath());
}

EduManTest::~EduManTest()
{
    mExporter.terminate();
}

/*Class Calculator tests: ---------------------------------------------------*/
void EduManTest::EMT_CourseCreationTest()
{
    Course c;
    QVERIFY(QString() == "");
    QVERIFY(c.Course_GetCourseShortcut() == QString());
    QVERIFY(c.Course_GetCourseName() == "");
    QVERIFY(c.Course_GetLectureCapacity() == 0);
    QVERIFY(c.Course_GetPracticeCapacity() == 0);
    QVERIFY(c.Course_GetSeminarCapacity() == 0);
    QVERIFY(c.Course_GetAtelierCapacity() == 0);
    QVERIFY(c.Course_GetLectureHours() == 0);
    QVERIFY(c.Course_GetPracticeHours() == 0);
    QVERIFY(c.Course_GetSeminarHours() == 0);
    QVERIFY(c.Course_GetAtelierHours() == 0);
    QVERIFY(c.Course_GetWeekCount() == 0);
    QVERIFY(c.Course_GetCredits() == 0);
    QVERIFY(c.Course_GetSemesterType(true) == "1");
    QVERIFY(c.Course_GetEnding(true) == "1");
    QVERIFY(c.Course_GetLanguage(true) == "1");
    QVERIFY(c.Course_GetGroups().count() == 0);
    mAssertions += 17;

    Lesson ls(12, 8, 0, 0);
    SemesterType st = SemesterType::eLS;
    Ending en = Ending::eKZ;
    Semester sm(24, 32, 0, 0, 14, 5, st, en);
    Language ln = Language::eCZ;
    QList<QSharedPointer<Group>> gList;
    QSharedPointer<Course> mCourse = QSharedPointer<Course>(new Course("1", "AP8PO", "Pokrocile Programovani", ls, sm, ln, gList));
    QVERIFY(mCourse->Course_GetCourseShortcut() == "AP8PO");
    QVERIFY(mCourse->Course_GetCourseName() == "Pokrocile Programovani");
    QVERIFY(mCourse->Course_GetLectureCapacity() == 12);
    QVERIFY(mCourse->Course_GetPracticeCapacity() == 8);
    QVERIFY(mCourse->Course_GetSeminarCapacity() == 0);
    QVERIFY(mCourse->Course_GetAtelierCapacity() == 0);
    QVERIFY(mCourse->Course_GetLectureHours() == 24);
    QVERIFY(mCourse->Course_GetPracticeHours() == 32);
    QVERIFY(mCourse->Course_GetSeminarHours() == 0);
    QVERIFY(mCourse->Course_GetAtelierHours() == 0);
    QVERIFY(mCourse->Course_GetWeekCount() == 14);
    QVERIFY(mCourse->Course_GetCredits() == 5);
    QVERIFY(mCourse->Course_GetSemesterType(true) == "1");
    QVERIFY(mCourse->Course_GetEnding(true) == "2");
    QVERIFY(mCourse->Course_GetLanguage(true) == "1");
    QVERIFY(mCourse->Course_GetGroups().count() == 0);
    gList.clear();
    mAssertions += 16;
}

void EduManTest::EMT_EmployeeCreationTest()
{
    Employee e;
    QVERIFY(e.Employee_GetTitledName() == QString());
    QVERIFY(e.Employee_GetFunction() == QString());
    QVERIFY(e.Employee_GetEmail() == QString());
    QVERIFY(e.Employee_GetPrivatePhone() == QString());
    QVERIFY(e.Employee_GetIsDoctoral(true) == "0");
    QVERIFY(e.Employee_GetRoom() == QString());
    QVERIFY(e.Employee_GetRoomPhone() == QString());
    mAssertions += 7;

    Workroom wr("51/518", "+420571552214");
    QSharedPointer<Employee> mEmployee = QSharedPointer<Employee>(new Employee("1", "Ing. Marian Novak",
           "Odborný asistent", "mnovak@utb.cz", "245812555", true, wr));
    QVERIFY(mEmployee->Employee_GetTitledName() == "Ing. Marian Novak");
    QVERIFY(mEmployee->Employee_GetFunction() == "Odborný asistent");
    QVERIFY(mEmployee->Employee_GetEmail() == "mnovak@utb.cz");
    QVERIFY(mEmployee->Employee_GetPrivatePhone() == "245812555");
    QVERIFY(mEmployee->Employee_GetIsDoctoral(true) == "1");
    QVERIFY(mEmployee->Employee_GetRoom() == "51/518");
    QVERIFY(mEmployee->Employee_GetRoomPhone() == "+420571552214");
    mAssertions += 7;
}

void EduManTest::EMT_WorkLabelCreationTest()
{
    WorkLabel wl;
    QVERIFY(wl.WorkLabel_GetLabelName() == QString());
    QVERIFY(wl.WorkLabel_GetWeekCount() == 0);
    QVERIFY(wl.WorkLabel_GetScheduledHours() == 0);
    QVERIFY(wl.WorkLabel_GetStudentCount() == 0);
    QVERIFY(wl.WorkLabel_GetWorkPoints() == 0.0f);
    QVERIFY(wl.WorkLabel_GetLabelType(true) == "1");
    QVERIFY(wl.WorkLabel_GetCourseType(true) == "1");
    QVERIFY(wl.WorkLabel_GetEmployee() == nullptr);
    QVERIFY(wl.WorkLabel_GetCourse() == nullptr);
    mAssertions += 9;

    LabelType lt = LabelType::ePractice;
    CourseType ct = CourseType::eStandard;
    QSharedPointer<WorkLabel> mWorkLabel = QSharedPointer<WorkLabel>(new WorkLabel("1", "Cviceni AP8PO, skupina 1",
          12, 24, 15, 4.8f, lt, ct, nullptr, nullptr));
    QVERIFY(mWorkLabel->WorkLabel_GetLabelName() == "Cviceni AP8PO, skupina 1");
    QVERIFY(mWorkLabel->WorkLabel_GetWeekCount() == 12);
    QVERIFY(mWorkLabel->WorkLabel_GetScheduledHours() == 24);
    QVERIFY(mWorkLabel->WorkLabel_GetStudentCount() == 15);
    QVERIFY(mWorkLabel->WorkLabel_GetWorkPoints() == 4.8f);
    QVERIFY(mWorkLabel->WorkLabel_GetLabelType(true) == "2");
    QVERIFY(mWorkLabel->WorkLabel_GetCourseType(true) == "1");
    QVERIFY(mWorkLabel->WorkLabel_GetEmployee() == nullptr);
    QVERIFY(mWorkLabel->WorkLabel_GetCourse() == nullptr);
    mAssertions += 9;
}

void EduManTest::EMT_GroupCreationTest()
{
    Group g;
    QVERIFY(g.Group_GetStudySpecialization() == QString());
    QVERIFY(g.Group_GetYear() == 0);
    QVERIFY(g.Group_GetSummerStudentCount() == 0);
    QVERIFY(g.Group_GetWinterStudentCount() == 0);
    QVERIFY(g.Group_GetStudyType(true) == "1");
    QVERIFY(g.Group_GetStudyForm(true) == "1");
    QVERIFY(g.Group_GetLanguage(true) == "1");
    mAssertions += 7;

    Year y(2, 65, 81);
    QSharedPointer<Group> mGroup = QSharedPointer<Group>(new Group("1", "Softwarové inženýrství",
          y, StudyType::eBc, StudyForm::eK, Language::eEN));
    QVERIFY(mGroup->Group_GetStudySpecialization() == "Softwarové inženýrství");
    QVERIFY(mGroup->Group_GetYear() == 2);
    QVERIFY(mGroup->Group_GetSummerStudentCount() == 65);
    QVERIFY(mGroup->Group_GetWinterStudentCount() == 81);
    QVERIFY(mGroup->Group_GetStudyType(true) == "1");
    QVERIFY(mGroup->Group_GetStudyForm(true) == "2");
    QVERIFY(mGroup->Group_GetLanguage(true) == "2");
    mAssertions += 7;
}

void EduManTest::EMT_MatchingForWorkLabelAndEmployeeTest()
{
    Workroom wr("52/504", "+420754812552");
    QSharedPointer<Employee> employee = QSharedPointer<Employee>(new Employee("1", "John",
          "docent", "john@example.com", "455487215", false, wr));

    Lesson ls(150, 24, 24, 24);
    Semester sm(14, 28, 0, 0, 14, 5, SemesterType::eLS, Ending::eZ);
    QList<QSharedPointer<Group>> courseGroups;
    QSharedPointer<Course> course = QSharedPointer<Course>(new Course("1", "AP8GI",
          "Geografické systémy", ls, sm, Language::eCZ, courseGroups));

    /*Assigning employee and course to a work label: ------------------------*/
    QSharedPointer<WorkLabel> mWorkLabel = QSharedPointer<WorkLabel>(new WorkLabel("1", "Přednášky AP8GI",
         14, 14, 42, 3.9f, LabelType::eLecture, CourseType::eStandard, employee, course));

    QVERIFY(mWorkLabel->WorkLabel_GetEmployee() != nullptr);
    QVERIFY(mWorkLabel->WorkLabel_GetCourse() != nullptr);
    QVERIFY(mWorkLabel->WorkLabel_GetEmployee()->Employee_GetFunction() == "docent");
    QVERIFY(mWorkLabel->WorkLabel_GetEmployee()->Employee_GetRoom() == "52/504");
    QVERIFY(mWorkLabel->WorkLabel_GetCourse()->Course_GetLanguage(true) == "1");
    QVERIFY(mWorkLabel->WorkLabel_GetCourse()->Course_GetLectureCapacity() == 150);
    mAssertions += 6;
}

void EduManTest::EMT_MatchingForCourseTest()
{
    Year yr(3, 32, 37);
    QSharedPointer<Group> group = QSharedPointer<Group>(new Group("1", "Softwarové inženýrství", yr, StudyType::eMgr, StudyForm::eP, Language::eCZ));
    QList<QSharedPointer<Group>> groupList = QList<QSharedPointer<Group>> ();

    Lesson ls(120, 15, 15, 15);
    Semester sm(14, 28, 0, 0, 14, 6, SemesterType::eZS, Ending::eKZ);
    Course c("1", "AP8PP", "Paralelní procesy", ls, sm, Language::eCZ, groupList);

    QVERIFY(c.Course_GetGroups().count() == 0);
    c.Course_AppendGroup(group);
    QVERIFY(c.Course_GetGroups().count() > 0);
    QVERIFY(c.Course_GetGroups().first()->Group_GetWinterStudentCount() == 37);
    mAssertions += 3;
}

void EduManTest::EMT_CourseXmlProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<Course> courseFirst = QSharedPointer<Course>(new Course("1", "AP7SC", "Softcomputing a datamining", Lesson(160, 20, 0, 0),
          Semester(28, 28, 0, 0, 14, 5, SemesterType::eLS, Ending::eZZk), Language::eCZ, QList<QSharedPointer<Group>> ()));
    QSharedPointer<Course> courseSecond = QSharedPointer<Course>(new Course("2", "AP7MT", "Mobilní technologie", Lesson(90, 14, 0, 0),
          Semester(14, 28, 0, 0, 14, 4, SemesterType::eZS, Ending::eKZ), Language::eCZ, QList<QSharedPointer<Group>> ()));
    QList<QSharedPointer<Course>> courseList;
    courseList.append(courseFirst);
    courseList.append(courseSecond);
    mXmlParser.XmlParser_SaveCourseData(courseList);
    courseList.clear();

    QList<QSharedPointer<Course>> newList;
    QStringList dataLoaded;
    newList = mXmlParser.XmlParser_LoadCourseData();
    dataLoaded = newList.first()->Course_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "AP7SC");
    QVERIFY(dataLoaded.at(2) == "Softcomputing a datamining");
    QVERIFY(dataLoaded.at(3) == "160");
    QVERIFY(dataLoaded.at(4) == "20");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(dataLoaded.at(6) == "0");
    QVERIFY(dataLoaded.at(7) == "28");
    QVERIFY(dataLoaded.at(8) == "28");
    QVERIFY(dataLoaded.at(9) == "0");
    QVERIFY(dataLoaded.at(10) == "0");
    QVERIFY(dataLoaded.at(11) == "14");
    QVERIFY(dataLoaded.at(12) == "5");
    QVERIFY(newList.first()->Course_GetSemesterType(true) == "1");
    QVERIFY(newList.first()->Course_GetEnding(true) == "4");
    QVERIFY(newList.first()->Course_GetLanguage(true) == "1");
    mAssertions += 16;

    dataLoaded = newList.last()->Course_GetAll();
    QVERIFY(dataLoaded.at(0) == "2");
    QVERIFY(dataLoaded.at(1) == "AP7MT");
    QVERIFY(dataLoaded.at(2) == "Mobilní technologie");
    QVERIFY(dataLoaded.at(3) == "90");
    QVERIFY(dataLoaded.at(4) == "14");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(dataLoaded.at(6) == "0");
    QVERIFY(dataLoaded.at(7) == "14");
    QVERIFY(dataLoaded.at(8) == "28");
    QVERIFY(dataLoaded.at(9) == "0");
    QVERIFY(dataLoaded.at(10) == "0");
    QVERIFY(dataLoaded.at(11) == "14");
    QVERIFY(dataLoaded.at(12) == "4");
    QVERIFY(newList.last()->Course_GetSemesterType(true) == "2");
    QVERIFY(newList.last()->Course_GetEnding(true) == "2");
    QVERIFY(newList.last()->Course_GetLanguage(true) == "1");
    mAssertions += 16;
}

void EduManTest::EMT_CourseDbProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<Course> courseFirst = QSharedPointer<Course>(new Course("1", "AP7SC", "Softcomputing a datamining", Lesson(160, 20, 0, 0),
          Semester(28, 28, 0, 0, 14, 5, SemesterType::eLS, Ending::eZZk), Language::eCZ, QList<QSharedPointer<Group>> ()));
    QSharedPointer<Course> courseSecond = QSharedPointer<Course>(new Course("2", "AP7MT", "Mobilní technologie", Lesson(90, 14, 0, 0),
          Semester(14, 28, 0, 0, 14, 4, SemesterType::eZS, Ending::eKZ), Language::eCZ, QList<QSharedPointer<Group>> ()));

    mDbParser.DbParser_DeleteCourses(QStringList({"initialCourse"}));
    mDbParser.DbParser_InsertCourse(courseFirst);
    mDbParser.DbParser_InsertCourse(courseSecond);

    QList<QSharedPointer<Course>> newList;
    QStringList dataLoaded;
    newList = mDbParser.DbParser_LoadCourseData();
    dataLoaded = newList.first()->Course_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "AP7SC");
    QVERIFY(dataLoaded.at(2) == "Softcomputing a datamining");
    QVERIFY(dataLoaded.at(3) == "160");
    QVERIFY(dataLoaded.at(4) == "20");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(dataLoaded.at(6) == "0");
    QVERIFY(dataLoaded.at(7) == "28");
    QVERIFY(dataLoaded.at(8) == "28");
    QVERIFY(dataLoaded.at(9) == "0");
    QVERIFY(dataLoaded.at(10) == "0");
    QVERIFY(dataLoaded.at(11) == "14");
    QVERIFY(dataLoaded.at(12) == "5");
    QVERIFY(newList.first()->Course_GetSemesterType(true) == "1");
    QVERIFY(newList.first()->Course_GetEnding(true) == "4");
    QVERIFY(newList.first()->Course_GetLanguage(true) == "1");
    mAssertions += 16;

    dataLoaded = newList.last()->Course_GetAll();
    QVERIFY(dataLoaded.at(0) == "2");
    QVERIFY(dataLoaded.at(1) == "AP7MT");
    QVERIFY(dataLoaded.at(2) == "Mobilní technologie");
    QVERIFY(dataLoaded.at(3) == "90");
    QVERIFY(dataLoaded.at(4) == "14");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(dataLoaded.at(6) == "0");
    QVERIFY(dataLoaded.at(7) == "14");
    QVERIFY(dataLoaded.at(8) == "28");
    QVERIFY(dataLoaded.at(9) == "0");
    QVERIFY(dataLoaded.at(10) == "0");
    QVERIFY(dataLoaded.at(11) == "14");
    QVERIFY(dataLoaded.at(12) == "4");
    QVERIFY(newList.last()->Course_GetSemesterType(true) == "2");
    QVERIFY(newList.last()->Course_GetEnding(true) == "2");
    QVERIFY(newList.last()->Course_GetLanguage(true) == "1");
    mAssertions += 16;
}

void EduManTest::EMT_EmployeeXmlProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<WorkLabel> workLabel = QSharedPointer<WorkLabel>(new WorkLabel("1", "AP8UN, cvičení skupina 2", 14, 28, 16, 0.0f,
           LabelType::ePractice, CourseType::eStandard, nullptr, nullptr));
    QList<QSharedPointer<WorkLabel>> workLabelList;
    workLabelList.append(workLabel);
    mXmlParser.XmlParser_SaveWorkLabelData(workLabelList);
    workLabelList.clear();

    QList<QSharedPointer<WorkLabel>> newList;
    QStringList dataLoaded;
    newList = mXmlParser.XmlParser_LoadWorkLabelData();
    dataLoaded = newList.first()->WorkLabel_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "AP8UN, cvičení skupina 2");
    QVERIFY(dataLoaded.at(2) == "14");
    QVERIFY(dataLoaded.at(3) == "28");
    QVERIFY(dataLoaded.at(4) == "16");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(newList.first()->WorkLabel_GetLabelType(true) == "2");
    QVERIFY(newList.first()->WorkLabel_GetCourseType(true) == "1");
    mAssertions += 8;
}

void EduManTest::EMT_EmployeeDbProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<Employee> employee = QSharedPointer<Employee>(new Employee("1", "Ing. Marián Boris", "asistent", "mboris@utb.cz", "", true,
         Workroom("52/212", "+420757123645")));

    mDbParser.DbParser_DeleteEmployees(QStringList({"initialEmployee"}));
    mDbParser.DbParser_InsertEmployee(employee);

    QList<QSharedPointer<Employee>> newList;
    QStringList dataLoaded;
    newList = mDbParser.DbParser_LoadEmployeeData();
    dataLoaded = newList.first()->Employee_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "Ing. Marián Boris");
    QVERIFY(dataLoaded.at(2) == "asistent");
    QVERIFY(dataLoaded.at(3) == "mboris@utb.cz");
    QVERIFY(dataLoaded.at(4) == "");
    QVERIFY(newList.first()->Employee_GetIsDoctoral(true) == "1");
    QVERIFY(dataLoaded.at(6) == "52/212");
    QVERIFY(dataLoaded.at(7) == "+420757123645");
    mAssertions += 8;
}

void EduManTest::EMT_WorkLabelXmlProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<WorkLabel> workLabel = QSharedPointer<WorkLabel>(new WorkLabel("1", "AP8UN, cvičení skupina 2", 14, 28, 16, 0.0f,
           LabelType::ePractice, CourseType::eStandard, nullptr, nullptr));
    QList<QSharedPointer<WorkLabel>> workLabelList;
    workLabelList.append(workLabel);
    mXmlParser.XmlParser_SaveWorkLabelData(workLabelList);
    workLabelList.clear();

    QList<QSharedPointer<WorkLabel>> newList;
    QStringList dataLoaded;
    newList = mXmlParser.XmlParser_LoadWorkLabelData();
    dataLoaded = newList.first()->WorkLabel_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "AP8UN, cvičení skupina 2");
    QVERIFY(dataLoaded.at(2) == "14");
    QVERIFY(dataLoaded.at(3) == "28");
    QVERIFY(dataLoaded.at(4) == "16");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(newList.first()->WorkLabel_GetLabelType(true) == "2");
    QVERIFY(newList.first()->WorkLabel_GetCourseType(true) == "1");
    mAssertions += 8;
}

void EduManTest::EMT_WorkLabelDbProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<WorkLabel> workLabel = QSharedPointer<WorkLabel>(new WorkLabel("1", "AP8UN, cvičení skupina 2", 14, 28, 16, 0.0f,
           LabelType::ePractice, CourseType::eStandard, nullptr, nullptr));

    mDbParser.DbParser_DeleteWorkLabelsByID(QStringList({"initialWorkLabel"}));
    mDbParser.DbParser_InsertWorkLabel(workLabel);

    QList<QSharedPointer<WorkLabel>> newList;
    QStringList dataLoaded;
    newList = mDbParser.DbParser_LoadWorkLabelData();
    dataLoaded = newList.first()->WorkLabel_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "AP8UN, cvičení skupina 2");
    QVERIFY(dataLoaded.at(2) == "14");
    QVERIFY(dataLoaded.at(3) == "28");
    QVERIFY(dataLoaded.at(4) == "16");
    QVERIFY(dataLoaded.at(5) == "0");
    QVERIFY(newList.first()->WorkLabel_GetLabelType(true) == "2");
    QVERIFY(newList.first()->WorkLabel_GetCourseType(true) == "1");
    mAssertions += 8;
}

void EduManTest::EMT_GroupXmlProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<Group> group = QSharedPointer<Group>(new Group("1", "Bezpečnostní technologie", Year(2, 56, 73), StudyType::eBc,
          StudyForm::eP, Language::eCZ));
    QList<QSharedPointer<Group>> groupList;
    groupList.append(group);
    mXmlParser.XmlParser_SaveGroupData(groupList);
    groupList.clear();

    QList<QSharedPointer<Group>> newList;
    QStringList dataLoaded;
    newList = mXmlParser.XmlParser_LoadGroupData();
    dataLoaded = newList.first()->Group_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "Bezpečnostní technologie");
    QVERIFY(dataLoaded.at(2) == "2");
    QVERIFY(dataLoaded.at(3) == "56");
    QVERIFY(dataLoaded.at(4) == "73");
    QVERIFY(newList.first()->Group_GetStudyType(true) == "1");
    QVERIFY(newList.first()->Group_GetStudyForm(true) == "1");
    QVERIFY(newList.first()->Group_GetLanguage(true) == "1");
    mAssertions += 8;
}

void EduManTest::EMT_GroupDbProcessingTest()
{
    /*This test involves automatic file creation inside tests build folder. -*/
    QSharedPointer<Group> group = QSharedPointer<Group>(new Group("1", "Bezpečnostní technologie", Year(2, 56, 73), StudyType::eBc,
          StudyForm::eP, Language::eCZ));


    mDbParser.DbParser_DeleteGroups(QStringList({"initialGroup"}));
    mDbParser.DbParser_InsertGroup(group);


    QList<QSharedPointer<Group>> newList;
    QStringList dataLoaded;
    newList = mDbParser.DbParser_LoadGroupData();
    dataLoaded = newList.first()->Group_GetAll();
    QVERIFY(dataLoaded.at(0) == "1");
    QVERIFY(dataLoaded.at(1) == "Bezpečnostní technologie");
    QVERIFY(dataLoaded.at(2) == "2");
    QVERIFY(dataLoaded.at(3) == "56");
    QVERIFY(dataLoaded.at(4) == "73");
    QVERIFY(newList.first()->Group_GetStudyType(true) == "1");
    QVERIFY(newList.first()->Group_GetStudyForm(true) == "1");
    QVERIFY(newList.first()->Group_GetLanguage(true) == "1");
    mAssertions += 8;
}

void EduManTest::EMT_WorkPointXmlProcessingTest()
{
    QStringList workPointList;
    WorkPoint workPoint;
    QList<float> workPointsLoaded;
    workPointList = mXmlParser.XmlParser_LoadWorkPointData();

    QVERIFY(workPointList.at(0) == "1.8");
    QVERIFY(workPointList.at(1) == "2.4");
    QVERIFY(workPointList.at(2) == "1.2");
    QVERIFY(workPointList.at(3) == "1.8");
    QVERIFY(workPointList.at(4) == "1.2");
    QVERIFY(workPointList.at(5) == "1.8");
    QVERIFY(workPointList.at(6) == "1.2");
    QVERIFY(workPointList.at(7) == "1.8");
    QVERIFY(workPointList.at(8) == "0.2");
    QVERIFY(workPointList.at(9) == "0.2");
    QVERIFY(workPointList.at(10) == "0.3");
    QVERIFY(workPointList.at(11) == "0.3");
    QVERIFY(workPointList.at(12) == "0.4");
    QVERIFY(workPointList.at(13) == "0.4");
    mAssertions += 14;

    QVERIFY(workPoint.WorkPoint_SetWorkPoints(workPointList) == true);
    workPointsLoaded = workPoint.WorkPoint_GetWorkPoints();
    QVERIFY(workPointsLoaded.at(0) == 1.8f);
    QVERIFY(workPointsLoaded.at(1) == 2.4f);
    QVERIFY(workPointsLoaded.at(2) == 1.2f);
    QVERIFY(workPointsLoaded.at(3) == 1.8f);
    QVERIFY(workPointsLoaded.at(4) == 1.2f);
    QVERIFY(workPointsLoaded.at(5) == 1.8f);
    QVERIFY(workPointsLoaded.at(6) == 1.2f);
    QVERIFY(workPointsLoaded.at(7) == 1.8f);
    QVERIFY(workPointsLoaded.at(8) == 0.2f);
    QVERIFY(workPointsLoaded.at(9) == 0.2f);
    QVERIFY(workPointsLoaded.at(10) == 0.3f);
    QVERIFY(workPointsLoaded.at(11) == 0.3f);
    QVERIFY(workPointsLoaded.at(12) == 0.4f);
    QVERIFY(workPointsLoaded.at(13) == 0.4f);
    mAssertions += 14;
}

void EduManTest::summaryTestCase()
{
    qDebug() << "\t\t\t\tTotal assertions: " << mAssertions;
    qDebug() << "\t\t\t\tSaving unit test output to file, please wait...";

    QStringList args;
    args << "-o" << "testResults.txt";
    mExporter.start("EduManTests.exe", args);
    mExporter.waitForFinished(8000);

    qDebug() << "\t\t\t\tTest output saved to file 'testResults.txt'.";
}

QTEST_MAIN(EduManTest)

/*MOC file inclusion: -------------------------------------------------------*/
#include "edu_man_tests.moc"
