/**
 * @file        employee.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for class Employee
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef EMPLOYEE_HPP
#define EMPLOYEE_HPP

/*Private includes: ---------------------------------------------------------*/
#include "workroom.hpp"

/*Forward class declaration: ------------------------------------------------*/
class WorkLabel;

/*Class declaration: --------------------------------------------------------*/
class Employee
{
public:
    Employee();
    Employee(QString ID, QString titledName, QString function, QString email, QString privatePhone,
             bool isDoctoral, Workroom workroom);
    ~Employee();

    QString& Employee_GetEmployeeID();
    QString& Employee_GetTitledName();
    QString& Employee_GetFunction();
    QString& Employee_GetEmail();
    QString& Employee_GetPrivatePhone();
    QString Employee_GetIsDoctoral(bool getByID);
    QString& Employee_GetRoom();
    QString& Employee_GetRoomPhone();
    float Employee_GetWorkPoints(QList<QSharedPointer<WorkLabel>> allLabelsList);
    QStringList Employee_GetAll();

private:
    /*!
        * @brief mID (QString) represents unique hash of employee instance
        */
    QString mID;
    /*!
        * @brief mTitledName (QString) defines the name of an employee along
        * with their titles (e.g. "Ing. Petr Novák, Ph. D.")
        */
    QString mTitledName;
    /*!
        * @brief mFunction (QString) defines the role of an employee
        * (e. g. "odborný asistent")
        */
    QString mFunction;
    /*!
        * @brief mEmail (QString) holds informations about e-mail address
        */
    QString mEmail;
    /*!
        * @brief mPrivatePhone (QString) keeps the employee's private phone number
        */
    QString mPrivatePhone;
    /*!
        * @brief mIsDoctoral (bool) decides, whether an employee is a doctoral or not
        */
    bool mIsDoctoral;
    /*!
        * @brief mWorkroom (Workroom) keeps info about a room number and local
        * workroom cell phone number
        */
    Workroom mWorkroom;
};

#endif // EMPLOYEE_HPP
