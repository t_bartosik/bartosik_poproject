/**
 * @file        notificationwindow.hpp
 * @author      Tomas Bartosik
 * @date        01.06.2021
 * @brief       header file for notification window
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef NOTIFICATIONWINDOW_HPP
#define NOTIFICATIONWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QDialog>
#include <QPushButton>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui {class NotificationWindow;}
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class NotificationWindow : public QDialog
{
    Q_OBJECT

public:
    explicit NotificationWindow(QWidget *parent = nullptr, bool connectionStatus = true);
    ~NotificationWindow();

    void NotificationWindow_InitializeUI();
    void NotificationWindow_SetEmployeeContent(QList<QStringList> content);
    void NotificationWindow_SetWorkPointsContent(QList<float> content);
    void NotificationWindow_DrawNotificationContent();
    QPair<QList<int>, bool> NotificationWindow_GetResults();

private slots:
    void NotificationWindow_ClearEmployeeSelection();
    void on_buttonBox_accepted();

private:
    Ui::NotificationWindow *mNotificationUserInterface;
    QList<QStringList> mEmployeeListContent;
    QList<float> mWorkPointsListContent;
    QPair<QList<int>, bool> mResults;
    bool mAppendAttachments;
    bool mConnectionStatus;
};

#endif // NOTIFICATIONWINDOW_HPP
