/**
 * @file        course.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       class Course declaration file
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef COURSE_HPP
#define COURSE_HPP

/*Private includes: ---------------------------------------------------------*/
#include "lesson.hpp"
#include "semester.hpp"
#include "group.hpp"

/*Class declaration: --------------------------------------------------------*/
class Course
{
public:
    Course();
    Course(QString ID, QString courseShortcut, QString courseName, Lesson lesson,
           Semester semester, Language language, QList<QSharedPointer<Group>> groups);
    ~Course();

    QString& Course_GetCourseID();
    QString& Course_GetCourseShortcut();
    QString& Course_GetCourseName();
    int Course_GetLectureCapacity();
    int Course_GetPracticeCapacity();
    int Course_GetSeminarCapacity();
    int Course_GetAtelierCapacity();
    int Course_GetLectureHours();
    int Course_GetPracticeHours();
    int Course_GetSeminarHours();
    int Course_GetAtelierHours();
    int Course_GetWeekCount();
    int Course_GetCredits();
    QString Course_GetSemesterType(bool getByID);
    QString Course_GetEnding(bool getByID);
    QString Course_GetLanguage(bool getByID);
    QList<QSharedPointer<Group>>& Course_GetGroups();
    QStringList Course_GetAll();

    void Course_ResetGroups();
    void Course_AppendGroup(QSharedPointer<Group> group);
    void Course_ReplaceGroup(int index, QSharedPointer<Group> group);

private:
    /*!
        * @brief mID (QString) represents unique hash of course instance
        */
    QString mID;
    /*!
        * @brief mCourseShortcut (QString) defines the shortcut of a course (e.g. "AP7MP")
        */
    QString mCourseShortcut;
    /*!
        * @brief mCourseName (QString) defines the full name of course
        */
    QString mCourseName;
    /*!
        * @brief mLesson (Lesson) holds informations about capacity of lectures,
        * practices, seminars and ateliers
        */
    Lesson mLesson;
    /*!
        * @brief mSemester (Semester) keeps informations about hours, weeks,
        * credits and a way of ending the course
        */
    Semester mSemester;
    /*!
        * @brief mLanguage (Language) is a enum type that keeps informations
        * about course language
        */
    Language mLanguage;
    /*!
        * @brief mGroups (QList<QSharedPointer<Group>>) serves as a list of student groups
        * participating this course, so that a number of all students for
        * such a course can be counted
        */
    QList<QSharedPointer<Group>> mGroups;
};

#endif // COURSE_HPP
