/**
 * @file        dbparser.hpp
 * @author      Tomas Bartosik
 * @date        29.05.2021
 * @brief       declaration file for DB class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef DBPARSER_HPP
#define DBPARSER_HPP

/*Private includes: ---------------------------------------------------------*/
#include "xmlparser.hpp"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSql>

/*Private defines: ----------------------------------------------------------*/
#define TABLES_COUNT 6

#define COURSE_ITEMS 15
#define EMPLOYEE_ITEMS 7
#define WORKLABEL_ITEMS 7
#define GROUP_ITEMS 7

/*Class declaration: --------------------------------------------------------*/
class DbParser
{
public:
    DbParser();
    ~DbParser();

    void DbParser_InitializePath();
    void DbParser_InitializeDb();
    void DbParser_InsertCourse(QSharedPointer<Course> course);
    void DbParser_InsertCourseGroup(QString courseID, QString groupID);
    void DbParser_InsertEmployee(QSharedPointer<Employee> employee);
    void DbParser_InsertWorkLabel(QSharedPointer<WorkLabel> workLabel);
    void DbParser_InsertGroup(QSharedPointer<Group> group);
    void DbParser_UpdateCourseGroup(QString originalHashedID, QString newHashedID);
    void DbParser_UpdateWorkLabel(QString workLabelID, QString courseID, QString employeeID);
    void DbParser_UpdateGroup(QString originalHashedID, QSharedPointer<Group> group);
    void DbParser_DeleteCourses(QStringList removalIdList);
    void DbParser_DeleteCourseGroupsByCourse(QString removalID);
    void DbParser_DeleteCourseGroupsByGroup(QString removalID);
    void DbParser_DeleteEmployees(QStringList removalIdList);
    void DbParser_DeleteWorkLabelsByID(QStringList removalIdList);
    void DbParser_DeleteWorkLabelsByShortcut(QString workLabelShortcut);
    void DbParser_DeleteGroups(QStringList removalIdList);
    QList<QSharedPointer<Course>> DbParser_LoadCourseData();
    QStringList DbParser_LoadCourseGroupsData(QString courseID);
    QList<QSharedPointer<Employee>> DbParser_LoadEmployeeData();
    QList<QSharedPointer<WorkLabel>> DbParser_LoadWorkLabelData();
    QList<QSharedPointer<Group>> DbParser_LoadGroupData();

private:
    QSqlDatabase mDatabase;
    QString mDirPath;
};

#endif // DBPARSER_HPP
