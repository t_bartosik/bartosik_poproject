/**
 * @file        lesson.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for class Lesson
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef LESSON_HPP
#define LESSON_HPP

/*Class declaration: --------------------------------------------------------*/
class Lesson
{
public:
    Lesson();
    Lesson(int lectureCapacity, int practiceCapacity, int seminarCapacity, int atelierCapacity);
    ~Lesson();

    int Lesson_GetLectureCapacity();
    int Lesson_GetPracticeCapacity();
    int Lesson_GetSeminarCapacity();
    int Lesson_GetAtelierCapacity();

private:
    /*!
        * @brief mLectureCapacity (int) holds informations about student capacity for lectures
        */
    int mLectureCapacity;
    /*!
        * @brief mPracticeCapacity (int) keeps info about student capacity for practice classes
        */
    int mPracticeCapacity;
    /*!
        * @brief mSeminarCapacity (int) defines the student capacity for seminars
        */
    int mSeminarCapacity;
    /*!
        * @brief mAtelierCapacity (int) serves as a student capacity meter for atelier classes
        */
    int mAtelierCapacity;
};

#endif // LESSON_HPP
