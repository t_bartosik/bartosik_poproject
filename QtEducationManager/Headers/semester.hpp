/**
 * @file        semester.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for class Semester
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef SEMESTER_HPP
#define SEMESTER_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>
#include <QSharedPointer>
#include <QDebug>

enum class SemesterType
{
    eLS = 1,
    eZS
};

enum class Ending
{
    eZ = 1,
    eKZ,
    eZk,
    eZZk
};

/*Class declaration: --------------------------------------------------------*/
class Semester
{
public:
    Semester();
    Semester(int lectureHours, int practiceHours, int seminarHours, int atelierHours,
             int weekCount, int credits, SemesterType semesterType, Ending ending);
    ~Semester();

    int Semester_GetLectureHours();
    int Semester_GetPracticeHours();
    int Semester_GetSeminarHours();
    int Semester_GetAtelierHours();
    int Semester_GetWeekCount();
    int Semester_GetCredits();
    QString Semester_GetSemesterType(bool getByID);
    QString Semester_GetEnding(bool getByID);

private:
    /*!
        * @brief mLectureHours (int) holds informations about weekly hours of lectures
        */
    int mLectureHours;
    /*!
        * @brief mPracticeHours (int) serves for keeping the weekly hours of practice classes
        */
    int mPracticeHours;
    /*!
        * @brief mSeminarHours (int) keeps information about weekly hours of seminars
        */
    int mSeminarHours;
    /*!
        * @brief mAtelierHours (int) keeps information about weekly hours of atelier classes
        */
    int mAtelierHours;
    /*!
        * @brief mWeekCount (int) keeps status of number of weeks in course
        */
    int mWeekCount;
    /*!
        * @brief mCredits (int) defines the number of credits for a course
        */
    int mCredits;
    /*!
        * @brief mSemesterType (SemesterType) enum that defines the course alignment
        * into semester (e.g. "summer semester" or "winter semester")
        */
    SemesterType mSemesterType;
    /*!
        * @brief mEnding (Ending) enum that describes the means of ending the course
        * (e.g. "zapocet", "klasifikovany zapocet", "zkouska" or "zapocet + zkouska")
        */
    Ending mEnding;
};

#endif // SEMESTER_HPP
