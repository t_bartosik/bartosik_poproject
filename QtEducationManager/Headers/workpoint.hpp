/**
 * @file        workpoint.hpp
 * @author      Tomas Bartosik
 * @date        27.03.2021
 * @brief       declaration file for class WorkPoint
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef WORKPOINT_HPP
#define WORKPOINT_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QList>
#include <QVariant>

/*Class declaration: --------------------------------------------------------*/
class WorkPoint
{
public:
    WorkPoint();
    ~WorkPoint();

    bool WorkPoint_SetWorkPoints(QStringList workPointsLoaded);
    QList<float> WorkPoint_GetWorkPoints();
    float WorkPoint_CalculateWorkPoints(int scheduledHours, int labelType, int courseType);

private:
    /*!
        * @brief mLectureCZ (float) keeps czech lecture work point value
        */
    float mLectureCZ;
    /*!
        * @brief mLectureEN (float) keeps english lecture work point value
        */
    float mLectureEN;
    /*!
        * @brief mPracticeCZ (float) holds value of czech practice work point
        */
    float mPracticeCZ;
    /*!
        * @brief mPracticeEN (float) holds value of english practice work point
        */
    float mPracticeEN;
    /*!
        * @brief mSeminarCZ (float) informs about czech seminar work point value
        */
    float mSeminarCZ;
    /*!
        * @brief mSeminarEN (float) informs about english seminar work point value
        */
    float mSeminarEN;
    /*!
        * @brief mAtelierCZ (float) holds czech atelier work point value
        */
    float mAtelierCZ;
    /*!
        * @brief mAtelierEN (float) holds english atelier work point value
        */
    float mAtelierEN;
    /*!
        * @brief mCreditTestCZ (float) holds the value of credit test work point in czech
        */
    float mCreditTestCZ;
    /*!
        * @brief mCreditTestEN (float) holds the value of credit test work point in english
        */
    float mCreditTestEN;
    /*!
        * @brief mClassifiedCreditTestCZ (float) informs about the classified credit
        * test work point value in czech
        */
    float mClassifiedCreditTestCZ;
    /*!
        * @brief mClassifiedCreditTestEN (float) informs about the classified credit
        * test work point value in english
        */
    float mClassifiedCreditTestEN;
    /*!
        * @brief mExamCZ (float) holds informations about czech exam work point value
        */
    float mExamCZ;
    /*!
        * @brief mExamEN (float) holds informations about english exam work point value
        */
    float mExamEN;
};

#endif // WORKPOINT_HPP
