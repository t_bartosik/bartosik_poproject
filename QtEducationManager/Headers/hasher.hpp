/**
 * @file        hasher.hpp
 * @author      Tomas Bartosik
 * @date        08.04.2021
 * @brief       declaration file for class Hasher
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef HASHER_HPP
#define HASHER_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QString>
#include <QCryptographicHash>

/*Class declaration: --------------------------------------------------------*/
class Hasher
{
public:
    Hasher();
    ~Hasher();

    QString Hasher_CalculateHash(QString dataString);
private:
    QString mHash;
};

#endif // HASHER_HPP
