/**
 * @file        group.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for Group
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef GROUP_HPP
#define GROUP_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QString>
#include "year.hpp"

enum class StudyType
{
    eBc = 1,
    eMgr,
    ePhD
};

enum class StudyForm
{
    eP = 1,
    eK
};

enum class Language
{
    eCZ = 1,
    eEN
};

/*Class declaration: --------------------------------------------------------*/
class Group
{
public:
    Group();
    Group(QString ID, QString studySpecialization, Year year, StudyType studyType,
          StudyForm studyForm, Language language);
    ~Group();

    QString& Group_GetGroupID();
    QString& Group_GetStudySpecialization();
    int Group_GetYear();
    int Group_GetSummerStudentCount();
    int Group_GetWinterStudentCount();
    QString Group_GetStudyType(bool getByID);
    QString Group_GetStudyForm(bool getByID);
    QString Group_GetLanguage(bool getByID);
    QStringList Group_GetAll();

    void Group_SetGroupID(QString groupID);
    void Group_SetSummerStudentCount(int summerStudentCount);
    void Group_SetWinterStudentCount(int winterStudentCount);

private:
    /*!
        * @brief mID (QString) represents unique hash of student group instance
        */
    QString mID;
    /*!
        * @brief mStudySpecialization (QString) keeps informations about a student group
        * study specialization (e.g. "SWI")
        */
    QString mStudySpecialization;
    /*!
        * @brief mYear (Year) keeps informations about current student group year
        * and summer & winter semester total student count
        */
    Year mYear;
    /*!
        * @brief mStudyType (StudyType) is an enumeration keeping info about study
        * type (e.g. "Bc." or "Mgr.")
        */
    StudyType mStudyType;
    /*!
        * @brief mStudyForm (StudyForm) is an enum that decides the form
        * of studying (e.g. "prezencni", "kombinovane")
        */
    StudyForm mStudyForm;
    /*!
        * @brief mLanguage (Language) is an enum that serves for keeping
        * the language information (e.g. "CZ" or "EN")
        */
    Language mLanguage;
};

#endif // GROUP_HPP
