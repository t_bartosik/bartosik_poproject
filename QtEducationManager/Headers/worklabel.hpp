/**
 * @file        worklabel.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for WorkLabel class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef WORKLABEL_HPP
#define WORKLABEL_HPP

/*Private includes: ---------------------------------------------------------*/
#include "course.hpp"
#include "employee.hpp"

enum class LabelType
{
    eLecture = 1,
    ePractice,
    eSeminar,
    eAtelier,
    eZ,
    eKZ,
    eZk
};

enum class CourseType
{
    eStandard = 1,
    eForeignLanguage,
    eDoctoral
};

/*Class declaration: --------------------------------------------------------*/
class WorkLabel
{
public:
    WorkLabel();
    WorkLabel(QString ID, QString labelName, int weekCount, int scheduledHours,
              int studentCount, float workPoints, LabelType labelType,
              CourseType courseType, QSharedPointer<Employee> employee, QSharedPointer<Course> course);
    ~WorkLabel();

    QString& WorkLabel_GetWorkLabelID();
    QString& WorkLabel_GetLabelName();
    int WorkLabel_GetWeekCount();
    int WorkLabel_GetScheduledHours();
    int WorkLabel_GetStudentCount();
    float WorkLabel_GetWorkPoints();
    QString WorkLabel_GetLabelType(bool getByID);
    QString WorkLabel_GetCourseType(bool getByID);
    QSharedPointer<Employee> WorkLabel_GetEmployee();
    QSharedPointer<Course> WorkLabel_GetCourse();
    QStringList WorkLabel_GetAll();

    void WorkLabel_SetEmployee(QSharedPointer<Employee> employee);
    void WorkLabel_SetCourse(QSharedPointer<Course> course);

private:
    /*!
        * @brief mID (QString) represents unique hash of work label instance
        */
    QString mID;
    /*!
        * @brief mLabelName (QString) defines the name of a work label
        */
    QString mLabelName;
    /*!
        * @brief mWeekCount (int) describes the number of weeks for a
        * work label
        */
    int mWeekCount;
    /*!
        * @brief mScheduledHours (int) keeps info about total number of hours
        */
    int mScheduledHours;
    /*!
        * @brief mStudentCount (int) defines the number of students for a
        * work label
        */
    int mStudentCount;
    /*!
        * @brief mWorkPoints (float) holds the value of work label in work points
        */
    float mWorkPoints;
    /*!
        * @brief mLabelType (LabelType) is an enum for definition of label type
        * (e.g. "prednaska", "cviceni" etc..)
        */
    LabelType mLabelType;
    /*!
        * @brief mCourseType (CourseType) enum for deciding of a course type
        * (e.g. "standard course", "doctoral course" or "foreign language")
        */
    CourseType mCourseType;
    /*!
        * @brief mEmployee (QSharedPointer<Employee>) is a reference to an employee, who owns
        * the work label -> can be null (unassigned)
        */
    QSharedPointer<Employee> mEmployee;
    /*!
        * @brief mCourse (QSharedPointer<Course>) is used in order to match a work label with
        * existing course -> can be null (unassigned)
        */
    QSharedPointer<Course> mCourse;
};

#endif // WORKLABEL_HPP
