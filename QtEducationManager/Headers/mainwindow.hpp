/**
 * @file        mainwindow.hpp
 * @author      Tomas Bartosik
 * @date        26.02.2021
 * @brief       main declaration file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include "dbparser.hpp"
#include "workpoint.hpp"
#include "hasher.hpp"
#include "dialogwindow.hpp"
#include "notificationwindow.hpp"
#include "aboutwindow.hpp"
#include "../Libraries/SmtpClient/smtp.hpp"
#include <QMainWindow>
#include <QSettings>
#include <QTranslator>
#include <QScrollBar>
#include <QComboBox>
#include <QRegularExpression>
#include <QTcpSocket>
#include <algorithm>
#include <cfloat>

/*Private defines: ----------------------------------------------------------*/
#define ALL_TABLES_ID 0
#define COURSE_TABLE_ID 1
#define EMPLOYEE_TABLE_ID 2
#define WORKLABEL_TABLE_ID 3
#define GROUP_TABLE_ID 4

#define CLEAR_LABELS 0
#define ASSIGN_LABELS 1

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void MainWindow_InitializeSettings();
    void MainWindow_InitializeUI();
    void MainWindow_ResizeTables(unsigned int tableID);
    void resizeEvent(QResizeEvent *event);
    void MainWindow_DrawTableContent(unsigned int tableID);
    void MainWindow_AddTableRow(unsigned int tableID);
    QString MainWindow_CheckTableRow(unsigned int tableID, QStringList newEntry);
    void MainWindow_RemoveTableRow(unsigned int tableID);
    void MainWindow_UpdateTableRow();
    void MainWindow_ConnectData(unsigned int source);
    QList<int> MainWindow_StratifyStudents(int enlistedStudents, int capacity);
    void MainWindow_AutogenerateLabels(int selectedItem, QStringList results);
    void MainWindow_ProcessLabels(unsigned int source);
    void MainWindow_FilterLabels(int filterID);

private slots:
    void MainWindow_EnableCourses();
    void MainWindow_EnableEmployees();
    void MainWindow_EnableWorkLabels();
    void MainWindow_EnableGroups();
    void MainWindow_EnableNotifications();
    void MainWindow_EnableAbout();
    void MainWindow_UsingXmlFiles();
    void MainWindow_UsingDatabase();
    void MainWindow_TranslateIntoCzech();
    void MainWindow_TranslateIntoEnglish();

protected:
    QTranslator *mTranslator;

private:
    Ui::MainWindow *mUserInterface;

    QList<QSharedPointer<Course>> mCourseList;
    QList<QSharedPointer<Employee>> mEmployeeList;
    QList<QSharedPointer<WorkLabel>> mWorkLabelList;
    QList<QSharedPointer<Group>> mGroupList;

    QSharedPointer<Course> mCourse;
    QSharedPointer<Employee> mEmployee;
    QSharedPointer<WorkLabel> mWorkLabel;
    QSharedPointer<Group> mGroup;

    QStringList mWorkLabelListFilter;
    XmlParser mXmlParser;
    DbParser mDbParser;
    WorkPoint mWorkPoint;
    Hasher mHasher;
    bool mEditingEnabled;
    bool mUpdatingEnabled;
    int mUpdatedItem;
};

#endif // MAINWINDOW_HPP
