/**
 * @file        year.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       main declaration file for Year
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef YEAR_HPP
#define YEAR_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QMainWindow>

/*Class declaration: --------------------------------------------------------*/
class Year
{
public:
    Year();
    Year(int year, int summerStudentCount, int winterStudentCount);
    ~Year();

    int Year_GetYear();
    int Year_GetSummerStudentCount();
    int Year_GetWinterStudentCount();

    void Year_SetSummerStudentCount(int summerStudentCount);
    void Year_SetWinterStudentCount(int winterStudentCount);

private:
    /*!
        * @brief mYear (int) holds the study year information
        */
    int mYear;
    /*!
        * @brief mSummerStudentCount (int) holds the number of summer
        * semester students
        */
    int mSummerStudentCount;
    /*!
        * @brief mWinterStudentCount (int) defines the number of winter
        * semester students
        */
    int mWinterStudentCount;
};

#endif // YEAR_HPP
