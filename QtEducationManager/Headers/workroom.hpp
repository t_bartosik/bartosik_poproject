/**
 * @file        workroom.hpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       declaration file for class Workroom
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef WORKROOM_HPP
#define WORKROOM_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QString>
#include <QSharedPointer>

/*Class declaration: --------------------------------------------------------*/
class Workroom
{
public:
    Workroom();
    Workroom(QString room, QString roomPhone);
    ~Workroom();

    QString& Workroom_GetRoom();
    QString& Workroom_GetRoomPhone();

private:
    /*!
        * @brief mRoom (QString) keeps info about a room number
        * ("e.g. 51/206")
        */
    QString mRoom;
    /*!
        * @brief mRoomPhone (QString) holds the cell phone number of
        * a workroom
        */
    QString mRoomPhone;
};

#endif // WORKROOM_HPP
