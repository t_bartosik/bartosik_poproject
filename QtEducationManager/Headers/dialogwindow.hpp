/**
 * @file        dialogwindow.hpp
 * @author      Tomas Bartosik
 * @date        08.04.2021
 * @brief       header file for Education Manager dialogs
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef DIALOGWINDOW_HPP
#define DIALOGWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QDialog>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>

/*Private defines: ----------------------------------------------------------*/
#define GROUP_MANAGEMENT 0
#define WORKLABEL_MANAGEMENT 1
#define EMPLOYEE_MANAGEMENT 2

QT_BEGIN_NAMESPACE
namespace Ui {class DialogWindow;}
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class DialogWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DialogWindow(QWidget *parent = nullptr, unsigned int source = 0);
    ~DialogWindow();

    void DialogWindow_InitializeUI();
    void DialogWindow_SetGroupContent(QList<QStringList> content);
    void DialogWindow_SetCourseContent(QList<QStringList> content);
    void DialogWindow_SetEmployeeContent(QList<QStringList> content);
    void DialogWindow_DrawGroupContent(QStringList currentIDList);
    void DialogWindow_DrawCourseContent(QString currentID);
    void DialogWindow_DrawEmployeeContent(QString currentID);
    QStringList DialogWindow_GetResults(unsigned int source);

private slots:
    void DialogWindow_ClearGroupSelection();
    void DialogWindow_ClearWorkLabelSelection();
    void on_buttonBox_accepted();

private:
    Ui::DialogWindow *mDialogUserInterface;
    QList<QStringList> mGroupListContent;
    QList<QStringList> mCourseListContent;
    QList<QStringList> mEmployeeListContent;
    QStringList mGroupListResults;
    QStringList mWorkLabelResults;
    unsigned int mSource;
};

#endif // DIALOGWINDOW_HPP
