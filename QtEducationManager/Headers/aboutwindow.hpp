/**
 * @file        aboutwindow.hpp
 * @author      Tomas Bartosik
 * @date        03.06.2021
 * @brief       header file for AboutWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef ABOUTWINDOW_HPP
#define ABOUTWINDOW_HPP

/*Private includes: ---------------------------------------------------------*/
#include <QDialog>
#include <QPushButton>
#include <QPixmap>

QT_BEGIN_NAMESPACE
namespace Ui {class AboutWindow;}
QT_END_NAMESPACE

/*Class declaration: --------------------------------------------------------*/
class AboutWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AboutWindow(QWidget *parent = nullptr);
    ~AboutWindow();

private:
    Ui::AboutWindow *mAboutUserInterface;
};

#endif // ABOUTWINDOW_HPP
