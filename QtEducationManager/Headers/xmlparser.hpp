/**
 * @file        xmlparser.hpp
 * @author      Tomas Bartosik
 * @date        13.03.2021
 * @brief       declaration file for XML processing class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

#ifndef XMLPARSER_HPP
#define XMLPARSER_HPP

/*Private includes: ---------------------------------------------------------*/
#include "group.hpp"
#include "employee.hpp"
#include "worklabel.hpp"
#include <QDir>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QMessageBox>

/*Class declaration: --------------------------------------------------------*/
class XmlParser
{
public:
    XmlParser();
    ~XmlParser();

    void XmlParser_InitializePath();
    void XmlParser_InitializeWorkPointData();
    QStringList XmlParser_LoadWorkPointData();
    void XmlParser_InitializeCourseData();
    void XmlParser_InitializeEmployeeData();
    void XmlParser_InitializeWorkLabelData();
    void XmlParser_InitializeGroupData();
    QList<QSharedPointer<Course>> XmlParser_LoadCourseData();
    QList<QSharedPointer<Employee>> XmlParser_LoadEmployeeData();
    QList<QSharedPointer<WorkLabel>> XmlParser_LoadWorkLabelData();
    QList<QSharedPointer<Group>> XmlParser_LoadGroupData();
    void XmlParser_SaveCourseData(QList<QSharedPointer<Course>> course);
    void XmlParser_SaveEmployeeData(QList<QSharedPointer<Employee>> employee);
    QString XmlParser_SaveEmployeeStatus(QList<QSharedPointer<WorkLabel>> workLabel, QString email);
    void XmlParser_SaveWorkLabelData(QList<QSharedPointer<WorkLabel>> workLabel);
    void XmlParser_SaveGroupData(QList<QSharedPointer<Group>> group);

private:
    QString mDirPath;
};

#endif // XMLPARSER_HPP
