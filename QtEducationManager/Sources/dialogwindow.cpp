/**
 * @file        dialogwindow.cpp
 * @author      Tomas Bartosik
 * @date        08.04.2021
 * @brief       implementation file for Education Manager dialog window
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/dialogwindow.hpp"
#include "ui_dialogwindow.h"

/*Class definition: ---------------------------------------------------------*/
DialogWindow::DialogWindow(QWidget *parent, unsigned int source) : QDialog(parent), mDialogUserInterface(new Ui::DialogWindow),
    mGroupListContent(QList<QStringList> ()), mCourseListContent(QList<QStringList> ()), mEmployeeListContent(QList<QStringList> ()),
    mGroupListResults(QStringList()), mWorkLabelResults(QStringList())
{
    mDialogUserInterface->setupUi(this);
    mSource = source;
    DialogWindow_InitializeUI();

    connect(mDialogUserInterface->groupClearButton, &QPushButton::clicked, this, &DialogWindow::DialogWindow_ClearGroupSelection);
    connect(mDialogUserInterface->worklabelClearButton, &QPushButton::clicked, this, &DialogWindow::DialogWindow_ClearWorkLabelSelection);
}
DialogWindow::~DialogWindow()
{
    delete mDialogUserInterface;
}

void DialogWindow::DialogWindow_InitializeUI()
{
    if(mSource == GROUP_MANAGEMENT)
    {
        mDialogUserInterface->workLabelManagement_widget->hide();
        mDialogUserInterface->groupManagement_widget->show();
        mDialogUserInterface->groupList_widget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    }
    else if(mSource == WORKLABEL_MANAGEMENT)
    {
        mDialogUserInterface->groupManagement_widget->hide();
        mDialogUserInterface->workLabelManagement_widget->show();
        mDialogUserInterface->courseList_widget->setSelectionMode(QAbstractItemView::SingleSelection);
        mDialogUserInterface->descriptionLabel->setText(tr("Description: L = lecture, P = practice, S = seminar, A = atelier"));
    }
    mDialogUserInterface->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Confirm assignment"));
    mDialogUserInterface->buttonBox->button(QDialogButtonBox::Cancel)->setText(tr("Cancel"));
}

void DialogWindow::DialogWindow_SetGroupContent(QList<QStringList> content){mGroupListContent = content;}

void DialogWindow::DialogWindow_SetCourseContent(QList<QStringList> content){mCourseListContent = content;}

void DialogWindow::DialogWindow_SetEmployeeContent(QList<QStringList> content){mEmployeeListContent = content;}

void DialogWindow::DialogWindow_DrawGroupContent(QStringList currentIDList)
{
    QString rowItems;
    QList<int> groupIDList;
    for(int row = 0; row < mGroupListContent.count(); row++)
    {
        rowItems = QString(tr("%1) %2, %3. year (%4, %5)\n    summer semester students: %6, winter semester students: %7"))
                .arg(row + 1)
                .arg(mGroupListContent.at(row).at(1))
                .arg(mGroupListContent.at(row).at(2))
                .arg(mGroupListContent.at(row).at(5))
                .arg(mGroupListContent.at(row).at(6))
                .arg(mGroupListContent.at(row).at(3))
                .arg(mGroupListContent.at(row).at(4));
        mDialogUserInterface->groupList_widget->addItem(rowItems);
        if(currentIDList.isEmpty() == false)
        {
            for(int item = 0; item < currentIDList.count(); item++)
            {
                if(QString::compare(mGroupListContent.at(row).at(0), currentIDList.at(item), Qt::CaseSensitive) == 0)
                {
                    groupIDList << row;
                    currentIDList.removeAt(item);
                }
            }
        }
    }
    for(int item = 0; item < groupIDList.count(); item++)
    {
        mDialogUserInterface->groupList_widget->setItemSelected(mDialogUserInterface->groupList_widget->item(groupIDList.at(item)), true);
    }
}

void DialogWindow::DialogWindow_DrawCourseContent(QString currentID)
{
    QString rowItems;
    int courseID = -1;
    for(int row = 0; row < mCourseListContent.count(); row++)
    {
        rowItems = QString(tr("%1) %2 (%3)\n    course ending: %4 (credits: %5)\n    language: %6, scheduled weeks: %7\n    "
                              "semester: %8\n    scheduled hours  ~  L: %9    P: %10    S: %11    A: %12\n    "
                              "course limits  ~  L: %13    P: %14    S: %15    A: %16"))
                .arg(row + 1)
                .arg(mCourseListContent.at(row).at(2))
                .arg(mCourseListContent.at(row).at(1))
                .arg(mCourseListContent.at(row).at(14))
                .arg(mCourseListContent.at(row).at(12))
                .arg(mCourseListContent.at(row).at(15))
                .arg(mCourseListContent.at(row).at(11))
                .arg(mCourseListContent.at(row).at(13))
                .arg(mCourseListContent.at(row).at(7))
                .arg(mCourseListContent.at(row).at(8))
                .arg(mCourseListContent.at(row).at(9))
                .arg(mCourseListContent.at(row).at(10))
                .arg(mCourseListContent.at(row).at(3))
                .arg(mCourseListContent.at(row).at(4))
                .arg(mCourseListContent.at(row).at(5))
                .arg(mCourseListContent.at(row).at(6));
        mDialogUserInterface->courseList_widget->addItem(rowItems);
        if(currentID != QString())
        {
            if(QString::compare(mCourseListContent.at(row).at(0), currentID, Qt::CaseSensitive) == 0)
            {
                courseID = row;
            }
        }
    }
    if(courseID != -1)
    {
        mDialogUserInterface->courseList_widget->setItemSelected(mDialogUserInterface->courseList_widget->item(courseID), true);
    }
}

void DialogWindow::DialogWindow_DrawEmployeeContent(QString currentID)
{
    QString rowItems;
    int employeeID = -1;
    for(int row = 0; row < mEmployeeListContent.count(); row++)
    {
        rowItems = QString(tr("%1) %2 (%3)\n    e-mail: %4\n    workroom: %5    doctoral: %6"))
                .arg(row + 1)
                .arg(mEmployeeListContent.at(row).at(1))
                .arg(mEmployeeListContent.at(row).at(2))
                .arg(mEmployeeListContent.at(row).at(3))
                .arg(mEmployeeListContent.at(row).at(6))
                .arg(mEmployeeListContent.at(row).at(5));
        mDialogUserInterface->employeeList_widget->addItem(rowItems);
        if(currentID != QString())
        {
            if(QString::compare(mEmployeeListContent.at(row).at(0), currentID, Qt::CaseSensitive) == 0)
            {
                employeeID = row;
            }
        }
    }
    if(employeeID != -1)
    {
        mDialogUserInterface->employeeList_widget->setItemSelected(mDialogUserInterface->employeeList_widget->item(employeeID), true);
    }
}

QStringList DialogWindow::DialogWindow_GetResults(unsigned int source)
{
    if(source == GROUP_MANAGEMENT) return mGroupListResults;
    else if(source == WORKLABEL_MANAGEMENT) return mWorkLabelResults;
    return QStringList();
}

void DialogWindow::DialogWindow_ClearGroupSelection()
{
    mDialogUserInterface->groupList_widget->clearSelection();
}

void DialogWindow::DialogWindow_ClearWorkLabelSelection()
{
    mDialogUserInterface->courseList_widget->clearSelection();
    mDialogUserInterface->employeeList_widget->clearSelection();
}

void DialogWindow::on_buttonBox_accepted()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Assignment error"));
    if(mSource == GROUP_MANAGEMENT)
    {
        QList<QListWidgetItem*> selection = mDialogUserInterface->groupList_widget->selectedItems();
        std::sort(selection.begin(), selection.end(), std::greater<QListWidgetItem*>());
        for(int index = 0; index < selection.size(); index++)
        {
            mGroupListResults << QString::number(mDialogUserInterface->groupList_widget->row(selection.at(index)));
        }
    }
    else if(mSource == WORKLABEL_MANAGEMENT)
    {
        if(mDialogUserInterface->courseList_widget->selectedItems().isEmpty() == false)
        {
            QListWidgetItem *courseSelection = mDialogUserInterface->courseList_widget->selectedItems().first();
            mWorkLabelResults << QString::number(mDialogUserInterface->courseList_widget->row(courseSelection));
        }
        if(mDialogUserInterface->employeeList_widget->selectedItems().isEmpty() == false)
        {
            QListWidgetItem *employeeSelection = mDialogUserInterface->employeeList_widget->selectedItems().first();
            mWorkLabelResults << QString::number(mDialogUserInterface->employeeList_widget->row(employeeSelection));
        }

        if(mWorkLabelResults.count() == 1)
        {
            mWorkLabelResults.clear();
            errMsg.setText(tr("Both a course and an employee have to be selected or none at all."));
            errMsg.exec();
        }
    }
}
