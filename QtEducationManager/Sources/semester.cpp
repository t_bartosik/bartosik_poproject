/**
 * @file        semester.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for class Semester
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/semester.hpp"

/*Class definition: ---------------------------------------------------------*/
Semester::Semester() : mLectureHours(0), mPracticeHours(0), mSeminarHours(0),
    mAtelierHours(0), mWeekCount(0), mCredits(0), mSemesterType(SemesterType::eLS), mEnding(Ending::eZ){}
Semester::Semester(int lectureHours, int practiceHours, int seminarHours, int atelierHours,
                   int weekCount, int credits, SemesterType semesterType, Ending ending) : mLectureHours(lectureHours),
    mPracticeHours(practiceHours), mSeminarHours(seminarHours), mAtelierHours(atelierHours),
    mWeekCount(weekCount), mCredits(credits), mSemesterType(semesterType), mEnding(ending){}
Semester::~Semester(){}

int Semester::Semester_GetLectureHours() {return mLectureHours;}
int Semester::Semester_GetPracticeHours() {return mPracticeHours;}
int Semester::Semester_GetSeminarHours() {return mSeminarHours;}
int Semester::Semester_GetAtelierHours() {return mAtelierHours;}
int Semester::Semester_GetWeekCount() {return mWeekCount;}
int Semester::Semester_GetCredits() {return mCredits;}
QString Semester::Semester_GetSemesterType(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mSemesterType));

    switch(static_cast<int>(mSemesterType))
    {
    case 1: return QObject::tr("summer semester");
    case 2: return QObject::tr("winter semester");
    default: return QString();
    }
}
QString Semester::Semester_GetEnding(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mEnding));

    switch(static_cast<int>(mEnding))
    {
    case 1: return QObject::tr("credit test");
    case 2: return QObject::tr("classified credit test");
    case 3: return QObject::tr("exam");
    case 4: return QObject::tr("credit test & exam");
    default: return QString();
    }
}
