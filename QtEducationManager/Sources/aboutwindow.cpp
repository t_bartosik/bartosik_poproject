/**
 * @file        aboutwindow.cpp
 * @author      Tomas Bartosik
 * @date        03.06.2021
 * @brief       definition file for class AboutWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/aboutwindow.hpp"
#include "ui_aboutwindow.h"

/*Class definition: ---------------------------------------------------------*/
AboutWindow::AboutWindow(QWidget *parent) : QDialog(parent),
    mAboutUserInterface(new Ui::AboutWindow)
{
    mAboutUserInterface->setupUi(this);

    mAboutUserInterface->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Close"));
    mAboutUserInterface->buttonBox->button(QDialogButtonBox::Cancel)->setVisible(false);

    QPixmap image;
    if(image.load(":/images/EM_logo_round_small.png"))
    {
        mAboutUserInterface->aboutImage->setPixmap(image);
    }

    mAboutUserInterface->aboutVersion->setText("ver. " + QApplication::applicationVersion());
    mAboutUserInterface->aboutFooter->setText(tr("Education Manager ©2021, Tomáš Bartošík, TBU in Zlin"));
}

AboutWindow::~AboutWindow()
{
    delete mAboutUserInterface;
}
