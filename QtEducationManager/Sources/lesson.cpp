/**
 * @file        lesson.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       main definition file for Lesson
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/lesson.hpp"

/*Class definition: ---------------------------------------------------------*/
Lesson::Lesson() : mLectureCapacity(0), mPracticeCapacity(0), mSeminarCapacity(0), mAtelierCapacity(0){}
Lesson::Lesson(int lectureCapacity, int practiceCapacity, int seminarCapacity, int atelierCapacity)
    : mLectureCapacity(lectureCapacity), mPracticeCapacity(practiceCapacity),
      mSeminarCapacity(seminarCapacity), mAtelierCapacity(atelierCapacity){}
Lesson::~Lesson(){}

int Lesson::Lesson_GetLectureCapacity() {return mLectureCapacity;}
int Lesson::Lesson_GetPracticeCapacity() {return mPracticeCapacity;}
int Lesson::Lesson_GetSeminarCapacity() {return mSeminarCapacity;}
int Lesson::Lesson_GetAtelierCapacity() {return mAtelierCapacity;}
