/**
 * @file        employee.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for class Employee
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/employee.hpp"
#include "../Headers/worklabel.hpp"

/*Class definition: ---------------------------------------------------------*/
Employee::Employee() : mID(QString()), mTitledName(QString()), mFunction(QString()), mEmail(QString()),
    mPrivatePhone(QString()), mIsDoctoral(false), mWorkroom(){}
Employee::Employee(QString ID, QString titledName, QString function, QString email, QString phone,
                   bool isDoctoral, Workroom workroom)
    : mID(ID), mTitledName(titledName), mFunction(function), mEmail(email), mPrivatePhone(phone),
      mIsDoctoral(isDoctoral), mWorkroom(workroom){}
Employee::~Employee(){}

QString& Employee::Employee_GetEmployeeID() {return mID;}
QString& Employee::Employee_GetTitledName() {return mTitledName;}
QString& Employee::Employee_GetFunction() {return mFunction;}
QString& Employee::Employee_GetEmail() {return mEmail;}
QString& Employee::Employee_GetPrivatePhone() {return mPrivatePhone;}
QString Employee::Employee_GetIsDoctoral(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mIsDoctoral));

    switch(static_cast<int>(mIsDoctoral))
    {
    case 0: return QObject::tr("No"); break;
    case 1: default: return QObject::tr("Yes"); break;
    }
}
QString& Employee::Employee_GetRoom() {return mWorkroom.Workroom_GetRoom();}
QString& Employee::Employee_GetRoomPhone() {return mWorkroom.Workroom_GetRoomPhone();}

float Employee::Employee_GetWorkPoints(QList<QSharedPointer<WorkLabel>> allLabelsList)
{
    float allPoints = 0.0f;
    for(int index = 0; index < allLabelsList.count(); index++)
    {
        if(allLabelsList.at(index)->WorkLabel_GetEmployee() != nullptr)
        {
            if(QString::compare(allLabelsList.at(index)->WorkLabel_GetEmployee()->Employee_GetEmployeeID(),
               this->Employee_GetEmployeeID(), Qt::CaseSensitive) == 0)
            {
                allPoints = allPoints + allLabelsList.at(index)->WorkLabel_GetWorkPoints();
            }
        }
    }
    return allPoints;
}

QStringList Employee::Employee_GetAll()
{
    return {mID, mTitledName, mFunction, mEmail, mPrivatePhone, Employee_GetIsDoctoral(false),
    mWorkroom.Workroom_GetRoom(), mWorkroom.Workroom_GetRoomPhone()};
}

