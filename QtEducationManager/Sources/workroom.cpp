/**
 * @file        workroom.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for class Workroom
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/workroom.hpp"

/*Class definition: ---------------------------------------------------------*/
Workroom::Workroom() : mRoom(QString()), mRoomPhone(QString()){}
Workroom::Workroom(QString room, QString phone) : mRoom(room), mRoomPhone(phone){}
Workroom::~Workroom(){}

QString& Workroom::Workroom_GetRoom() {return mRoom;}
QString& Workroom::Workroom_GetRoomPhone() {return mRoomPhone;}
