/**
 * @file        main.cpp
 * @author      Tomas Bartosik
 * @date        26.02.2021
 * @brief       main file for Education Manager
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/mainwindow.hpp"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("Education Manager");
    QApplication::setOrganizationName("utb.cz");
    QApplication::setApplicationVersion("1.0");

    QFile styleFile(":/styles/stylesheet.qss");
    styleFile.open(QFile::ReadOnly);
    QString fileSheet = styleFile.readAll();
    qApp->setStyleSheet(fileSheet);

    MainWindow window;
    window.setWindowTitle("Education Manager");
    window.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    window.setFixedWidth(1280);
    window.setMaximumWidth(QWIDGETSIZE_MAX);
    window.setMinimumWidth(1200);
    window.setMinimumHeight(600);
    window.show();

    return app.exec();
}
