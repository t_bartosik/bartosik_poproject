/**
 * @file        group.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for class Group
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/group.hpp"

/*Class definition: ---------------------------------------------------------*/
Group::Group() : mID(QString()), mStudySpecialization(QString()), mYear(0, 0, 0), mStudyType(StudyType::eBc),
    mStudyForm(StudyForm::eP), mLanguage(Language::eCZ){}
Group::Group(QString ID, QString studySpecialization, Year year, StudyType studyType, StudyForm studyForm,
             Language language) : mID(ID), mStudySpecialization(studySpecialization), mYear(year),
    mStudyType(studyType), mStudyForm(studyForm), mLanguage(language){}
Group::~Group(){}

QString& Group::Group_GetGroupID() {return mID;}
QString& Group::Group_GetStudySpecialization() {return mStudySpecialization;}
int Group::Group_GetYear() {return mYear.Year_GetYear();}
int Group::Group_GetSummerStudentCount() {return mYear.Year_GetSummerStudentCount();}
int Group::Group_GetWinterStudentCount() {return mYear.Year_GetWinterStudentCount();}
QString Group::Group_GetStudyType(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mStudyType));

    switch(static_cast<int>(mStudyType))
    {
    case 1: return QObject::tr("Bc.");
    case 2: return QObject::tr("Mgr.");
    case 3: return QObject::tr("Ph. D.");
    default: return QString();
    }
}
QString Group::Group_GetStudyForm(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mStudyForm));

    switch(static_cast<int>(mStudyForm))
    {
    case 1: return QObject::tr("full-time");
    case 2: return QObject::tr("combined");
    default: return QString();
    }
}
QString Group::Group_GetLanguage(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mLanguage));

    switch(static_cast<int>(mLanguage))
    {
    case 1: return QObject::tr("CZ");
    case 2: return QObject::tr("EN");
    default: return QString();
    }
}
QStringList Group::Group_GetAll()
{
    return {mID, mStudySpecialization, QString::number(mYear.Year_GetYear()), QString::number(mYear.Year_GetSummerStudentCount()),
                QString::number(mYear.Year_GetWinterStudentCount()), Group_GetStudyType(false), Group_GetStudyForm(false), Group_GetLanguage(false)};
}

void Group::Group_SetGroupID(QString groupID){mID = groupID;}
void Group::Group_SetSummerStudentCount(int summerStudentCount) {mYear.Year_SetSummerStudentCount(summerStudentCount);}
void Group::Group_SetWinterStudentCount(int winterStudentCount) {mYear.Year_SetWinterStudentCount(winterStudentCount);}

