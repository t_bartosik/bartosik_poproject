/**
 * @file        notificationwindow.cpp
 * @author      Tomas Bartosik
 * @date        01.06.2021
 * @brief       definition file for Education Manager notification window
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/notificationwindow.hpp"
#include "ui_notificationwindow.h"

/*Class definition: ---------------------------------------------------------*/
NotificationWindow::NotificationWindow(QWidget *parent, bool connectionStatus) : QDialog(parent),
    mNotificationUserInterface(new Ui::NotificationWindow), mEmployeeListContent(QList<QStringList> ()),
    mWorkPointsListContent(QList<float> ()), mResults(QPair<QList<int>, bool> ()), mAppendAttachments(false),
    mConnectionStatus(connectionStatus)
{
    mNotificationUserInterface->setupUi(this);
    NotificationWindow_InitializeUI();

    connect(mNotificationUserInterface->notificationClearButton, &QPushButton::clicked, this, &NotificationWindow::NotificationWindow_ClearEmployeeSelection);
}

NotificationWindow::~NotificationWindow()
{
    delete mNotificationUserInterface;
}

void NotificationWindow::NotificationWindow_InitializeUI()
{
    mNotificationUserInterface->employeeList_widget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mNotificationUserInterface->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Send e-mail(s)"));
    mNotificationUserInterface->buttonBox->button(QDialogButtonBox::Cancel)->setText(tr("Cancel"));


    if(mConnectionStatus == false)
    {
        mNotificationUserInterface->connectionLabel->setText(tr("Internet connection unavailable."));
        mNotificationUserInterface->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void NotificationWindow::NotificationWindow_SetEmployeeContent(QList<QStringList> content){mEmployeeListContent = content;}
void NotificationWindow::NotificationWindow_SetWorkPointsContent(QList<float> content){mWorkPointsListContent = content;}

void NotificationWindow::NotificationWindow_DrawNotificationContent()
{
    QString rowItems;
    for(int row = 0; row < mEmployeeListContent.count(); row++)
    {
        rowItems = QString(tr("%1) %2 (%3)\n    e-mail: %4\n    workroom: %5    doctoral: %6    work points: %7"))
                .arg(row + 1)
                .arg(mEmployeeListContent.at(row).at(1))
                .arg(mEmployeeListContent.at(row).at(2))
                .arg(mEmployeeListContent.at(row).at(3))
                .arg(mEmployeeListContent.at(row).at(6))
                .arg(mEmployeeListContent.at(row).at(5))
                .arg(mWorkPointsListContent.at(row));
        mNotificationUserInterface->employeeList_widget->addItem(rowItems);
    }
}

QPair<QList<int>, bool> NotificationWindow::NotificationWindow_GetResults(){return mResults;}

void NotificationWindow::NotificationWindow_ClearEmployeeSelection()
{
    mNotificationUserInterface->employeeList_widget->clearSelection();
}

void NotificationWindow::on_buttonBox_accepted()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Assignment error"));
    errMsg.setText(tr("At least one recipient has to be selected."));

    if(mNotificationUserInterface->employeeList_widget->selectedItems().isEmpty() == false)
    {
        QList<QListWidgetItem*> selection = mNotificationUserInterface->employeeList_widget->selectedItems();
        std::sort(selection.begin(), selection.end(), std::greater<QListWidgetItem*>());
        for(int index = 0; index < selection.size(); index++)
        {
            mResults.first.append(mNotificationUserInterface->employeeList_widget->row(selection.at(index)));
        }
        mResults.second = mNotificationUserInterface->attachmentCheckBox->isChecked();
    }
    else errMsg.exec();
}

