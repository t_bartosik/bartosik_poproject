/**
 * @file        year.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for class Year
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/year.hpp"

/*Class definition: ---------------------------------------------------------*/
Year::Year() : mYear(0), mSummerStudentCount(0), mWinterStudentCount(0){}
Year::Year(int year, int summerStudentCount, int winterStudentCount) : mYear(year),
    mSummerStudentCount(summerStudentCount), mWinterStudentCount(winterStudentCount){}
Year::~Year(){}

int Year::Year_GetYear() {return mYear;}
int Year::Year_GetSummerStudentCount() {return mSummerStudentCount;}
int Year::Year_GetWinterStudentCount() {return mWinterStudentCount;}

void Year::Year_SetSummerStudentCount(int summerStudentCount) {mSummerStudentCount = summerStudentCount;}
void Year::Year_SetWinterStudentCount(int winterStudentCount) {mWinterStudentCount = winterStudentCount;}
