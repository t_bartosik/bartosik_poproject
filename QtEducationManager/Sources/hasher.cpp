/**
 * @file        hasher.cpp
 * @author      Tomas Bartosik
 * @date        08.04.2021
 * @brief       definition file for class Hasher
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/hasher.hpp"

/*Class definition: ---------------------------------------------------------*/
Hasher::Hasher() : mHash(QString()){}
Hasher::~Hasher(){}

QString Hasher::Hasher_CalculateHash(QString dataString)
{
    QByteArray hash = QCryptographicHash::hash(dataString.toUtf8(), QCryptographicHash::Md5);
    mHash = QString(hash.toHex());
    return mHash;
}
