/**
 * @file        xmlparser.cpp
 * @author      Tomas Bartosik
 * @date        13.03.2021
 * @brief       implementation file for XML processing class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/xmlparser.hpp"

/*Class definition: ---------------------------------------------------------*/
XmlParser::XmlParser() : mDirPath(QString()){}
XmlParser::~XmlParser(){}

void XmlParser::XmlParser_InitializePath()
{
    mDirPath = QString("xmlFiles/");
    QDir directory;

    if(!directory.exists(mDirPath)) directory.mkpath(mDirPath);
}

void XmlParser::XmlParser_InitializeWorkPointData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "settings.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Education Manager settings XML file");
            writer->writeStartElement("Settings");
            writer->writeStartElement("WorkPoints");
            writer->writeTextElement("LectureCZ", "1.8");
            writer->writeTextElement("LectureEN", "2.4");
            writer->writeTextElement("PracticeCZ", "1.2");
            writer->writeTextElement("PracticeEN", "1.8");
            writer->writeTextElement("SeminarCZ", "1.2");
            writer->writeTextElement("SeminarEN", "1.8");
            writer->writeTextElement("AtelierCZ", "1.2");
            writer->writeTextElement("AtelierEN", "1.8");
            writer->writeTextElement("CreditTestCZ", "0.2");
            writer->writeTextElement("CreditTestEN", "0.2");
            writer->writeTextElement("ClassifiedCreditTestCZ", "0.3");
            writer->writeTextElement("ClassifiedCreditTestEN", "0.3");
            writer->writeTextElement("ExamCZ", "0.4");
            writer->writeTextElement("ExamEN", "0.4");
            writer->writeEndElement();
            writer->writeEndElement();
            writer->writeEndDocument();
            delete writer;
        }
    }
}

QStringList XmlParser::XmlParser_LoadWorkPointData()
{
    QStringList workPointsLoaded;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load application settings"));
    XmlParser_InitializeWorkPointData();
    QFile file(mDirPath + "settings.xml");
    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);
        if(reader->readNextStartElement())
        {
            if(reader->name() == "Settings")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "WorkPoints")
                    {
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "LectureCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "LectureEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "PracticeCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "PracticeEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "SeminarCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "SeminarEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "AtelierCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "AtelierEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "CreditTestCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "CreditTestEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "ClassifiedCreditTestCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "ClassifiedCreditTestEN") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "ExamCZ") workPointsLoaded << reader->readElementText();
                            else if(reader->name() == "ExamEN") workPointsLoaded << reader->readElementText();
                            else reader->skipCurrentElement();
                        }
                    }
                    else reader->skipCurrentElement();
                }
            }
        }
        delete reader;
    }
    else
    {
        errMsg.exec();
        return QStringList();
    }
    return workPointsLoaded;
}

void XmlParser::XmlParser_InitializeCourseData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "course.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Course list XML file");
            writer->writeStartElement("CourseList");
            writer->writeStartElement("Course");
            writer->writeAttribute("ID", "initialCourse");
            writer->writeTextElement("CourseShortcut", "AP8VT");
            writer->writeTextElement("CourseName", "Vybrané techniky vývoje");
            writer->writeTextElement("LectureCapacity", "60");
            writer->writeTextElement("PracticeCapacity", "20");
            writer->writeTextElement("SeminarCapacity", "0");
            writer->writeTextElement("AtelierCapacity", "0");
            writer->writeTextElement("LectureHours", "28");
            writer->writeTextElement("PracticeHours", "42");
            writer->writeTextElement("SeminarHours", "0");
            writer->writeTextElement("AtelierHours", "0");
            writer->writeTextElement("WeekCount", "14");
            writer->writeTextElement("Credits", "4");
            writer->writeTextElement("Semester", "1");
            writer->writeTextElement("Ending", "2");
            writer->writeTextElement("Language", "1");
            writer->writeEndElement();
            writer->writeEndElement();
            writer->writeEndDocument();
            delete writer;
        }
    }
}

void XmlParser::XmlParser_InitializeEmployeeData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "employee.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Employee list XML file");
            writer->writeStartElement("EmployeeList");
            writer->writeStartElement("Employee");
            writer->writeAttribute("ID", "initialEmployee");
            writer->writeTextElement("TitledName", "Ing. Petr Novák, Ph. D.");
            writer->writeTextElement("Function", "odborný asistent");
            writer->writeTextElement("Email", "pnovak@utb.cz");
            writer->writeTextElement("PrivatePhone", "628457144");
            writer->writeTextElement("IsDoctoral", "0");
            writer->writeTextElement("Workroom", "54/226");
            writer->writeTextElement("WorkroomPhone", "+420512488581");
            writer->writeEndElement();
            writer->writeEndElement();
            writer->writeEndDocument();
            delete writer;
        }
    }
}

void XmlParser::XmlParser_InitializeWorkLabelData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "worklabel.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Work label list XML file");
            writer->writeStartElement("WorkLabelList");
            writer->writeStartElement("WorkLabel");
            writer->writeAttribute("ID", "initialWorkLabel");
            writer->writeTextElement("LabelName", "AP8VT [L]");
            writer->writeTextElement("WeekCount", "14");
            writer->writeTextElement("ScheduledHours", "28");
            writer->writeTextElement("StudentCount", "22");
            writer->writeTextElement("WorkPoints", "50.4");
            writer->writeTextElement("LabelType", "1");
            writer->writeTextElement("CourseType", "1");
            writer->writeEndElement();
            writer->writeEndElement();
            writer->writeEndDocument();
            delete writer;
        }
    }
}

void XmlParser::XmlParser_InitializeGroupData()
{
    XmlParser_InitializePath();
    QFile file(mDirPath + "group.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        if(file.open(QIODevice::WriteOnly))
        {
            QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
            writer->setAutoFormatting(true);
            writer->writeStartDocument();
            writer->writeComment("Group list XML file");
            writer->writeStartElement("GroupList");
            writer->writeStartElement("Group");
            writer->writeAttribute("ID", "initialGroup");
            writer->writeTextElement("Specialization", "Softwarové inženýrství");
            writer->writeTextElement("Year", "1");
            writer->writeTextElement("SummerSemesterStudents", "28");
            writer->writeTextElement("WinterSemesterStudents", "36");
            writer->writeTextElement("StudyType", "2");
            writer->writeTextElement("StudyForm", "1");
            writer->writeTextElement("Language", "1");
            writer->writeEndElement();
            writer->writeEndElement();
            writer->writeEndDocument();
            delete writer;
        }
    }
}

QList<QSharedPointer<Course>> XmlParser::XmlParser_LoadCourseData()
{
    XmlParser_InitializePath();
    QList<QSharedPointer<Course>> courseList;
    QList<QSharedPointer<Group>> groupList = XmlParser_LoadGroupData();
    QList<QSharedPointer<Group>> courseGroupList;
    QSharedPointer<Course> course;
    QStringList courseLoaded;
    QString groupID;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load course data"));
    XmlParser_InitializeCourseData();
    QFile file(mDirPath + "course.xml");
    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);
        if(reader->readNextStartElement())
        {
            if(reader->name() == "CourseList")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "Course")
                    {
                        courseLoaded << reader->attributes().value("ID").toString();
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "CourseShortcut") courseLoaded << reader->readElementText();
                            else if(reader->name() == "CourseName") courseLoaded << reader->readElementText();
                            else if(reader->name() == "LectureCapacity") courseLoaded << reader->readElementText();
                            else if(reader->name() == "PracticeCapacity") courseLoaded << reader->readElementText();
                            else if(reader->name() == "SeminarCapacity") courseLoaded << reader->readElementText();
                            else if(reader->name() == "AtelierCapacity") courseLoaded << reader->readElementText();
                            else if(reader->name() == "LectureHours") courseLoaded << reader->readElementText();
                            else if(reader->name() == "PracticeHours") courseLoaded << reader->readElementText();
                            else if(reader->name() == "SeminarHours") courseLoaded << reader->readElementText();
                            else if(reader->name() == "AtelierHours") courseLoaded << reader->readElementText();
                            else if(reader->name() == "WeekCount") courseLoaded << reader->readElementText();
                            else if(reader->name() == "Credits") courseLoaded << reader->readElementText();
                            else if(reader->name() == "Semester") courseLoaded << reader->readElementText();
                            else if(reader->name() == "Ending") courseLoaded << reader->readElementText();
                            else if(reader->name() == "Language")
                            {
                                courseLoaded << reader->readElementText();
                                Lesson ls(courseLoaded.at(3).toInt(), courseLoaded.at(4).toInt(), courseLoaded.at(5).toInt(), courseLoaded.at(6).toInt());
                                SemesterType semesterType;
                                switch(courseLoaded.at(13).toInt())
                                {
                                case 1: semesterType = SemesterType::eLS; break;
                                case 2: default: semesterType = SemesterType::eZS; break;
                                }
                                Ending ending;
                                switch(courseLoaded.at(14).toInt())
                                {
                                case 1: ending = Ending::eZ; break;
                                case 2: ending = Ending::eKZ; break;
                                case 3: ending = Ending::eZk; break;
                                case 4: default: ending = Ending::eZZk; break;
                                }
                                Semester sm(courseLoaded.at(7).toInt(), courseLoaded.at(8).toInt(), courseLoaded.at(9).toInt(),
                                     courseLoaded.at(10).toInt(), courseLoaded.at(11).toInt(), courseLoaded.at(12).toInt(), semesterType, ending);
                                Language language;
                                switch(courseLoaded.at(15).toInt())
                                {
                                case 1: language = Language::eCZ; break;
                                case 2: default: language = Language::eEN; break;
                                }
                                course = QSharedPointer<Course>(new Course(courseLoaded.at(0), courseLoaded.at(1), courseLoaded.at(2),
                                     ls, sm, language, courseGroupList));
                                courseLoaded.clear();
                            }
                            else if(reader->name() == "Groups")
                            {
                                while(reader->readNextStartElement())
                                {
                                    if(reader->name() == "Group")
                                    {
                                        groupID = reader->readElementText();
                                        for(int item = 0; item < groupList.count(); item++)
                                        {
                                            if(QString::compare(groupList.at(item)->Group_GetGroupID(), groupID, Qt::CaseSensitive) == 0)
                                            {
                                                course->Course_AppendGroup(groupList.at(item));
                                            }
                                        }
                                    }
                                    else reader->skipCurrentElement();
                                }
                            }
                            else reader->skipCurrentElement();
                        }
                        courseList.append(course);
                    }
                    else reader->skipCurrentElement();
                }
            }
        }
        delete reader;
    }
    else
    {
        errMsg.exec();
        return QList<QSharedPointer<Course>> ();
    }
    return courseList;
}

QList<QSharedPointer<Employee>> XmlParser::XmlParser_LoadEmployeeData()
{
    XmlParser_InitializePath();
    QList<QSharedPointer<Employee>> employeeList;
    QSharedPointer<Employee> employee;
    QStringList employeeLoaded;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load employee data"));
    XmlParser_InitializeEmployeeData();
    QFile file(mDirPath + "employee.xml");
    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);
        if(reader->readNextStartElement())
        {
            if(reader->name() == "EmployeeList")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "Employee")
                    {
                        employeeLoaded << reader->attributes().value("ID").toString();
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "TitledName") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "Function") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "Email") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "PrivatePhone") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "IsDoctoral") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "Workroom") employeeLoaded << reader->readElementText();
                            else if(reader->name() == "WorkroomPhone")
                            {
                                employeeLoaded << reader->readElementText();
                                Workroom wr(employeeLoaded.at(6), employeeLoaded.at(7));
                                employee = QSharedPointer<Employee>(new Employee(employeeLoaded.at(0), employeeLoaded.at(1), employeeLoaded.at(2),
                                      employeeLoaded.at(3), employeeLoaded.at(4), employeeLoaded.at(5).toInt(), wr));
                                employeeList.append(employee);
                                employeeLoaded.clear();
                            }
                            else reader->skipCurrentElement();
                        }
                    }
                    else reader->skipCurrentElement();
                }
            }
        }
        delete reader;
    }
    else
    {
        errMsg.exec();
        return QList<QSharedPointer<Employee>> ();
    }
    return employeeList;
}

QList<QSharedPointer<WorkLabel>> XmlParser::XmlParser_LoadWorkLabelData()
{
    XmlParser_InitializePath();
    QList<QSharedPointer<WorkLabel>> workLabelList;
    QList<QSharedPointer<Course>> courseList = XmlParser_LoadCourseData();
    QList<QSharedPointer<Employee>> employeeList = XmlParser_LoadEmployeeData();
    QSharedPointer<WorkLabel> workLabel;
    QStringList workLabelLoaded;
    QString courseID;
    QString employeeID;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load work label data"));
    XmlParser_InitializeWorkLabelData();
    QFile file(mDirPath + "worklabel.xml");
    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);
        if(reader->readNextStartElement())
        {
            if(reader->name() == "WorkLabelList")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "WorkLabel")
                    {
                        workLabelLoaded << reader->attributes().value("ID").toString();
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "LabelName") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "WeekCount") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "ScheduledHours") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "StudentCount") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "WorkPoints") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "LabelType") workLabelLoaded << reader->readElementText();
                            else if(reader->name() == "CourseType")
                            {
                                workLabelLoaded << reader->readElementText();
                                LabelType labelType;
                                switch(workLabelLoaded.at(6).toInt())
                                {
                                case 1: labelType = LabelType::eLecture; break;
                                case 2: labelType = LabelType::ePractice; break;
                                case 3: labelType = LabelType::eSeminar; break;
                                case 4: labelType = LabelType::eAtelier; break;
                                case 5: labelType = LabelType::eZ; break;
                                case 6: labelType = LabelType::eKZ; break;
                                case 7: default: labelType = LabelType::eZk; break;
                                }
                                CourseType courseType;
                                switch(workLabelLoaded.at(7).toInt())
                                {
                                case 1: courseType = CourseType::eStandard; break;
                                case 2: courseType = CourseType::eForeignLanguage; break;
                                case 3: default: courseType = CourseType::eDoctoral; break;
                                }
                                workLabel = QSharedPointer<WorkLabel>(new WorkLabel(workLabelLoaded.at(0), workLabelLoaded.at(1),
                                      workLabelLoaded.at(2).toInt(), workLabelLoaded.at(3).toInt(), workLabelLoaded.at(4).toInt(),
                                      workLabelLoaded.at(5).toFloat(), labelType, courseType, nullptr, nullptr));
                                workLabelList.append(workLabel);
                                workLabelLoaded.clear();
                            }
                            else if(reader->name() == "Course")
                            {
                                courseID = reader->readElementText();
                                for(int item = 0; item < courseList.count(); item++)
                                {
                                    if(QString::compare(courseList.at(item)->Course_GetCourseID(), courseID, Qt::CaseSensitive) == 0)
                                    {
                                        workLabelList.last()->WorkLabel_SetCourse(courseList.at(item));
                                    }
                                }
                            }
                            else if(reader->name() == "Employee")
                            {
                                employeeID = reader->readElementText();
                                for(int item = 0; item < employeeList.count(); item++)
                                {
                                    if(QString::compare(employeeList.at(item)->Employee_GetEmployeeID(), employeeID, Qt::CaseSensitive) == 0)
                                    {
                                        workLabelList.last()->WorkLabel_SetEmployee(employeeList.at(item));
                                    }
                                }
                            }
                            else reader->skipCurrentElement();
                        }
                    }
                    else reader->skipCurrentElement();
                }
            }
        }
        delete reader;
    }
    else
    {
        errMsg.exec();
        return QList<QSharedPointer<WorkLabel>> ();
    }
    return workLabelList;
}

QList<QSharedPointer<Group>> XmlParser::XmlParser_LoadGroupData()
{
    XmlParser_InitializePath();
    QList<QSharedPointer<Group>> groupList;
    QSharedPointer<Group> group;
    QStringList groupLoaded;
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML loading error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to load group data"));
    XmlParser_InitializeGroupData();
    QFile file(mDirPath + "group.xml");
    if(file.open(QIODevice::ReadOnly))
    {
        QXmlStreamReader *reader = new QXmlStreamReader(&file);
        if(reader->readNextStartElement())
        {
            if(reader->name() == "GroupList")
            {
                while(reader->readNextStartElement())
                {
                    if(reader->name() == "Group")
                    {
                        groupLoaded << reader->attributes().value("ID").toString();
                        while(reader->readNextStartElement())
                        {
                            if(reader->name() == "Specialization") groupLoaded << reader->readElementText();
                            else if(reader->name() == "Year") groupLoaded << reader->readElementText();
                            else if(reader->name() == "SummerSemesterStudents") groupLoaded << reader->readElementText();
                            else if(reader->name() == "WinterSemesterStudents") groupLoaded << reader->readElementText();
                            else if(reader->name() == "StudyType") groupLoaded << reader->readElementText();
                            else if(reader->name() == "StudyForm") groupLoaded << reader->readElementText();
                            else if(reader->name() == "Language")
                            {
                                groupLoaded << reader->readElementText();
                                Year year(groupLoaded.at(2).toInt(), groupLoaded.at(3).toInt(), groupLoaded.at(4).toInt());
                                StudyType studyType;
                                switch(groupLoaded.at(5).toInt())
                                {
                                case 1: studyType = StudyType::eBc; break;
                                case 2: studyType = StudyType::eMgr; break;
                                case 3: default: studyType = StudyType::ePhD; break;
                                }
                                StudyForm studyForm;
                                switch(groupLoaded.at(6).toInt())
                                {
                                case 1: studyForm = StudyForm::eP; break;
                                case 2: default: studyForm = StudyForm::eK; break;
                                }
                                Language language;
                                switch(groupLoaded.at(7).toInt())
                                {
                                case 1: language = Language::eCZ; break;
                                case 2: default: language = Language::eEN; break;
                                }
                                group = QSharedPointer<Group>(new Group(groupLoaded.at(0), groupLoaded.at(1), year, studyType, studyForm, language));
                                groupList.append(group);
                                groupLoaded.clear();
                            }
                            else reader->skipCurrentElement();
                        }
                    }
                    else reader->skipCurrentElement();
                }
            }
        }
        delete reader;
    }
    else
    {
        errMsg.exec();
        return QList<QSharedPointer<Group>> ();
    }
    return groupList;
}

void XmlParser::XmlParser_SaveCourseData(QList<QSharedPointer<Course>> course)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save group changes"));
    QFile file(mDirPath + "course.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        int groupsCount = 0;
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Course list XML file");
        writer->writeStartElement("CourseList");
        for(int index = 0; index < course.count(); index++)
        {
            QStringList items = course.at(index)->Course_GetAll();
            writer->writeStartElement("Course");
            writer->writeAttribute("ID", items.at(0));
            writer->writeTextElement("CourseShortcut", items.at(1));
            writer->writeTextElement("CourseName", items.at(2));
            writer->writeTextElement("LectureCapacity", items.at(3));
            writer->writeTextElement("PracticeCapacity", items.at(4));
            writer->writeTextElement("SeminarCapacity", items.at(5));
            writer->writeTextElement("AtelierCapacity", items.at(6));
            writer->writeTextElement("LectureHours", items.at(7));
            writer->writeTextElement("PracticeHours", items.at(8));
            writer->writeTextElement("SeminarHours", items.at(9));
            writer->writeTextElement("AtelierHours", items.at(10));
            writer->writeTextElement("WeekCount", items.at(11));
            writer->writeTextElement("Credits", items.at(12));
            writer->writeTextElement("Semester", course.at(index)->Course_GetSemesterType(true));
            writer->writeTextElement("Ending", course.at(index)->Course_GetEnding(true));
            writer->writeTextElement("Language", course.at(index)->Course_GetLanguage(true));

            groupsCount = course.at(index)->Course_GetGroups().count();
            if(groupsCount > 0)
            {
                writer->writeStartElement("Groups");
                for(int item = 0; item < groupsCount; item++)
                {
                    writer->writeTextElement("Group", course.at(index)->Course_GetGroups().at(item)->Group_GetGroupID());
                }
                writer->writeEndElement();
            }
            writer->writeEndElement();
        }
        writer->writeEndElement();
        writer->writeEndDocument();
        delete writer;
    }
    else errMsg.exec();
}

void XmlParser::XmlParser_SaveEmployeeData(QList<QSharedPointer<Employee>> employee)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save employee changes"));
    QFile file(mDirPath + "employee.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Employee list XML file");
        writer->writeStartElement("EmployeeList");
        for(int index = 0; index < employee.count(); index++)
        {
            QStringList items = employee.at(index)->Employee_GetAll();
            writer->writeStartElement("Employee");
            writer->writeAttribute("ID", items.at(0));
            writer->writeTextElement("TitledName", items.at(1));
            writer->writeTextElement("Function", items.at(2));
            writer->writeTextElement("Email", items.at(3));
            writer->writeTextElement("PrivatePhone", items.at(4));
            writer->writeTextElement("IsDoctoral", employee.at(index)->Employee_GetIsDoctoral(true));
            writer->writeTextElement("Workroom", items.at(6));
            writer->writeTextElement("WorkroomPhone", items.at(7));
            writer->writeEndElement();
        }
        writer->writeEndElement();
        writer->writeEndDocument();
        delete writer;
    }
    else errMsg.exec();
}

QString XmlParser::XmlParser_SaveEmployeeStatus(QList<QSharedPointer<WorkLabel>> workLabel, QString email)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save work labels for employee"));
    QString filePath = QString(mDirPath + "status.xml");
    QFile file(filePath);
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Employee status XML file");
        writer->writeStartElement("WorkLabelList");
        for(int index = 0; index < workLabel.count(); index++)
        {
            if(workLabel.at(index)->WorkLabel_GetEmployee() != nullptr)
            {
                if(QString::compare(workLabel.at(index)->WorkLabel_GetEmployee()->Employee_GetEmail(), email, Qt::CaseSensitive) == 0)
                {
                    QStringList items = workLabel.at(index)->WorkLabel_GetAll();
                    writer->writeStartElement("WorkLabel");
                    writer->writeTextElement("LabelName", items.at(1));
                    writer->writeTextElement("WeekCount", items.at(2));
                    writer->writeTextElement("ScheduledHours", items.at(3));
                    writer->writeTextElement("StudentCount", items.at(4));
                    writer->writeTextElement("WorkPoints", items.at(5));
                    writer->writeEndElement();
                }
            }
        }
        writer->writeEndElement();
        writer->writeEndDocument();
        delete writer;

        return filePath;
    }
    else
    {
        errMsg.exec();
        return QString("");
    }
    return QString("");
}

void XmlParser::XmlParser_SaveWorkLabelData(QList<QSharedPointer<WorkLabel>> workLabel)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save work label changes"));
    QFile file(mDirPath + "worklabel.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Work label list XML file");
        writer->writeStartElement("WorkLabelList");
        for(int index = 0; index < workLabel.count(); index++)
        {
            QStringList items = workLabel.at(index)->WorkLabel_GetAll();
            writer->writeStartElement("WorkLabel");
            writer->writeAttribute("ID", items.at(0));
            writer->writeTextElement("LabelName", items.at(1));
            writer->writeTextElement("WeekCount", items.at(2));
            writer->writeTextElement("ScheduledHours", items.at(3));
            writer->writeTextElement("StudentCount", items.at(4));
            writer->writeTextElement("WorkPoints", items.at(5));
            writer->writeTextElement("LabelType", workLabel.at(index)->WorkLabel_GetLabelType(true));
            writer->writeTextElement("CourseType", workLabel.at(index)->WorkLabel_GetCourseType(true));
            if(workLabel.at(index)->WorkLabel_GetCourse() != nullptr)
            {
                writer->writeTextElement("Course", workLabel.at(index)->WorkLabel_GetCourse()->Course_GetCourseID());
            }
            if(workLabel.at(index)->WorkLabel_GetEmployee() != nullptr)
            {
                writer->writeTextElement("Employee", workLabel.at(index)->WorkLabel_GetEmployee()->Employee_GetEmployeeID());
            }
            writer->writeEndElement();
        }
        writer->writeEndElement();
        writer->writeEndDocument();
        delete writer;
    }
    else errMsg.exec();
}

void XmlParser::XmlParser_SaveGroupData(QList<QSharedPointer<Group>> group)
{
    XmlParser_InitializePath();
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("XML saving error"));
    errMsg.setText(QObject::tr("XML file could not be opened in order to save group changes"));
    QFile file(mDirPath + "group.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QXmlStreamWriter *writer = new QXmlStreamWriter(&file);
        writer->setAutoFormatting(true);
        writer->writeStartDocument();
        writer->writeComment("Group list XML file");
        writer->writeStartElement("GroupList");
        for(int index = 0; index < group.count(); index++)
        {
            QStringList items = group.at(index)->Group_GetAll();
            writer->writeStartElement("Group");
            writer->writeAttribute("ID", items.at(0));
            writer->writeTextElement("Specialization", items.at(1));
            writer->writeTextElement("Year", items.at(2));
            writer->writeTextElement("SummerSemesterStudents", items.at(3));
            writer->writeTextElement("WinterSemesterStudents", items.at(4));
            writer->writeTextElement("StudyType", group.at(index)->Group_GetStudyType(true));
            writer->writeTextElement("StudyForm", group.at(index)->Group_GetStudyForm(true));
            writer->writeTextElement("Language", group.at(index)->Group_GetLanguage(true));
            writer->writeEndElement();
        }
        writer->writeEndElement();
        writer->writeEndDocument();
        delete writer;
    }
    else errMsg.exec();
}
