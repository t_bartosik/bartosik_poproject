/**
 * @file        dbparser.cpp
 * @author      Tomas Bartosik
 * @date        29.05.2021
 * @brief       implementation file for DB class
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/dbparser.hpp"

/*Class definition: ---------------------------------------------------------*/
DbParser::DbParser() : mDirPath(QString())
{
    QMessageBox errMsg;
    errMsg.setWindowTitle(QObject::tr("Connection error"));

    mDatabase = QSqlDatabase::addDatabase("QSQLITE");

    DbParser_InitializePath();
    mDatabase.setDatabaseName(mDirPath + "database.db");

    if(!mDatabase.open())
    {
        errMsg.setText(QObject::tr("Database file could not be opened."));
        errMsg.exec();
    }
    else
    {

        if(mDatabase.tables().count() < TABLES_COUNT)
        {
            DbParser_InitializeDb();
        }
    }
}
DbParser::~DbParser()
{
    mDatabase.close();
}

void DbParser::DbParser_InitializePath()
{
    mDirPath = QString("dbFiles/");
    QDir directory;

    if(!directory.exists(mDirPath)) directory.mkpath(mDirPath);
}

void DbParser::DbParser_InitializeDb()
{
    QSqlQuery query;
    query.exec("CREATE TABLE Courses (id VARCHAR(40) PRIMARY KEY, courseShortcut VARCHAR(20), courseName VARCHAR(60), "
               "lectureCapacity INT, practiceCapacity INT, seminarCapacity INT, atelierCapacity INT, lectureHours INT, practiceHours INT, "
               "seminarHours INT, atelierHours INT, weekCount INT, credits INT, semester INT, ending INT, language INT)");

    query.exec("CREATE TABLE CourseGroups (id INTEGER PRIMARY KEY AUTOINCREMENT, courseId VARCHAR(40), groupId VARCHAR(40))");

    query.exec("CREATE TABLE Employees (id VARCHAR(40) PRIMARY KEY, titledName VARCHAR(60), function VARCHAR(30), email VARCHAR(50), "
               "privatePhone VARCHAR(15), isDoctoral INT, workroom VARCHAR(30), workroomPhone VARCHAR(15))");

    query.exec("CREATE TABLE WorkLabels (id VARCHAR(40) PRIMARY KEY, labelName VARCHAR(60), weekCount INT, scheduledHours INT, "
               "studentCount INT, workPoints REAL, labelType INT, courseType INT, courseId VARCHAR(40), employeeId VARCHAR(40))");

    query.exec("CREATE TABLE Groups (id VARCHAR(40) PRIMARY KEY, specialization VARCHAR(60), year INT, summerSemesterStudents INT, "
               "winterSemesterStudents INT, studyType INT, studyForm INT, language INT)");

    query.exec("INSERT INTO Courses VALUES ('initialCourse', 'AP8VT', 'Vybrané techniky vývoje', 60, 20, 0, 0, 28, 42, 0, 0, 14, 4, 1, 2, 1)");
    query.exec("INSERT INTO Employees VALUES ('initialEmployee', 'Ing. Petr Novák, Ph. D.', 'odborný asistent', "
               "'pnovak@utb.cz', '628457144', 0, '54/226', '+420512488581')");
    query.exec("INSERT INTO Groups VALUES ('initialGroup', 'Softwarové inženýrství', 1, 28, 36, 2, 1, 1)");
    query.exec("INSERT INTO WorkLabels VALUES ('initialWorkLabel', 'AP8VT [L]', 14, 28, 22, 50.4, 1, 1, 'null', 'null')");
}

void DbParser::DbParser_InsertCourse(QSharedPointer<Course> course)
{
    QStringList courseEntry = course->Course_GetAll();
    QSqlQuery query;
    query.prepare("INSERT INTO Courses VALUES (:id, :courseShortcut, :courseName, :lectureCapacity, :practiceCapacity, :seminarCapacity, "
                  ":atelierCapacity, :lectureHours, :practiceHours, :seminarHours, :atelierHours, :weekCount, :credits, :semester, :ending, :language)");
    query.bindValue(":id", courseEntry.at(0));
    query.bindValue(":courseShortcut", courseEntry.at(1));
    query.bindValue(":courseName", courseEntry.at(2));
    query.bindValue(":lectureCapacity", courseEntry.at(3).toInt());
    query.bindValue(":practiceCapacity", courseEntry.at(4).toInt());
    query.bindValue(":seminarCapacity", courseEntry.at(5).toInt());
    query.bindValue(":atelierCapacity", courseEntry.at(6).toInt());
    query.bindValue(":lectureHours", courseEntry.at(7).toInt());
    query.bindValue(":practiceHours", courseEntry.at(8).toInt());
    query.bindValue(":seminarHours", courseEntry.at(9).toInt());
    query.bindValue(":atelierHours", courseEntry.at(10).toInt());
    query.bindValue(":weekCount", courseEntry.at(11).toInt());
    query.bindValue(":credits", courseEntry.at(12).toInt());
    query.bindValue(":semester", course->Course_GetSemesterType(true));
    query.bindValue(":ending", course->Course_GetEnding(true));
    query.bindValue(":language", course->Course_GetLanguage(true));
    query.exec();
}

void DbParser::DbParser_InsertCourseGroup(QString courseID, QString groupID)
{
    QSqlQuery query;
    query.prepare("INSERT INTO CourseGroups (courseId, groupId) VALUES (:courseId, :groupId)");
    query.bindValue(":courseId", courseID);
    query.bindValue(":groupId", groupID);
    query.exec();
}

void DbParser::DbParser_InsertEmployee(QSharedPointer<Employee> employee)
{
    QStringList employeeEntry = employee->Employee_GetAll();
    QSqlQuery query;
    query.prepare("INSERT INTO Employees VALUES (:id, :titledName, :function, :email, :privatePhone, "
                  ":isDoctoral, :workroom, :workroomPhone)");
    query.bindValue(":id", employeeEntry.at(0));
    query.bindValue(":titledName", employeeEntry.at(1));
    query.bindValue(":function", employeeEntry.at(2));
    query.bindValue(":email", employeeEntry.at(3));
    query.bindValue(":privatePhone", employeeEntry.at(4));
    query.bindValue(":isDoctoral", employee->Employee_GetIsDoctoral(true));
    query.bindValue(":workroom", employeeEntry.at(6));
    query.bindValue(":workroomPhone", employeeEntry.at(7));
    query.exec();
}

void DbParser::DbParser_InsertWorkLabel(QSharedPointer<WorkLabel> workLabel)
{
    QStringList workLabelEntry = workLabel->WorkLabel_GetAll();
    QSqlQuery query;
    query.prepare("INSERT INTO WorkLabels VALUES (:id, :labelName, :weekCount, :scheduledHours, "
                  ":studentCount, :workPoints, :labelType, :courseType, :courseId, :employeeId)");
    query.bindValue(":id", workLabelEntry.at(0));
    query.bindValue(":labelName", workLabelEntry.at(1));
    query.bindValue(":weekCount", workLabelEntry.at(2).toInt());
    query.bindValue(":scheduledHours", workLabelEntry.at(3).toInt());
    query.bindValue(":studentCount", workLabelEntry.at(4).toInt());
    query.bindValue(":workPoints", workLabelEntry.at(5).toFloat());
    query.bindValue(":labelType", workLabel->WorkLabel_GetLabelType(true));
    query.bindValue(":courseType", workLabel->WorkLabel_GetCourseType(true));

    if(workLabel->WorkLabel_GetCourse() != nullptr) query.bindValue(":courseId", workLabel->WorkLabel_GetCourse()->Course_GetCourseID());
    else query.bindValue(":courseId", QString("null"));

    if(workLabel->WorkLabel_GetEmployee() != nullptr) query.bindValue(":employeeId", workLabel->WorkLabel_GetEmployee()->Employee_GetEmployeeID());
    else query.bindValue(":employeeId", QString("null"));

    query.exec();
}

void DbParser::DbParser_InsertGroup(QSharedPointer<Group> group)
{
    QStringList groupEntry = group->Group_GetAll();
    QSqlQuery query;
    query.prepare("INSERT INTO Groups VALUES (:id, :specialization, :year, :summerSemesterStudents, "
                  ":winterSemesterStudents, :studyType, :studyForm, :language)");
    query.bindValue(":id", groupEntry.at(0));
    query.bindValue(":specialization", groupEntry.at(1));
    query.bindValue(":year", groupEntry.at(2).toInt());
    query.bindValue(":summerSemesterStudents", groupEntry.at(3).toInt());
    query.bindValue(":winterSemesterStudents", groupEntry.at(4).toInt());
    query.bindValue(":studyType", group->Group_GetStudyType(true));
    query.bindValue(":studyForm", group->Group_GetStudyForm(true));
    query.bindValue(":language", group->Group_GetLanguage(true));
    query.exec();
}

void DbParser::DbParser_UpdateCourseGroup(QString originalHashedID, QString newHashedID)
{
    QSqlQuery query;
    query.prepare("UPDATE CourseGroups SET groupId = :newID WHERE groupId LIKE '%" + originalHashedID + "%'");
    query.bindValue(":newID", newHashedID);
    query.exec();
}

void DbParser::DbParser_UpdateWorkLabel(QString workLabelID, QString courseID, QString employeeID)
{
    QSqlQuery query;
    query.prepare("UPDATE WorkLabels SET courseId = :courseID, employeeId = :employeeID WHERE id LIKE '%" + workLabelID + "%'");
    query.bindValue(":courseID", courseID);
    query.bindValue(":employeeID", employeeID);
    query.exec();
}

void DbParser::DbParser_UpdateGroup(QString originalHashedID, QSharedPointer<Group> group)
{
    QSqlQuery query;
    query.prepare("UPDATE Groups SET id = :hash, summerSemesterStudents = :sStudents, "
                  "winterSemesterStudents = :wStudents WHERE id LIKE '%" + originalHashedID + "%'");
    query.bindValue(":hash", group->Group_GetGroupID());
    query.bindValue(":sStudents", group->Group_GetSummerStudentCount());
    query.bindValue(":wStudents", group->Group_GetWinterStudentCount());
    query.exec();
}

void DbParser::DbParser_DeleteCourses(QStringList removalIdList)
{
    QSqlQuery query;
    for(int index = 0; index < removalIdList.count(); index++)
    {
        query.exec("DELETE FROM Courses WHERE id LIKE '%" + removalIdList.at(index) + "%'");

        DbParser_DeleteCourseGroupsByCourse(removalIdList.at(index));
    }
}

void DbParser::DbParser_DeleteCourseGroupsByCourse(QString removalID)
{
    QSqlQuery query;
    query.exec("DELETE FROM CourseGroups WHERE courseId LIKE '%" + removalID + "%'");
}

void DbParser::DbParser_DeleteCourseGroupsByGroup(QString removalID)
{
    QSqlQuery query;
    query.exec("DELETE FROM CourseGroups WHERE groupId LIKE '%" + removalID + "%'");
}

void DbParser::DbParser_DeleteEmployees(QStringList removalIdList)
{
    QSqlQuery query;
    for(int index = 0; index < removalIdList.count(); index++)
    {
        query.exec("DELETE FROM Employees WHERE id LIKE '%" + removalIdList.at(index) + "%'");
    }
}

void DbParser::DbParser_DeleteWorkLabelsByID(QStringList removalIdList)
{
    QSqlQuery query;
    for(int index = 0; index < removalIdList.count(); index++)
    {
        query.exec("DELETE FROM WorkLabels WHERE id LIKE '%" + removalIdList.at(index) + "%'");
    }
}

void DbParser::DbParser_DeleteWorkLabelsByShortcut(QString workLabelShortcut)
{
    QSqlQuery query;
    query.exec("DELETE FROM WorkLabels WHERE labelName LIKE '%" + workLabelShortcut + "%'");
}

void DbParser::DbParser_DeleteGroups(QStringList removalIdList)
{
    QSqlQuery query;
    for(int index = 0; index < removalIdList.count(); index++)
    {
        query.exec("DELETE FROM Groups WHERE id LIKE '%" + removalIdList.at(index) + "%'");

        DbParser_DeleteCourseGroupsByGroup(removalIdList.at(index));
    }
}

QList<QSharedPointer<Course>> DbParser::DbParser_LoadCourseData()
{
    QList<QSharedPointer<Course>> courseList;
    QList<QSharedPointer<Group>> courseGroupList;
    QList<QSharedPointer<Group>> groupList = DbParser_LoadGroupData();
    QSharedPointer<Course> course;
    QStringList courseLoaded;
    QSqlQuery query;
    query.exec("SELECT * FROM Courses");
    while(query.next())
    {
        for(int index = 0; index < (COURSE_ITEMS + 1); index++) courseLoaded << query.value(index).toString();

        Lesson ls(courseLoaded.at(3).toInt(), courseLoaded.at(4).toInt(), courseLoaded.at(5).toInt(), courseLoaded.at(6).toInt());
        SemesterType semesterType;
        switch(courseLoaded.at(13).toInt())
        {
        case 1: semesterType = SemesterType::eLS; break;
        case 2: default: semesterType = SemesterType::eZS; break;
        }
        Ending ending;
        switch(courseLoaded.at(14).toInt())
        {
        case 1: ending = Ending::eZ; break;
        case 2: ending = Ending::eKZ; break;
        case 3: ending = Ending::eZk; break;
        case 4: default: ending = Ending::eZZk; break;
        }
        Semester sm(courseLoaded.at(7).toInt(), courseLoaded.at(8).toInt(), courseLoaded.at(9).toInt(),
             courseLoaded.at(10).toInt(), courseLoaded.at(11).toInt(), courseLoaded.at(12).toInt(), semesterType, ending);
        Language language;
        switch(courseLoaded.at(15).toInt())
        {
        case 1: language = Language::eCZ; break;
        case 2: default: language = Language::eEN; break;
        }
        course = QSharedPointer<Course>(new Course(courseLoaded.at(0), courseLoaded.at(1), courseLoaded.at(2),
             ls, sm, language, courseGroupList));

        QStringList loadedGroups = DbParser_LoadCourseGroupsData(courseLoaded.at(0));
        while(loadedGroups.count() > 0)
        {
            for(int index = 0; index < groupList.count(); index++)
            {
                if(loadedGroups.count() > 0)
                {
                    if(QString::compare(loadedGroups.first(), groupList.at(index)->Group_GetGroupID(), Qt::CaseSensitive) == 0)
                    {
                        course->Course_AppendGroup(groupList.at(index));
                        loadedGroups.removeFirst();
                    }
                }
            }
        }

        courseList.append(course);
        courseLoaded.clear();
    }
    return courseList;
}

QStringList DbParser::DbParser_LoadCourseGroupsData(QString courseID)
{
    QStringList groupList;
    QSqlQuery query;
    query.exec("SELECT groupId FROM CourseGroups WHERE courseId LIKE '%" + courseID + "%'");
    while(query.next())
    {
        groupList << query.value(0).toString();
    }
    return groupList;
}

QList<QSharedPointer<Employee>> DbParser::DbParser_LoadEmployeeData()
{
    QList<QSharedPointer<Employee>> employeeList;
    QSharedPointer<Employee> employee;
    QStringList employeeLoaded;
    QSqlQuery query;
    query.exec("SELECT * FROM Employees");
    while(query.next())
    {
        for(int index = 0; index < (EMPLOYEE_ITEMS + 1); index++) employeeLoaded << query.value(index).toString();

        Workroom wr(employeeLoaded.at(6), employeeLoaded.at(7));
        employee = QSharedPointer<Employee>(new Employee(employeeLoaded.at(0), employeeLoaded.at(1), employeeLoaded.at(2),
              employeeLoaded.at(3), employeeLoaded.at(4), employeeLoaded.at(5).toInt(), wr));
        employeeList.append(employee);
        employeeLoaded.clear();
    }
    return employeeList;
}

QList<QSharedPointer<WorkLabel>> DbParser::DbParser_LoadWorkLabelData()
{
    QList<QSharedPointer<WorkLabel>> workLabelList;
    QList<QSharedPointer<Course>> courseList = DbParser_LoadCourseData();
    QList<QSharedPointer<Employee>> employeeList = DbParser_LoadEmployeeData();
    QSharedPointer<WorkLabel> workLabel;
    QStringList workLabelLoaded;
    QSqlQuery query;
    query.exec("SELECT * FROM WorkLabels");
    while(query.next())
    {
        for(int index = 0; index < (WORKLABEL_ITEMS + 3); index++) workLabelLoaded << query.value(index).toString();

        LabelType labelType;
        switch(workLabelLoaded.at(6).toInt())
        {
        case 1: labelType = LabelType::eLecture; break;
        case 2: labelType = LabelType::ePractice; break;
        case 3: labelType = LabelType::eSeminar; break;
        case 4: labelType = LabelType::eAtelier; break;
        case 5: labelType = LabelType::eZ; break;
        case 6: labelType = LabelType::eKZ; break;
        case 7: default: labelType = LabelType::eZk; break;
        }
        CourseType courseType;
        switch(workLabelLoaded.at(7).toInt())
        {
        case 1: courseType = CourseType::eStandard; break;
        case 2: courseType = CourseType::eForeignLanguage; break;
        case 3: default: courseType = CourseType::eDoctoral; break;
        }
        workLabel = QSharedPointer<WorkLabel>(new WorkLabel(workLabelLoaded.at(0), workLabelLoaded.at(1),
              workLabelLoaded.at(2).toInt(), workLabelLoaded.at(3).toInt(), workLabelLoaded.at(4).toInt(),
              workLabelLoaded.at(5).toFloat(), labelType, courseType, nullptr, nullptr));

        if(QString::compare(workLabelLoaded.at(8), QString("null"), Qt::CaseSensitive) != 0)
        {
            for(int index = 0; index < courseList.count(); index++)
            {
                if(QString::compare(courseList.at(index)->Course_GetCourseID(), workLabelLoaded.at(8), Qt::CaseSensitive) == 0)
                {
                    workLabel->WorkLabel_SetCourse(courseList.at(index));
                }
            }
        }
        if(QString::compare(workLabelLoaded.at(9), QString("null"), Qt::CaseSensitive) != 0)
        {
            for(int index = 0; index < employeeList.count(); index++)
            {
                if(QString::compare(employeeList.at(index)->Employee_GetEmployeeID(), workLabelLoaded.at(9), Qt::CaseSensitive) == 0)
                {
                    workLabel->WorkLabel_SetEmployee(employeeList.at(index));
                }
            }
        }

        workLabelList.append(workLabel);
        workLabelLoaded.clear();
    }
    return workLabelList;
}

QList<QSharedPointer<Group>> DbParser::DbParser_LoadGroupData()
{
    QList<QSharedPointer<Group>> groupList;
    QSharedPointer<Group> group;
    QStringList groupLoaded;
    QSqlQuery query;
    query.exec("SELECT * FROM Groups");
    while(query.next())
    {
        for(int index = 0; index < (GROUP_ITEMS + 1); index++) groupLoaded << query.value(index).toString();

        Year year(groupLoaded.at(2).toInt(), groupLoaded.at(3).toInt(), groupLoaded.at(4).toInt());
        StudyType studyType;
        switch(groupLoaded.at(5).toInt())
        {
        case 1: studyType = StudyType::eBc; break;
        case 2: studyType = StudyType::eMgr; break;
        case 3: default: studyType = StudyType::ePhD; break;
        }
        StudyForm studyForm;
        switch(groupLoaded.at(6).toInt())
        {
        case 1: studyForm = StudyForm::eP; break;
        case 2: default: studyForm = StudyForm::eK; break;
        }
        Language language;
        switch(groupLoaded.at(7).toInt())
        {
        case 1: language = Language::eCZ; break;
        case 2: default: language = Language::eEN; break;
        }
        group = QSharedPointer<Group>(new Group(groupLoaded.at(0), groupLoaded.at(1), year, studyType, studyForm, language));
        groupList.append(group);
        groupLoaded.clear();
    }
    return groupList;
}


