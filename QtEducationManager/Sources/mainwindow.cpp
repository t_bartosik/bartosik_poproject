/**
 * @file        mainwindow.cpp
 * @author      Tomas Bartosik
 * @date        26.02.2021
 * @brief       main definition file for MainWindow
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/mainwindow.hpp"
#include "ui_mainwindow.h"

/*Class definition: ---------------------------------------------------------*/
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    mUserInterface(new Ui::MainWindow), mCourseList(QList<QSharedPointer<Course>> ()),
    mEmployeeList(QList<QSharedPointer<Employee>> ()), mWorkLabelList(QList<QSharedPointer<WorkLabel>> ()),
    mGroupList(QList<QSharedPointer<Group>> ()), mCourse(new Course), mEmployee(new Employee),
    mWorkLabel(new WorkLabel), mGroup(new Group), mWorkLabelListFilter(QStringList()), mEditingEnabled(false),
    mUpdatingEnabled(false), mUpdatedItem(0)
{
    mUserInterface->setupUi(this);
    MainWindow_InitializeSettings();
    MainWindow_InitializeUI();

    connect(mUserInterface->actionCourses, &QAction::triggered, this, &MainWindow::MainWindow_EnableCourses);
    connect(mUserInterface->actionEmployees, &QAction::triggered, this, &MainWindow::MainWindow_EnableEmployees);
    connect(mUserInterface->actionWorkLabels, &QAction::triggered, this, &MainWindow::MainWindow_EnableWorkLabels);
    connect(mUserInterface->actionGroups, &QAction::triggered, this, &MainWindow::MainWindow_EnableGroups);
    connect(mUserInterface->actionNotification_center, &QAction::triggered, this, &MainWindow::MainWindow_EnableNotifications);
    connect(mUserInterface->actionAbout, &QAction::triggered, this, &MainWindow::MainWindow_EnableAbout);
    connect(mUserInterface->actionXML_files, &QAction::triggered, this, &MainWindow::MainWindow_UsingXmlFiles);
    connect(mUserInterface->actionLocal_database, &QAction::triggered, this, &MainWindow::MainWindow_UsingDatabase);
    connect(mUserInterface->actionCzech, &QAction::triggered, this, &MainWindow::MainWindow_TranslateIntoCzech);
    connect(mUserInterface->actionEnglish, &QAction::triggered, this, &MainWindow::MainWindow_TranslateIntoEnglish);
    connect(mUserInterface->newCourseButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_AddTableRow(COURSE_TABLE_ID);});
    connect(mUserInterface->newEmployeeButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_AddTableRow(EMPLOYEE_TABLE_ID);});
    connect(mUserInterface->newWorkLabelButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_AddTableRow(WORKLABEL_TABLE_ID);});
    connect(mUserInterface->newGroupButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_AddTableRow(GROUP_TABLE_ID);});
    connect(mUserInterface->removeCourseButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_RemoveTableRow(COURSE_TABLE_ID);});
    connect(mUserInterface->removeEmployeeButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_RemoveTableRow(EMPLOYEE_TABLE_ID);});
    connect(mUserInterface->removeWorkLabelButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_RemoveTableRow(WORKLABEL_TABLE_ID);});
    connect(mUserInterface->removeGroupButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_RemoveTableRow(GROUP_TABLE_ID);});
    connect(mUserInterface->updateGroupButton, &QPushButton::clicked, this, &MainWindow::MainWindow_UpdateTableRow);
    connect(mUserInterface->manageCourseGroupsButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_ConnectData(GROUP_MANAGEMENT);});
    connect(mUserInterface->manageWorkLabelButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_ConnectData(WORKLABEL_MANAGEMENT);});
    connect(mUserInterface->calculateWorkPointsButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_ConnectData(EMPLOYEE_MANAGEMENT);});
    connect(mUserInterface->autoAssignWorkLabelButton, &QPushButton::clicked, this, [this]{MainWindow::MainWindow_ProcessLabels(ASSIGN_LABELS);});
    connect(mUserInterface->workLabelComboBox, &QComboBox::currentTextChanged, this, [this]{MainWindow::MainWindow_FilterLabels(mUserInterface->workLabelComboBox->currentIndex());});
}

MainWindow::~MainWindow()
{
    delete mTranslator;
    delete mUserInterface;
    mCourseList.clear();
    mEmployeeList.clear();
    mWorkLabelList.clear();
    mGroupList.clear();
}

void MainWindow::MainWindow_InitializeSettings()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Warning);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Settings file error"));
    errMsg.setText(tr("Work point settings do not match required format"));
    QStringList workPoints = mXmlParser.XmlParser_LoadWorkPointData();
    if(mWorkPoint.WorkPoint_SetWorkPoints(workPoints) == false) errMsg.exec();

    QSettings applicationSettings;
    if(applicationSettings.value("general/source", "xml") == "xml")
    {
        mUserInterface->actionXML_files->setChecked(true);
        mUserInterface->actionLocal_database->setChecked(false);
    }
    else
    {
        mUserInterface->actionXML_files->setChecked(false);
        mUserInterface->actionLocal_database->setChecked(true);
    }

    if(applicationSettings.value("general/language", "cs") == "cs")
    {
        mUserInterface->actionCzech->setChecked(true);
        mUserInterface->actionEnglish->setChecked(false);
    }
    else
    {
        mUserInterface->actionCzech->setChecked(false);
        mUserInterface->actionEnglish->setChecked(true);
    }

    mTranslator = new QTranslator();
    mTranslator->load(":/languages/EducationManager_" + applicationSettings.value("general/language", "cs").toString() + ".qm");
    qApp->installTranslator(mTranslator);
    mUserInterface->retranslateUi(this);
}

void MainWindow::MainWindow_InitializeUI()
{
    mUserInterface->home_label->setText(tr("Education Manager"));

    mUserInterface->course_widget->hide();
    mUserInterface->employee_widget->hide();
    mUserInterface->workLabel_widget->hide();
    mUserInterface->group_widget->hide();
    mUserInterface->home_widget->show();

    mUserInterface->actionCourses->setChecked(false);
    mUserInterface->actionEmployees->setChecked(false);
    mUserInterface->actionWorkLabels->setChecked(false);
    mUserInterface->actionGroups->setChecked(false);

    mUserInterface->courseTable_widget->setColumnCount(COURSE_ITEMS);
    mUserInterface->courseTable_widget->setHorizontalHeaderLabels(
                {tr("Course\nshortcut"), tr("Course name"), tr("Lect.\nlimit"), tr("Prac.\nlimit"), tr("Sem.\nlimit"),
                tr("At.\nlimit"), tr("Lecture\nhours"), tr("Practice\nhours"), tr("Seminar\nhours"), tr("Atelier\nhours"),
                tr("Weeks"), tr("Credits"), tr("Semester"), tr("Ending"), tr("Language")});
    mUserInterface->courseTable_widget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->courseTable_widget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->courseTable_widget->setSelectionBehavior(QAbstractItemView::SelectRows);

    mUserInterface->employeeTable_widget->setColumnCount(EMPLOYEE_ITEMS);
    mUserInterface->employeeTable_widget->setHorizontalHeaderLabels(
                {tr("Name with titles"), tr("Function"), tr("E-mail"), tr("Private\nphone number"),
                tr("Is doctoral?"), tr("Workroom"), tr("Workroom\nphone number")});
    mUserInterface->employeeTable_widget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->employeeTable_widget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->employeeTable_widget->setSelectionBehavior(QAbstractItemView::SelectRows);

    mUserInterface->workLabelTable_widget->setColumnCount(WORKLABEL_ITEMS);
    mUserInterface->workLabelTable_widget->setHorizontalHeaderLabels(
                {tr("Work label name"), tr("Scheduled\nweeks"), tr("Scheduled\nhours (total)"),
                tr("Student count"), tr("Work points"), tr("Label type"), tr("Course type")});
    mUserInterface->workLabelTable_widget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->workLabelTable_widget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->workLabelTable_widget->setSelectionBehavior(QAbstractItemView::SelectRows);
    mUserInterface->workLabelTable_widget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mUserInterface->workLabelComboBox->clear();
    mUserInterface->workLabelComboBox->addItems(QStringList({tr("all labels"), tr("fully unassigned labels"),
                 tr("assigned to course only"), tr("fully assigned labels")}));

    mUserInterface->groupTable_widget->setColumnCount(GROUP_ITEMS);
    mUserInterface->groupTable_widget->setHorizontalHeaderLabels(
                {tr("Specialization"), tr("Year"), tr("Summer semester\nstudents"),
                 tr("Winter semester\nstudents"), tr("Study type"), tr("Study form"),
                 tr("Language")});
    mUserInterface->groupTable_widget->setTextElideMode(Qt::ElideMiddle);
    mUserInterface->groupTable_widget->horizontalHeader()->setStretchLastSection(true);
    mUserInterface->groupTable_widget->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void MainWindow::MainWindow_ResizeTables(unsigned int tableID)
{
    if(tableID == COURSE_TABLE_ID || tableID == ALL_TABLES_ID)
    {
        auto index = 0;
        auto columnsWidth = (mUserInterface->courseTable_widget->width()
                - mUserInterface->courseTable_widget->verticalHeader()->width()
                - mUserInterface->courseTable_widget->verticalScrollBar()->width()) / (COURSE_ITEMS * 4);
        for(auto ratio : {5, 8, 3, 3, 3, 3, 4, 4, 4, 4, 3, 3, 7, 5, 1})
        {
            mUserInterface->courseTable_widget->setColumnWidth(index, (columnsWidth * ratio));
            index++;
        }
    }

    if(tableID == EMPLOYEE_TABLE_ID || tableID == ALL_TABLES_ID)
    {
        auto index = 0;
        auto columnsWidth = (mUserInterface->employeeTable_widget->width()
                - mUserInterface->employeeTable_widget->verticalHeader()->width()
                - mUserInterface->employeeTable_widget->verticalScrollBar()->width()) / (EMPLOYEE_ITEMS * 3);
        for(auto ratio : {5, 4, 4, 3, 2, 2, 1})
        {
            mUserInterface->employeeTable_widget->setColumnWidth(index, (columnsWidth * ratio));
            index++;
        }
    }

    if(tableID == WORKLABEL_TABLE_ID || tableID == ALL_TABLES_ID)
    {
        auto index = 0;
        auto columnsWidth = (mUserInterface->workLabelTable_widget->width()
                - mUserInterface->workLabelTable_widget->verticalHeader()->width()
                - mUserInterface->workLabelTable_widget->verticalScrollBar()->width()) / (WORKLABEL_ITEMS * 3);
        for(auto ratio : {4, 3, 3, 3, 3, 3, 2})
        {
            mUserInterface->workLabelTable_widget->setColumnWidth(index, (columnsWidth * ratio));
            index++;
        }
    }

    if(tableID == GROUP_TABLE_ID || tableID == ALL_TABLES_ID)
    {
        auto index = 0;
        auto columnsWidth = (mUserInterface->groupTable_widget->width()
                - mUserInterface->groupTable_widget->verticalHeader()->width()
                - mUserInterface->groupTable_widget->verticalScrollBar()->width()) / (GROUP_ITEMS * 3);
        for(auto ratio : {6, 2, 3, 3, 2, 3, 2})
        {
            mUserInterface->groupTable_widget->setColumnWidth(index, (columnsWidth * ratio));
            index++;
        }
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    MainWindow_ResizeTables(ALL_TABLES_ID);
    QMainWindow::resizeEvent(event);
}

void MainWindow::MainWindow_DrawTableContent(unsigned int tableID)
{
    QTableWidgetItem *prototype = nullptr;

    if(tableID == COURSE_TABLE_ID)
    {
        mUserInterface->courseTable_widget->setRowCount(mCourseList.count());
        for(int row = 0; row < mCourseList.count(); row++)
        {
            QStringList rowList = mCourseList.at(row)->Course_GetAll();
            for(int column = 0; column < (rowList.count() - 1); column++)
            {
                prototype = new QTableWidgetItem(rowList.at(column + 1));
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->courseTable_widget->setItem(row, column, prototype);
            }
        }
    }

    if(tableID == EMPLOYEE_TABLE_ID)
    {
        mUserInterface->employeeTable_widget->setRowCount(mEmployeeList.count());
        for(int row = 0; row < mEmployeeList.count(); row++)
        {
            QStringList rowList = mEmployeeList.at(row)->Employee_GetAll();
            for(int column = 0; column < (rowList.count() - 1); column++)
            {
                prototype = new QTableWidgetItem(rowList.at(column + 1));
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->employeeTable_widget->setItem(row, column, prototype);
            }
        }
    }

    if(tableID == WORKLABEL_TABLE_ID)
    {
        if(mWorkLabelListFilter.count() > 0)
        {
            mUserInterface->workLabelTable_widget->setRowCount(mWorkLabelListFilter.count());
            int filterIndex = 0;
            int rowIndex = 0;
            for(int row = 0; row < mWorkLabelList.count(); row++)
            {
                if(QString::compare(mWorkLabelList.at(row)->WorkLabel_GetWorkLabelID(), mWorkLabelListFilter.at(filterIndex), Qt::CaseSensitive) == 0)
                {
                    QStringList rowList = mWorkLabelList.at(row)->WorkLabel_GetAll();
                    for(int column = 0; column < (rowList.count() - 1); column++)
                    {
                        prototype = new QTableWidgetItem(rowList.at(column + 1));
                        prototype->setTextAlignment(Qt::AlignCenter);
                        mUserInterface->workLabelTable_widget->setItem(rowIndex, column, prototype);
                    }
                    rowIndex++;
                    if(filterIndex < (mWorkLabelListFilter.count() - 1)) filterIndex++;
                }
            }
        }
        else mUserInterface->workLabelTable_widget->setRowCount(0);
    }

    if(tableID == GROUP_TABLE_ID)
    {
        mUserInterface->groupTable_widget->setRowCount(mGroupList.count());
        for(int row = 0; row < mGroupList.count(); row++)
        {
            QStringList rowList = mGroupList.at(row)->Group_GetAll();
            for(int column = 0; column < (rowList.count() - 1); column++)
            {
                prototype = new QTableWidgetItem(rowList.at(column + 1));
                prototype->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled|Qt::ItemIsEditable);
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->groupTable_widget->setItem(row, column, prototype);
            }
        }
    }
}

void MainWindow::MainWindow_AddTableRow(unsigned int tableID)
{
    int row = 0;
    QStringList newEntry = QStringList();
    QString entryCheckResult;
    QString hashedID;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Entry error"));

    if(tableID == COURSE_TABLE_ID)
    {
        row = mUserInterface->courseTable_widget->rowCount();
        if(mEditingEnabled == false)
        {
            mEditingEnabled = true;
            mUserInterface->manageCourseGroupsButton->setEnabled(false);
            QComboBox *semesterCB = new QComboBox();
            semesterCB->addItems(QStringList({tr("summer semester"), tr("winter semester")}));
            QComboBox *endingCB = new QComboBox();
            endingCB->addItems(QStringList({tr("credit test"), tr("classified\n credit test"), tr("exam"), tr("credit test\n & exam")}));
            QComboBox *languageCB = new QComboBox();
            languageCB->addItems(QStringList({"CZ", "EN"}));
            mUserInterface->courseTable_widget->insertRow(row);
            mUserInterface->courseTable_widget->setCellWidget(row, 12, semesterCB);
            mUserInterface->courseTable_widget->setCellWidget(row, 13, endingCB);
            mUserInterface->courseTable_widget->setCellWidget(row, 14, languageCB);
            for(int column = 0; column < COURSE_ITEMS; column++)
            {
                QTableWidgetItem *prototype = new QTableWidgetItem();
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->courseTable_widget->setItem(row, column, prototype);
            }
            mUserInterface->courseTable_widget->setCurrentCell(row, 0);
            mUserInterface->courseTable_widget->edit(mUserInterface->courseTable_widget->model()->index(row, 0));
            mUserInterface->newCourseButton->setText(tr("Confirm new course entry"));
            mUserInterface->removeCourseButton->hide();
            MainWindow_ResizeTables(COURSE_TABLE_ID);
        }
        else
        {
            mUserInterface->manageCourseGroupsButton->setEnabled(true);
            for(int column = 0; column < COURSE_ITEMS; column++)
            {
                if(column < 12)
                {
                    if(mUserInterface->courseTable_widget->item(row - 1, column) != nullptr)
                    {
                        newEntry << mUserInterface->courseTable_widget->item(row - 1, column)->text();
                    }
                }
                else
                {
                    QComboBox *cb = (QComboBox*)mUserInterface->courseTable_widget->cellWidget(row - 1, column);
                    newEntry << QString::number(cb->currentIndex());
                }
            }
            entryCheckResult = MainWindow_CheckTableRow(COURSE_TABLE_ID, newEntry);
            if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
            {
                hashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));
                Lesson ls(newEntry.at(2).toInt(), newEntry.at(3).toInt(), newEntry.at(4).toInt(), newEntry.at(5).toInt());
                SemesterType semesterType;
                switch(newEntry.at(12).toInt())
                {
                case 0: semesterType = SemesterType::eLS; break;
                case 1: default: semesterType = SemesterType::eZS; break;
                }
                Ending ending;
                switch(newEntry.at(13).toInt())
                {
                case 0: ending = Ending::eZ; break;
                case 1: ending = Ending::eKZ; break;
                case 2: ending = Ending::eZk; break;
                case 3: default: ending = Ending::eZZk; break;
                }
                Semester sm(newEntry.at(6).toInt(), newEntry.at(7).toInt(), newEntry.at(8).toInt(),
                            newEntry.at(9).toInt(), newEntry.at(10).toInt(), newEntry.at(11).toInt(), semesterType, ending);
                Language language;
                switch(newEntry.at(14).toInt())
                {
                case 0: language = Language::eCZ; break;
                case 1: default: language = Language::eEN; break;
                }
                mCourse = QSharedPointer<Course>(new Course(hashedID, newEntry.at(0), newEntry.at(1), ls, sm, language, QList<QSharedPointer<Group>> ()));
                mCourseList.append(mCourse);

                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveCourseData(mCourseList);
                }
                else
                {
                    mDbParser.DbParser_InsertCourse(mCourse);
                }

                mUserInterface->courseTable_widget->removeRow(row - 1);
                MainWindow_EnableCourses();
            }
            else
            {
                errMsg.setText(entryCheckResult);
                errMsg.exec();
            }
        }
    }

    if(tableID == EMPLOYEE_TABLE_ID)
    {
        row = mUserInterface->employeeTable_widget->rowCount();
        if(mEditingEnabled == false)
        {
            mEditingEnabled = true;
            QComboBox *isDoctoralCB = new QComboBox();
            isDoctoralCB->addItems(QStringList({tr("No"), tr("Yes")}));
            mUserInterface->employeeTable_widget->insertRow(row);
            mUserInterface->employeeTable_widget->setCellWidget(row, 4, isDoctoralCB);
            for(int column = 0; column < EMPLOYEE_ITEMS; column++)
            {
                QTableWidgetItem *prototype = new QTableWidgetItem();
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->employeeTable_widget->setItem(row, column, prototype);
            }
            mUserInterface->employeeTable_widget->setCurrentCell(row, 0);
            mUserInterface->employeeTable_widget->edit(mUserInterface->employeeTable_widget->model()->index(row, 0));
            mUserInterface->newEmployeeButton->setText(tr("Confirm new employee entry"));
            mUserInterface->removeEmployeeButton->hide();
            MainWindow_ResizeTables(EMPLOYEE_TABLE_ID);
        }
        else
        {
            for(int column = 0; column < EMPLOYEE_ITEMS; column++)
            {
                if(column == 4)
                {
                    QComboBox *cb = (QComboBox*)mUserInterface->employeeTable_widget->cellWidget(row - 1, column);
                    newEntry << QString::number(cb->currentIndex());
                }
                else
                {
                    if(mUserInterface->employeeTable_widget->item(row - 1, column) != nullptr)
                    {
                        newEntry << mUserInterface->employeeTable_widget->item(row - 1, column)->text();
                    }
                    else newEntry << "";
                }
            }
            entryCheckResult = MainWindow_CheckTableRow(EMPLOYEE_TABLE_ID, newEntry);
            if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
            {
                hashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));
                bool isDoctoral = false;
                switch(newEntry.at(4).toInt())
                {
                case 0: isDoctoral = false; break;
                case 1: default: isDoctoral = true; break;
                }
                Workroom wr(newEntry.at(5), newEntry.at(6));
                mEmployee = QSharedPointer<Employee>(new Employee(hashedID, newEntry.at(0), newEntry.at(1), newEntry.at(2),
                     newEntry.at(3), isDoctoral, wr));
                mEmployeeList.append(mEmployee);

                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveEmployeeData(mEmployeeList);
                }
                else
                {
                    mDbParser.DbParser_InsertEmployee(mEmployee);
                }

                mUserInterface->employeeTable_widget->removeRow(row - 1);
                MainWindow_EnableEmployees();
            }
            else
            {
                errMsg.setText(entryCheckResult);
                errMsg.exec();
            }
        }
    }

    if(tableID == WORKLABEL_TABLE_ID)
    {
        row = mUserInterface->workLabelTable_widget->rowCount();
        if(mEditingEnabled == false)
        {
            mEditingEnabled = true;
            QComboBox *labelTypeCB = new QComboBox();
            labelTypeCB->addItems(QStringList({tr("lecture"), tr("practice"), tr("seminar"), tr("atelier"),
                 tr("credit test"), tr("classified credit test"), tr("exam")}));
            QComboBox *courseTypeCB = new QComboBox();
            courseTypeCB->addItems(QStringList({tr("standard course"), tr("foreign language"), tr("doctoral course")}));
            mUserInterface->workLabelTable_widget->insertRow(row);
            mUserInterface->workLabelTable_widget->setCellWidget(row, 5, labelTypeCB);
            mUserInterface->workLabelTable_widget->setCellWidget(row, 6, courseTypeCB);
            for(int column = 0; column < WORKLABEL_ITEMS; column++)
            {
                QTableWidgetItem *prototype = new QTableWidgetItem();
                prototype->setTextAlignment(Qt::AlignCenter);
                if(column == 4)
                {
                    prototype->setText(QString::number(0));
                    prototype->setFlags(Qt::ItemFlags(!Qt::ItemIsEditable));
                }
                mUserInterface->workLabelTable_widget->setItem(row, column, prototype);
            }
            mUserInterface->workLabelTable_widget->setCurrentCell(row, 0);
            mUserInterface->workLabelTable_widget->edit(mUserInterface->workLabelTable_widget->model()->index(row, 0));
            mUserInterface->newWorkLabelButton->setText(tr("Confirm new work label entry"));
            mUserInterface->removeWorkLabelButton->hide();
            MainWindow_ResizeTables(WORKLABEL_TABLE_ID);
        }
        else
        {
            for(int column = 0; column < WORKLABEL_ITEMS; column++)
            {
                if(column < 5)
                {
                    if(mUserInterface->workLabelTable_widget->item(row - 1, column) != nullptr)
                    {
                        newEntry << mUserInterface->workLabelTable_widget->item(row - 1, column)->text();
                    }
                }
                else
                {
                    QComboBox *cb = (QComboBox*)mUserInterface->workLabelTable_widget->cellWidget(row - 1, column);
                    newEntry << QString::number(cb->currentIndex());
                }
            }
            entryCheckResult = MainWindow_CheckTableRow(WORKLABEL_TABLE_ID, newEntry);
            if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
            {
                hashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));
                LabelType labelType;
                switch(newEntry.at(5).toInt())
                {
                case 0: labelType = LabelType::eLecture; break;
                case 1: labelType = LabelType::ePractice; break;
                case 2: labelType = LabelType::eSeminar; break;
                case 3: labelType = LabelType::eAtelier; break;
                case 4: labelType = LabelType::eZ; break;
                case 5: labelType = LabelType::eKZ; break;
                case 6: default: labelType = LabelType::eZk; break;
                }
                CourseType courseType;
                switch(newEntry.at(6).toInt())
                {
                case 0: courseType = CourseType::eStandard; break;
                case 1: courseType = CourseType::eForeignLanguage; break;
                case 2: default: courseType = CourseType::eDoctoral; break;
                }
                float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(newEntry.at(2).toInt(),
                      newEntry.at(5).toInt(), newEntry.at(6).toInt());
                mWorkLabel = QSharedPointer<WorkLabel>(new WorkLabel(hashedID, newEntry.at(0), newEntry.at(1).toInt(),
                     newEntry.at(2).toInt(), newEntry.at(3).toInt(), workPoints, labelType, courseType, nullptr, nullptr));
                mWorkLabelList.append(mWorkLabel);

                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
                }
                else
                {
                    mDbParser.DbParser_InsertWorkLabel(mWorkLabel);
                }

                mUserInterface->workLabelTable_widget->removeRow(row - 1);
                MainWindow_EnableWorkLabels();
            }
            else
            {
                errMsg.setText(entryCheckResult);
                errMsg.exec();
            }
        }
    }

    if(tableID == GROUP_TABLE_ID)
    {
        row = mUserInterface->groupTable_widget->rowCount();
        if(mEditingEnabled == false)
        {
            mEditingEnabled = true;
            QComboBox *studyTypeCB = new QComboBox();
            studyTypeCB->addItems(QStringList({"Bc.", "Mgr.", "Ph. D."}));
            QComboBox *studyFormCB = new QComboBox();
            studyFormCB->addItems(QStringList({tr("full-time"), tr("combined")}));
            QComboBox *languageCB = new QComboBox();
            languageCB->addItems(QStringList({"CZ", "EN"}));
            mUserInterface->groupTable_widget->insertRow(row);
            mUserInterface->groupTable_widget->setCellWidget(row, 4, studyTypeCB);
            mUserInterface->groupTable_widget->setCellWidget(row, 5, studyFormCB);
            mUserInterface->groupTable_widget->setCellWidget(row, 6, languageCB);
            for(int column = 0; column < GROUP_ITEMS; column++)
            {
                QTableWidgetItem *prototype = new QTableWidgetItem();
                prototype->setTextAlignment(Qt::AlignCenter);
                mUserInterface->groupTable_widget->setItem(row, column, prototype);
            }
            mUserInterface->groupTable_widget->setCurrentCell(row, 0);
            mUserInterface->groupTable_widget->edit(mUserInterface->groupTable_widget->model()->index(row, 0));
            mUserInterface->newGroupButton->setText(tr("Confirm new group entry"));
            mUserInterface->updateGroupButton->hide();
            mUserInterface->removeGroupButton->hide();
            MainWindow_ResizeTables(GROUP_TABLE_ID);
        }
        else
        {
            for(int column = 0; column < GROUP_ITEMS; column++)
            {
                if(column < 4)
                {
                    if(mUserInterface->groupTable_widget->item(row - 1, column) != nullptr)
                    {
                        newEntry << mUserInterface->groupTable_widget->item(row - 1, column)->text();
                    }
                }
                else
                {
                    QComboBox *cb = (QComboBox*)mUserInterface->groupTable_widget->cellWidget(row - 1, column);
                    newEntry << QString::number(cb->currentIndex());
                }
            }

            entryCheckResult = MainWindow_CheckTableRow(GROUP_TABLE_ID, newEntry);
            if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
            {
                hashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));
                Year year(newEntry.at(1).toInt(), newEntry.at(2).toInt(), newEntry.at(3).toInt());
                StudyType studyType;
                switch(newEntry.at(4).toInt())
                {
                case 0: studyType = StudyType::eBc; break;
                case 1: studyType = StudyType::eMgr; break;
                case 2: default: studyType = StudyType::ePhD; break;
                }
                StudyForm studyForm;
                switch(newEntry.at(5).toInt())
                {
                case 0: studyForm = StudyForm::eP; break;
                case 1: default: studyForm = StudyForm::eK; break;
                }
                Language language;
                switch(newEntry.at(6).toInt())
                {
                case 0: language = Language::eCZ; break;
                case 1: default: language = Language::eEN; break;
                }
                mGroup = QSharedPointer<Group>(new Group(hashedID, newEntry.at(0), year, studyType, studyForm, language));
                mGroupList.append(mGroup);

                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveGroupData(mGroupList);
                }
                else
                {
                    mDbParser.DbParser_InsertGroup(mGroup);
                }

                mUserInterface->groupTable_widget->removeRow(row - 1);
                MainWindow_EnableGroups();
            }
            else
            {
                errMsg.setText(entryCheckResult);
                errMsg.exec();
            }
        }
    }
}

QString MainWindow::MainWindow_CheckTableRow(unsigned int tableID, QStringList newEntry)
{
    QString result;
    QVariant numberFromString;
    int integralNumber = -1;
    int correct = 0;

    if(tableID == COURSE_TABLE_ID)
    {
        if(newEntry.count() == COURSE_ITEMS)
        {
            result = tr("Course shortcut should contain at least one character.");
            if(newEntry.at(0).size() < 1) return result;
            else correct++;

            result = tr("Course name should contain at least one character.");
            if(newEntry.at(1).size() < 1) return result;
            else correct++;

            result = tr("Lecture capacity should be defined as a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(2));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(2), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Practice capacity should be defined as a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(3));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(3), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Seminar capacity should be defined as a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(4));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(4), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Atelier capacity should be defined as a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(5));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(5), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Lecture hours are represented by a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(6));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(6), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Practice hours are represented by a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(7));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(7), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Seminar hours are represented by a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(8));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(8), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Atelier hours are represented by a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(9));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(9), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of course weeks should be defined as a natural number.");
            numberFromString = QVariant(newEntry.at(10));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of course credits should match a non-negative integral number.");
            numberFromString = QVariant(newEntry.at(11));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0 || QString::compare(newEntry.at(11), QString("0"), Qt::CaseSensitive) == 0) correct++;
                else return result;
            }
            else return result;

            if(correct == 12) return QString("OK");
        }
        return tr("Some columns have not been filled.");
    }

    if(tableID == EMPLOYEE_TABLE_ID)
    {
        if(newEntry.count() == EMPLOYEE_ITEMS)
        {
            result = tr("Employee name should contain at least one character.");
            if(newEntry.at(0).size() < 1) return result;
            else correct++;

            result = tr("Employee function should contain at least one character.");
            if(newEntry.at(1).size() < 1) return result;
            else correct++;

            result = tr("E-mail format does not match requirements.");
            QRegularExpression mail("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}");
            if(!mail.match(newEntry.at(2)).hasMatch()) return result;
            else correct++;

            result = tr("Private phone number does not match required format.");
            QRegularExpression phone("^(\\+?420)? ?[0-9]{3} ?[0-9]{3} ?[0-9]{3}$");
            if(newEntry.at(3).size() < 1) correct++;
            else if(!phone.match(newEntry.at(3)).hasMatch()) return result;
            else correct++;

            result = tr("Workroom label should contain at least one character.");
            if(newEntry.at(5).size() < 1) return result;
            else correct++;

            result = tr("Workroom phone number does not match required format.");
            if(!phone.match(newEntry.at(6)).hasMatch()) return result;
            else correct++;

            if(correct == 6) return QString("OK");
        }
        return tr("Some columns have not been filled.");
    }

    if(tableID == WORKLABEL_TABLE_ID)
    {
        if(newEntry.count() == WORKLABEL_ITEMS)
        {
            result = tr("Work label name should be defined by at least one character.");
            if(newEntry.at(0).size() < 1) return result;
            else correct++;

            result = tr("Number of scheduled weeks should be defined by a natural number.");
            numberFromString = QVariant(newEntry.at(1));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of scheduled hours should be defined by a natural number.");
            numberFromString = QVariant(newEntry.at(2));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of students should be defined by a natural number.");
            numberFromString = QVariant(newEntry.at(3));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber > 0) correct++;
                else return result;
            }
            else return result;

            if(correct == 4) return QString("OK");
        }
        return tr("Some columns have not been filled.");
    }

    if(tableID == GROUP_TABLE_ID)
    {
        if(newEntry.count() == GROUP_ITEMS)
        {
            result = tr("Specialization should contain at least one character.");
            if(newEntry.at(0).size() < 1) return result;
            else correct++;

            result = tr("A year is represented by an integral number in range from 1 to 4.");
            numberFromString = QVariant(newEntry.at(1));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber >= 1 && integralNumber <= 4) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of summer semester students does not match an integral number.");
            numberFromString = QVariant(newEntry.at(2));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber >= 1) correct++;
                else return result;
            }
            else return result;

            result = tr("Number of winter semester students does not match an integral number.");
            numberFromString = QVariant(newEntry.at(3));
            if(numberFromString.canConvert<int>())
            {
                integralNumber = numberFromString.toInt();
                if(integralNumber >= 1) correct++;
                else return result;
            }
            else return result;

            if(correct == 4) return QString("OK");
        }
        return tr("Some columns have not been filled.");
    }

    return tr("Unexpected behavior.");
}

void MainWindow::MainWindow_RemoveTableRow(unsigned int tableID)
{
    QStringList removalIdList;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Removal error"));
    errMsg.setText(tr("Select one or more rows in order to remove them."));

    if(tableID == COURSE_TABLE_ID)
    {
        QModelIndexList selectedList = mUserInterface->courseTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() < 1) errMsg.exec();
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
            msgBox.setWindowTitle(tr("Removal confirmation"));
            msgBox.setText(tr("Do you really want to remove selected row(s)?"));
            QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
            msgBox.addButton(tr("No"), QMessageBox::NoRole);
            msgBox.exec();
            if(msgBox.clickedButton() == buttonYes)
            {
                std::sort(selectedList.begin(), selectedList.end());
                for(int index = selectedList.count() - 1; index >= 0; index--)
                {
                    if(mUserInterface->actionLocal_database->isChecked() == true)
                    {
                        removalIdList << mCourseList.at(selectedList.at(index).row())->Course_GetCourseID();
                    }
                    mCourseList.removeAt(selectedList.at(index).row());
                }
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveCourseData(mCourseList);
                }
                else if(removalIdList.count() > 0)
                {
                    mDbParser.DbParser_DeleteCourses(removalIdList);
                }

                MainWindow_EnableCourses();
            }
        }
    }

    if(tableID == EMPLOYEE_TABLE_ID)
    {
        QModelIndexList selectedList = mUserInterface->employeeTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() < 1) errMsg.exec();
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
            msgBox.setWindowTitle(tr("Removal confirmation"));
            msgBox.setText(tr("Do you really want to remove selected row(s)?"));
            QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
            msgBox.addButton(tr("No"), QMessageBox::NoRole);
            msgBox.exec();
            if(msgBox.clickedButton() == buttonYes)
            {
                std::sort(selectedList.begin(), selectedList.end());
                for(int index = selectedList.count() - 1; index >= 0; index--)
                {
                    if(mUserInterface->actionLocal_database->isChecked() == true)
                    {
                        removalIdList << mEmployeeList.at(selectedList.at(index).row())->Employee_GetEmployeeID();
                    }
                    mEmployeeList.removeAt(selectedList.at(index).row());
                }
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveEmployeeData(mEmployeeList);
                }
                else if(removalIdList.count() > 0)
                {
                    mDbParser.DbParser_DeleteEmployees(removalIdList);
                }

                MainWindow_EnableEmployees();
            }
        }
    }

    if(tableID == WORKLABEL_TABLE_ID)
    {
        QModelIndexList selectedList = mUserInterface->workLabelTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() < 1) errMsg.exec();
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
            msgBox.setWindowTitle(tr("Removal confirmation"));
            msgBox.setText(tr("Do you really want to remove selected row(s)?"));
            QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
            msgBox.addButton(tr("No"), QMessageBox::NoRole);
            msgBox.exec();
            if(msgBox.clickedButton() == buttonYes)
            {
                std::sort(selectedList.begin(), selectedList.end());
                for(int index = selectedList.count() - 1; index >= 0; index--)
                {
                    QString hash = mWorkLabelListFilter.at(selectedList.at(index).row());
                    for(int position = mWorkLabelList.count() - 1; position >= 0; position--)
                    {
                        if(QString::compare(mWorkLabelList.at(position)->WorkLabel_GetWorkLabelID(), hash, Qt::CaseSensitive) == 0)
                        {
                            if(mUserInterface->actionLocal_database->isChecked() == true)
                            {
                                removalIdList << mWorkLabelList.at(position)->WorkLabel_GetWorkLabelID();
                            }
                            mWorkLabelList.removeAt(position);
                        }
                    }
                }
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
                }
                else if(removalIdList.count() > 0)
                {
                    mDbParser.DbParser_DeleteWorkLabelsByID(removalIdList);
                }

                MainWindow_EnableWorkLabels();
            }
        }
    }

    if(tableID == GROUP_TABLE_ID)
    {
        QModelIndexList selectedList = mUserInterface->groupTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() < 1) errMsg.exec();
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
            msgBox.setWindowTitle(tr("Removal confirmation"));
            msgBox.setText(tr("Do you really want to remove selected row(s)?"));
            QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
            msgBox.addButton(tr("No"), QMessageBox::NoRole);
            msgBox.exec();
            if(msgBox.clickedButton() == buttonYes)
            {
                std::sort(selectedList.begin(), selectedList.end());
                for(int index = selectedList.count() - 1; index >= 0; index--)
                {
                    if(mUserInterface->actionLocal_database->isChecked() == true)
                    {
                        removalIdList << mGroupList.at(selectedList.at(index).row())->Group_GetGroupID();
                    }
                    mGroupList.removeAt(selectedList.at(index).row());
                }
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveGroupData(mGroupList);
                }
                else if(removalIdList.count() > 0)
                {
                    mDbParser.DbParser_DeleteGroups(removalIdList);
                }

                MainWindow_EnableGroups();
            }
        }
    }
}

void MainWindow::MainWindow_UpdateTableRow()
{
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Selection error"));
    errMsg.setText(tr("Select one row in order to update it."));

    if(mUserInterface->groupTable_widget->selectedItems().isEmpty() == false)
    {
        if(mUserInterface->groupTable_widget->selectedItems().count() > GROUP_ITEMS) errMsg.exec();
        else if(mUpdatingEnabled == false)
        {
            mUpdatingEnabled = true;
            QTableWidgetItem *groupSelection = mUserInterface->groupTable_widget->selectedItems().first();
            mUpdatedItem = mUserInterface->groupTable_widget->row(groupSelection);

            for(int index = 0; index < GROUP_ITEMS; index++)
            {
                if(index < 2 || index > 3)
                {
                    QTableWidgetItem *prototype = new QTableWidgetItem();
                    prototype->setText(mUserInterface->groupTable_widget->item(mUpdatedItem, index)->text());
                    prototype->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
                    prototype->setTextAlignment(Qt::AlignCenter);
                    mUserInterface->groupTable_widget->setItem(mUpdatedItem, index, prototype);
                }
            }
            mUserInterface->groupTable_widget->setCurrentCell(mUpdatedItem, 2);
            mUserInterface->groupTable_widget->edit(mUserInterface->groupTable_widget->model()->index(mUpdatedItem, 2));
            mUserInterface->updateGroupButton->setText(tr("Confirm row changes"));
            mUserInterface->newGroupButton->hide();
            mUserInterface->removeGroupButton->hide();
        }
        else
        {
            QStringList groupIndexList = QStringList();
            QStringList newEntry;
            QString entryCheckResult;
            QString originalHashedID;
            QString newHashedID;

            for(int column = 0; column < GROUP_ITEMS; column++)
            {
                if(mUserInterface->groupTable_widget->item(mUpdatedItem, column) != nullptr)
                {
                    newEntry << mUserInterface->groupTable_widget->item(mUpdatedItem, column)->text();
                }
            }
            entryCheckResult = MainWindow_CheckTableRow(GROUP_TABLE_ID, newEntry);

            if(QString::compare(entryCheckResult, QString("OK"), Qt::CaseSensitive) == 0)
            {
                originalHashedID = mGroupList.at(mUpdatedItem)->Group_GetGroupID();

                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mCourseList = mXmlParser.XmlParser_LoadCourseData();
                    mWorkLabelList = mXmlParser.XmlParser_LoadWorkLabelData();
                }
                else
                {
                    mCourseList = mDbParser.DbParser_LoadCourseData();
                    mWorkLabelList = mDbParser.DbParser_LoadWorkLabelData();
                }

                newEntry.replace(4, QString::number(mGroupList.at(mUpdatedItem)->Group_GetStudyType(true).toInt() - 1));
                newEntry.replace(5, QString::number(mGroupList.at(mUpdatedItem)->Group_GetStudyForm(true).toInt() - 1));
                newEntry.replace(6, QString::number(mGroupList.at(mUpdatedItem)->Group_GetLanguage(true).toInt() - 1));
                newHashedID = mHasher.Hasher_CalculateHash(newEntry.join(""));
                mGroupList.at(mUpdatedItem)->Group_SetGroupID(newHashedID);
                mGroupList.at(mUpdatedItem)->Group_SetSummerStudentCount(newEntry.at(2).toInt());
                mGroupList.at(mUpdatedItem)->Group_SetWinterStudentCount(newEntry.at(3).toInt());
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveGroupData(mGroupList);
                }
                else
                {
                    mDbParser.DbParser_UpdateGroup(originalHashedID, mGroupList.at(mUpdatedItem));
                    mDbParser.DbParser_UpdateCourseGroup(originalHashedID, newHashedID);
                }

                for(int index = 0; index < mCourseList.count(); index++)
                {
                    groupIndexList.clear();
                    int groupCount = mCourseList.at(index)->Course_GetGroups().count();
                    if(groupCount > 0)
                    {
                        for(int group = 0; group < groupCount; group++)
                        {
                            for(int item = 0; item < mGroupList.count(); item++)
                            {
                                if(QString::compare(mCourseList.at(index)->Course_GetGroups().at(group)->Group_GetGroupID(),
                                mGroupList.at(item)->Group_GetGroupID(), Qt::CaseSensitive) == 0)
                                {
                                    groupIndexList << QString::number(item);
                                }
                            }

                            if(QString::compare(mCourseList.at(index)->Course_GetGroups().at(group)->Group_GetGroupID(), originalHashedID, Qt::CaseSensitive) == 0)
                            {
                                QRegularExpression courseShortcut(mCourseList.at(index)->Course_GetCourseShortcut() + " \\[");
                                bool generateLabels = false;

                                mCourseList.at(index)->Course_ReplaceGroup(group, mGroupList.at(mUpdatedItem));
                                groupIndexList << QString::number(mUpdatedItem);
                                for(int label = 0; label < mWorkLabelList.count(); label++)
                                {
                                    if(courseShortcut.match(mWorkLabelList.at(label)->WorkLabel_GetLabelName()).hasMatch() == true) generateLabels = true;
                                }

                                if(generateLabels == true) MainWindow_AutogenerateLabels(index, groupIndexList);
                            }
                        }
                    }
                }
                if(mUserInterface->actionXML_files->isChecked() == true)
                {
                    mXmlParser.XmlParser_SaveCourseData(mCourseList);
                }

                MainWindow_EnableGroups();
            }
            else
            {
                errMsg.setText(entryCheckResult);
                errMsg.exec();
            }
        }
    }
    else errMsg.exec();
}

void MainWindow::MainWindow_ConnectData(unsigned int source)
{
    DialogWindow dialogWindow(this, source);
    dialogWindow.setModal(true);
    dialogWindow.setWindowTitle("Education Manager");
    dialogWindow.setFixedWidth(800);
    dialogWindow.setMaximumWidth(1024);
    dialogWindow.setMaximumHeight(600);
    dialogWindow.setMinimumWidth(800);
    dialogWindow.setMinimumHeight(480);
    int selectedItem = -1;
    int selectedItemFiltered = -1;
    QMessageBox errMsg;
    errMsg.setIcon(QMessageBox::Information);
    errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
    errMsg.setWindowTitle(tr("Selection error"));
    if(source == GROUP_MANAGEMENT)
    {
        QModelIndexList selectedList = mUserInterface->courseTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() == 1) selectedItem = selectedList.first().row();
        else
        {
            errMsg.setText(tr("Select one course in order to assign groups."));
            errMsg.exec();
            return;
        }

        QList<QStringList> groupContent;
        QStringList currentGroupIDList;
        if(mGroupList.count() < 1)
        {
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mGroupList = mXmlParser.XmlParser_LoadGroupData();
            }
            else
            {
                mGroupList = mDbParser.DbParser_LoadGroupData();
            }
        }
        for(int index = 0; index < mGroupList.count(); index++)
        {
            groupContent.append(mGroupList.at(index)->Group_GetAll());
        }
        dialogWindow.DialogWindow_SetGroupContent(groupContent);
        for(int index = 0; index < mCourseList.at(selectedItem)->Course_GetGroups().count(); index++)
        {
            currentGroupIDList << mCourseList.at(selectedItem)->Course_GetGroups().at(index)->Group_GetGroupID();
        }
        dialogWindow.DialogWindow_DrawGroupContent(currentGroupIDList);
        if(dialogWindow.exec() == QDialog::Accepted)
        {
            QStringList results;
            results = dialogWindow.DialogWindow_GetResults(GROUP_MANAGEMENT);

            mCourseList.at(selectedItem)->Course_ResetGroups();
            if(mUserInterface->actionLocal_database->isChecked() == true)
            {
                mDbParser.DbParser_DeleteCourseGroupsByCourse(mCourseList.at(selectedItem)->Course_GetCourseID());
            }
            for(int index = 0; index < results.count(); index++)
            {
                mCourseList.at(selectedItem)->Course_AppendGroup(mGroupList.at(results.at(index).toInt()));
                if(mUserInterface->actionLocal_database->isChecked() == true)
                {
                    QString groupID = mGroupList.at(results.at(index).toInt())->Group_GetGroupID();
                    mDbParser.DbParser_InsertCourseGroup(mCourseList.at(selectedItem)->Course_GetCourseID(), groupID);
                }
            }

            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mXmlParser.XmlParser_SaveCourseData(mCourseList);
            }

            if(results.count() > 0)
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Question);
                msgBox.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
                msgBox.setWindowTitle(tr("Work labels creation"));
                msgBox.setText(tr("Would you like to automatically create work label(s)?"));
                QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
                msgBox.addButton(tr("No"), QMessageBox::NoRole);
                msgBox.exec();
                if(msgBox.clickedButton() == buttonYes)
                {
                    MainWindow_AutogenerateLabels(selectedItem, results);
                }
            }
        }
    }
    if(source == WORKLABEL_MANAGEMENT)
    {
        QModelIndexList selectedList = mUserInterface->workLabelTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() == 1)
        {
            selectedItem = selectedList.first().row();
            for(int index = 0; index < mWorkLabelList.count(); index++)
            {
                if(QString::compare(mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID(), mWorkLabelListFilter.at(selectedItem), Qt::CaseSensitive) == 0)
                {
                    selectedItemFiltered = index;
                }
            }
        }
        else
        {
            errMsg.setText(tr("Select one work label in order to assign its course and employee."));
            errMsg.exec();
            return;
        }

        QList<QStringList> courseContent;
        QList<QStringList> employeeContent;
        QString courseID = QString();
        QString employeeID = QString();
        if(mCourseList.count() < 1)
        {
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mCourseList = mXmlParser.XmlParser_LoadCourseData();
            }
            else
            {
                mCourseList = mDbParser.DbParser_LoadCourseData();
            }
        }
        if(mEmployeeList.count() < 1)
        {
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mEmployeeList = mXmlParser.XmlParser_LoadEmployeeData();
            }
            else
            {
                mEmployeeList = mDbParser.DbParser_LoadEmployeeData();
            }
        }
        for(int index = 0; index < mCourseList.count(); index++)
        {
            courseContent.append(mCourseList.at(index)->Course_GetAll());
        }
        for(int index = 0; index < mEmployeeList.count(); index++)
        {
            employeeContent.append(mEmployeeList.at(index)->Employee_GetAll());
        }
        dialogWindow.DialogWindow_SetCourseContent(courseContent);
        dialogWindow.DialogWindow_SetEmployeeContent(employeeContent);
        if(mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetCourse() != nullptr)
        {
            courseID = mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetCourse()->Course_GetCourseID();
        }
        if(mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetEmployee() != nullptr)
        {
            employeeID = mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetEmployee()->Employee_GetEmployeeID();
        }
        dialogWindow.DialogWindow_DrawCourseContent(courseID);
        dialogWindow.DialogWindow_DrawEmployeeContent(employeeID);
        if(dialogWindow.exec() == QDialog::Accepted)
        {
            QStringList results;
            results = dialogWindow.DialogWindow_GetResults(WORKLABEL_MANAGEMENT);
            if(results.count() == 2)
            {
                mWorkLabelList.at(selectedItemFiltered)->WorkLabel_SetCourse(mCourseList.at(results.at(0).toInt()));
                mWorkLabelList.at(selectedItemFiltered)->WorkLabel_SetEmployee(mEmployeeList.at(results.at(1).toInt()));
                if(mUserInterface->actionLocal_database->isChecked() == true)
                {
                    QString workLabelID = mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetWorkLabelID();
                    QString courseID = mCourseList.at(results.at(0).toInt())->Course_GetCourseID();
                    QString employeeID = mEmployeeList.at(results.at(1).toInt())->Employee_GetEmployeeID();
                    mDbParser.DbParser_UpdateWorkLabel(workLabelID, courseID, employeeID);
                }
            }
            else
            {
                mWorkLabelList.at(selectedItemFiltered)->WorkLabel_SetCourse(nullptr);
                mWorkLabelList.at(selectedItemFiltered)->WorkLabel_SetEmployee(nullptr);
                if(mUserInterface->actionLocal_database->isChecked() == true)
                {
                    QString workLabelID = mWorkLabelList.at(selectedItemFiltered)->WorkLabel_GetWorkLabelID();
                    mDbParser.DbParser_UpdateWorkLabel(workLabelID, QString("null"), QString("null"));
                }
            }

            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
            }
        }
    }
    if(source == EMPLOYEE_MANAGEMENT)
    {
        QMessageBox workPointsMsg;
        workPointsMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
        workPointsMsg.setWindowTitle(tr("Total work points"));
        QModelIndexList selectedList = mUserInterface->employeeTable_widget->selectionModel()->selectedRows();
        if(selectedList.count() == 1) selectedItem = selectedList.first().row();
        else
        {
            errMsg.setText(tr("Select one employee in order to calculate their work points."));
            errMsg.exec();
            return;
        }

        if(mWorkLabelList.count() < 1)
        {
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mWorkLabelList = mXmlParser.XmlParser_LoadWorkLabelData();
            }
            else
            {
                mWorkLabelList = mDbParser.DbParser_LoadWorkLabelData();
            }
        }

        workPointsMsg.setText(QString(tr("Total work points of %1: %2"))
             .arg(mEmployeeList.at(selectedItem)->Employee_GetTitledName())
             .arg(mEmployeeList.at(selectedItem)->Employee_GetWorkPoints(mWorkLabelList)));
        workPointsMsg.exec();
    }
}

QList<int> MainWindow::MainWindow_StratifyStudents(int enlistedStudents, int capacity)
{
    QList<int> resultList = QList<int> ();
    int integralNumber = 0;
    int newLabels = 0;
    int students = 0;
    int remainder = 0;

    if(capacity > 0)
    {
        integralNumber = (enlistedStudents / capacity);
        if(enlistedStudents <= capacity)
        {
            newLabels = 1;
            students = enlistedStudents;
        }
        else
        {
            remainder = (enlistedStudents - (integralNumber * capacity));
            if(remainder == 0)
            {
                newLabels = integralNumber;
                students = capacity;
            }
            else
            {
                newLabels = integralNumber + 1;
                students = (enlistedStudents / newLabels);
                remainder = (enlistedStudents % newLabels);
            }
        }
    }
    else return QList<int> ({0});

    for(int index = 0; index < newLabels; index++) resultList.append(students);
    for(int index = 0; index < remainder; index++) resultList[index % newLabels] = (resultList.at(index % newLabels) + 1);

    return resultList;
}

void MainWindow::MainWindow_AutogenerateLabels(int selectedItem, QStringList results)
{
    QList<int> formedGroups;
    QStringList workLabelEntry;
    QString workLabelName;
    int listedStudents = 0;
    int capacity = 0;
    if(mWorkLabelList.count() < 1)
    {
        if(mUserInterface->actionXML_files->isChecked() == true)
        {
            mWorkLabelList = mXmlParser.XmlParser_LoadWorkLabelData();
        }
        else
        {
            mWorkLabelList = mDbParser.DbParser_LoadWorkLabelData();
        }
    }
    if(QString::compare(mCourseList.at(selectedItem)->Course_GetSemesterType(true), "1", Qt::CaseSensitive) == 0)
    {
        for(int index = 0; index < results.count(); index++)
        {
            listedStudents += mGroupList.at(results.at(index).toInt())->Group_GetSummerStudentCount();
        }
    }
    else
    {
        for(int index = 0; index < results.count(); index++)
        {
            listedStudents += mGroupList.at(results.at(index).toInt())->Group_GetWinterStudentCount();
        }
    }

    QRegularExpression courseShortcut(mCourseList.at(selectedItem)->Course_GetCourseShortcut() + " \\[");
    QList<int> positions = QList<int> ();
    for(int index = 0; index < mWorkLabelList.count(); index++)
    {
        if(courseShortcut.match(mWorkLabelList.at(index)->WorkLabel_GetLabelName()).hasMatch() == true) positions << index;
    }
    if(positions.count() > 0)
    {
        for(int index = positions.count() - 1; index >= 0; index--) mWorkLabelList.removeAt(positions.at(index));
    }
    if(mUserInterface->actionLocal_database->isChecked() == true)
    {
        mDbParser.DbParser_DeleteWorkLabelsByShortcut(mCourseList.at(selectedItem)->Course_GetCourseShortcut() + " [");
    }

    CourseType ct;
    if(QString::compare(mCourseList.at(selectedItem)->Course_GetLanguage(true), "1", Qt::CaseSensitive) == 0) ct = CourseType::eStandard;
    else ct = CourseType::eForeignLanguage;

    capacity = mCourseList.at(selectedItem)->Course_GetLectureCapacity();
    formedGroups = MainWindow_StratifyStudents(listedStudents, capacity);
    if(formedGroups.first() != 0)
    {
        for(int index = 0; index < formedGroups.count(); index++)
        {
            workLabelName = QString("%1 [L] %2").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut()).arg(index + 1);

            float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(mCourseList.at(selectedItem)->Course_GetLectureHours(),
                  0, (static_cast<int>(ct) - 1));

            workLabelEntry << workLabelName << QString::number(mCourseList.at(selectedItem)->Course_GetWeekCount())
                 << QString::number(mCourseList.at(selectedItem)->Course_GetLectureHours()) << QString::number(formedGroups.at(index))
                 << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eLecture)) << QString::number(static_cast<int>(ct));

            mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
                   workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
                   LabelType::eLecture, ct, nullptr, mCourseList.at(selectedItem))));
            workLabelEntry.clear();

            if(mUserInterface->actionLocal_database->isChecked() == true)
            {
                mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
            }
        }
    }

    capacity = mCourseList.at(selectedItem)->Course_GetPracticeCapacity();
    formedGroups = MainWindow_StratifyStudents(listedStudents, capacity);
    if(formedGroups.first() != 0)
    {
        for(int index = 0; index < formedGroups.count(); index++)
        {
            workLabelName = QString("%1 [P] %2").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut()).arg(index + 1);

            float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(mCourseList.at(selectedItem)->Course_GetPracticeHours(),
                  1, (static_cast<int>(ct) - 1));

            workLabelEntry << workLabelName << QString::number(mCourseList.at(selectedItem)->Course_GetWeekCount())
                 << QString::number(mCourseList.at(selectedItem)->Course_GetPracticeHours()) << QString::number(formedGroups.at(index))
                 << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::ePractice)) << QString::number(static_cast<int>(ct));

            mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
                   workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
                   LabelType::ePractice, ct, nullptr, mCourseList.at(selectedItem))));
            workLabelEntry.clear();

            if(mUserInterface->actionLocal_database->isChecked() == true)
            {
                mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
            }
        }
    }

    capacity = mCourseList.at(selectedItem)->Course_GetSeminarCapacity();
    formedGroups = MainWindow_StratifyStudents(listedStudents, capacity);
    if(formedGroups.first() != 0)
    {
        for(int index = 0; index < formedGroups.count(); index++)
        {
            workLabelName = QString("%1 [S] %2").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut()).arg(index + 1);

            float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(mCourseList.at(selectedItem)->Course_GetSeminarHours(),
                  2, (static_cast<int>(ct) - 1));

            workLabelEntry << workLabelName << QString::number(mCourseList.at(selectedItem)->Course_GetWeekCount())
                 << QString::number(mCourseList.at(selectedItem)->Course_GetSeminarHours()) << QString::number(formedGroups.at(index))
                 << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eSeminar)) << QString::number(static_cast<int>(ct));

            mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
                   workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
                   LabelType::eSeminar, ct, nullptr, mCourseList.at(selectedItem))));
            workLabelEntry.clear();

            if(mUserInterface->actionLocal_database->isChecked() == true)
            {
                mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
            }
        }
    }

    capacity = mCourseList.at(selectedItem)->Course_GetAtelierCapacity();
    formedGroups = MainWindow_StratifyStudents(listedStudents, capacity);
    if(formedGroups.first() != 0)
    {
        for(int index = 0; index < formedGroups.count(); index++)
        {
            workLabelName = QString("%1 [A] %2").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut()).arg(index + 1);

            float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(mCourseList.at(selectedItem)->Course_GetAtelierHours(),
                  3, (static_cast<int>(ct) - 1));

            workLabelEntry << workLabelName << QString::number(mCourseList.at(selectedItem)->Course_GetWeekCount())
                 << QString::number(mCourseList.at(selectedItem)->Course_GetAtelierHours()) << QString::number(formedGroups.at(index))
                 << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eAtelier)) << QString::number(static_cast<int>(ct));

            mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
                   workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
                   LabelType::eAtelier, ct, nullptr, mCourseList.at(selectedItem))));
            workLabelEntry.clear();

            if(mUserInterface->actionLocal_database->isChecked() == true)
            {
                mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
            }
        }
    }
    formedGroups.clear();

    int endingType = mCourseList.at(selectedItem)->Course_GetEnding(true).toInt();
    /*All students per exam label: --------------------------*/

    if(endingType == 1 || endingType == 4)
    {
        workLabelName = QString("%1 [CT]").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut());

        float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(0, 4, (static_cast<int>(ct) - 1));

        workLabelEntry << workLabelName << QString::number(1) << QString::number(1) << QString::number(listedStudents)
             << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eZ)) << QString::number(static_cast<int>(ct));

        mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
               workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
               LabelType::eZ, ct, nullptr, mCourseList.at(selectedItem))));
        workLabelEntry.clear();

        if(mUserInterface->actionLocal_database->isChecked() == true)
        {
            mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
        }
    }
    if(endingType == 2)
    {
        workLabelName = QString("%1 [CCT]").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut());

        float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(0, 5, (static_cast<int>(ct) - 1));

        workLabelEntry << workLabelName << QString::number(1) << QString::number(1) << QString::number(listedStudents)
             << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eKZ)) << QString::number(static_cast<int>(ct));

        mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
               workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
               LabelType::eKZ, ct, nullptr, mCourseList.at(selectedItem))));
        workLabelEntry.clear();

        if(mUserInterface->actionLocal_database->isChecked() == true)
        {
            mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
        }
    }
    if(endingType == 3 || endingType == 4)
    {
        workLabelName = QString("%1 [EX]").arg(mCourseList.at(selectedItem)->Course_GetCourseShortcut());

        float workPoints = mWorkPoint.WorkPoint_CalculateWorkPoints(0, 6, (static_cast<int>(ct) - 1));

        workLabelEntry << workLabelName << QString::number(1) << QString::number(1) << QString::number(listedStudents)
             << QString::number(workPoints) << QString::number(static_cast<int>(LabelType::eZk)) << QString::number(static_cast<int>(ct));

        mWorkLabelList.append(QSharedPointer<WorkLabel> (new WorkLabel(mHasher.Hasher_CalculateHash(workLabelEntry.join("")),
               workLabelName, workLabelEntry.at(1).toInt(), workLabelEntry.at(2).toInt(), workLabelEntry.at(3).toInt(), workPoints,
               LabelType::eZk, ct, nullptr, mCourseList.at(selectedItem))));
        workLabelEntry.clear();

        if(mUserInterface->actionLocal_database->isChecked() == true)
        {
            mDbParser.DbParser_InsertWorkLabel(mWorkLabelList.last());
        }
    }

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
    }
}

void MainWindow::MainWindow_ProcessLabels(unsigned int source)
{
    if(source == CLEAR_LABELS)
    {
        for(int index = 0; index < mWorkLabelList.count(); index++)
        {
            if(mWorkLabelList.at(index)->WorkLabel_GetEmployee() != nullptr && mWorkLabelList.at(index)->WorkLabel_GetCourse() == nullptr)
            {
                mWorkLabelList.at(index)->WorkLabel_SetEmployee(nullptr);
                if(mUserInterface->actionLocal_database->isChecked() == true)
                {
                    mDbParser.DbParser_UpdateWorkLabel(mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID(), QString("null"), QString("null"));
                }
            }
        }
        if(mUserInterface->actionXML_files->isChecked() == true)
        {
            mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
        }
    }
    if(source == ASSIGN_LABELS)
    {
        QMessageBox errMsg;
        errMsg.setIcon(QMessageBox::Information);
        errMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
        errMsg.setWindowTitle(tr("Assignment error"));
        if(mEmployeeList.count() < 1)
        {
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mEmployeeList = mXmlParser.XmlParser_LoadEmployeeData();
            }
            else
            {
                mEmployeeList = mDbParser.DbParser_LoadEmployeeData();
            }
        }

        if(mEmployeeList.count() < 1)
        {
            errMsg.setText(tr("There are no employees to be assigned."));
            errMsg.exec();
        }
        else
        {
            for(int label = 0; label < mWorkLabelList.count(); label++)
            {
                if(mWorkLabelList.at(label)->WorkLabel_GetEmployee() == nullptr && mWorkLabelList.at(label)->WorkLabel_GetCourse() != nullptr)
                {
                    float employeePoints = 0.0f;
                    float smallest = FLT_MAX;
                    int position = 0;
                    for(int employee = 0; employee < mEmployeeList.count(); employee++)
                    {
                        employeePoints = mEmployeeList.at(employee)->Employee_GetWorkPoints(mWorkLabelList);
                        if(employeePoints < smallest)
                        {
                            smallest = employeePoints;
                            position = employee;
                        }
                    }
                    mWorkLabelList.at(label)->WorkLabel_SetEmployee(mEmployeeList.at(position));
                    if(mUserInterface->actionLocal_database->isChecked() == true)
                    {
                        QString courseID = mWorkLabelList.at(label)->WorkLabel_GetCourse()->Course_GetCourseID();
                        QString employeeID = mEmployeeList.at(position)->Employee_GetEmployeeID();
                        mDbParser.DbParser_UpdateWorkLabel(mWorkLabelList.at(label)->WorkLabel_GetWorkLabelID(), courseID, employeeID);
                    }
                }
            }
            if(mUserInterface->actionXML_files->isChecked() == true)
            {
                mXmlParser.XmlParser_SaveWorkLabelData(mWorkLabelList);
            }

            errMsg.setText(tr("Work labels previously unassigned to employees were distributed automatically."));
            if(errMsg.exec() == QMessageBox::Ok)
            {
                MainWindow_EnableWorkLabels();
            }
        }
    }
}

void MainWindow::MainWindow_FilterLabels(int filterID)
{
    mWorkLabelListFilter.clear();
    if(filterID == 0)
    {
        mUserInterface->autoAssignWorkLabelButton->setEnabled(true);
        mUserInterface->newWorkLabelButton->setEnabled(true);
        for(int index = 0; index < mWorkLabelList.count(); index++)
        {
            mWorkLabelListFilter << mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID();
        }
    }
    if(filterID == 1)
    {
        mUserInterface->autoAssignWorkLabelButton->setEnabled(false);
        mUserInterface->newWorkLabelButton->setEnabled(true);
        for(int index = 0; index < mWorkLabelList.count(); index++)
        {
            if(mWorkLabelList.at(index)->WorkLabel_GetEmployee() == nullptr && mWorkLabelList.at(index)->WorkLabel_GetCourse() == nullptr)
            {
                mWorkLabelListFilter << mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID();
            }
        }
    }
    if(filterID == 2)
    {
        mUserInterface->autoAssignWorkLabelButton->setEnabled(true);
        mUserInterface->newWorkLabelButton->setEnabled(false);
        for(int index = 0; index < mWorkLabelList.count(); index++)
        {
            if(mWorkLabelList.at(index)->WorkLabel_GetEmployee() == nullptr && mWorkLabelList.at(index)->WorkLabel_GetCourse() != nullptr)
            {
                mWorkLabelListFilter << mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID();
            }
        }
    }
    if(filterID == 3)
    {
        mUserInterface->autoAssignWorkLabelButton->setEnabled(false);
        mUserInterface->newWorkLabelButton->setEnabled(false);
        for(int index = 0; index < mWorkLabelList.count(); index++)
        {
            if(mWorkLabelList.at(index)->WorkLabel_GetEmployee() != nullptr && mWorkLabelList.at(index)->WorkLabel_GetCourse() != nullptr)
            {
                mWorkLabelListFilter << mWorkLabelList.at(index)->WorkLabel_GetWorkLabelID();
            }
        }
    }
    MainWindow_DrawTableContent(WORKLABEL_TABLE_ID);
    MainWindow_ResizeTables(WORKLABEL_TABLE_ID);
}

void MainWindow::MainWindow_EnableCourses()
{
    mUserInterface->home_widget->hide();
    mUserInterface->employee_widget->hide();
    mUserInterface->workLabel_widget->hide();
    mUserInterface->group_widget->hide();
    mUserInterface->course_widget->show();

    mUserInterface->actionCourses->setChecked(true);
    mUserInterface->actionEmployees->setChecked(false);
    mUserInterface->actionWorkLabels->setChecked(false);
    mUserInterface->actionGroups->setChecked(false);

    mEditingEnabled = false;
    mUpdatingEnabled = false;
    mUserInterface->newCourseButton->setText(tr("Add new course"));
    mUserInterface->removeCourseButton->show();
    mUserInterface->manageCourseGroupsButton->setEnabled(true);

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mCourseList = mXmlParser.XmlParser_LoadCourseData();
    }
    else
    {
        mCourseList = mDbParser.DbParser_LoadCourseData();
    }

    MainWindow_DrawTableContent(COURSE_TABLE_ID);
    MainWindow_ResizeTables(COURSE_TABLE_ID);
}

void MainWindow::MainWindow_EnableEmployees()
{
    mUserInterface->home_widget->hide();
    mUserInterface->course_widget->hide();
    mUserInterface->workLabel_widget->hide();
    mUserInterface->group_widget->hide();
    mUserInterface->employee_widget->show();

    mUserInterface->actionCourses->setChecked(false);
    mUserInterface->actionEmployees->setChecked(true);
    mUserInterface->actionWorkLabels->setChecked(false);
    mUserInterface->actionGroups->setChecked(false);

    mEditingEnabled = false;
    mUpdatingEnabled = false;
    mUserInterface->newEmployeeButton->setText(tr("Add new employee"));
    mUserInterface->removeEmployeeButton->show();

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mEmployeeList = mXmlParser.XmlParser_LoadEmployeeData();
    }
    else
    {
        mEmployeeList = mDbParser.DbParser_LoadEmployeeData();
    }

    MainWindow_DrawTableContent(EMPLOYEE_TABLE_ID);
    MainWindow_ResizeTables(EMPLOYEE_TABLE_ID);
}

void MainWindow::MainWindow_EnableWorkLabels()
{
    mUserInterface->home_widget->hide();
    mUserInterface->course_widget->hide();
    mUserInterface->employee_widget->hide();
    mUserInterface->group_widget->hide();
    mUserInterface->workLabel_widget->show();

    mUserInterface->actionCourses->setChecked(false);
    mUserInterface->actionEmployees->setChecked(false);
    mUserInterface->actionWorkLabels->setChecked(true);
    mUserInterface->actionGroups->setChecked(false);

    mEditingEnabled = false;
    mUpdatingEnabled = false;
    mUserInterface->newWorkLabelButton->setText(tr("Add new work label"));
    mUserInterface->removeWorkLabelButton->show();

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mWorkLabelList = mXmlParser.XmlParser_LoadWorkLabelData();
    }
    else
    {
        mWorkLabelList = mDbParser.DbParser_LoadWorkLabelData();
    }

    MainWindow_ProcessLabels(CLEAR_LABELS);

    if(mUserInterface->workLabelComboBox->currentIndex() != 0)
    {
        mUserInterface->workLabelComboBox->setCurrentIndex(0);
    }

    mUserInterface->workLabelTable_widget->clearSelection();

    MainWindow_FilterLabels(0);
    MainWindow_ResizeTables(WORKLABEL_TABLE_ID);
}

void MainWindow::MainWindow_EnableGroups()
{
    mUserInterface->home_widget->hide();
    mUserInterface->course_widget->hide();
    mUserInterface->employee_widget->hide();
    mUserInterface->workLabel_widget->hide();
    mUserInterface->group_widget->show();

    mUserInterface->actionCourses->setChecked(false);
    mUserInterface->actionEmployees->setChecked(false);
    mUserInterface->actionWorkLabels->setChecked(false);
    mUserInterface->actionGroups->setChecked(true);

    mEditingEnabled = false;
    mUpdatingEnabled = false;
    mUserInterface->newGroupButton->show();
    mUserInterface->newGroupButton->setText(tr("Add new group"));
    mUserInterface->updateGroupButton->show();
    mUserInterface->updateGroupButton->setText(tr("Update selected group"));
    mUserInterface->removeGroupButton->show();

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mGroupList = mXmlParser.XmlParser_LoadGroupData();
    }
    else
    {
        mGroupList = mDbParser.DbParser_LoadGroupData();
    }

    MainWindow_DrawTableContent(GROUP_TABLE_ID);
    MainWindow_ResizeTables(GROUP_TABLE_ID);
}

void MainWindow::MainWindow_EnableNotifications()
{
    bool connectionStatus = true;
    QTcpSocket socket(this);
    socket.connectToHost("smtp.gmail.com", 465);
    connectionStatus = socket.waitForConnected(1200);
    if(connectionStatus == false) socket.abort();
    socket.close();

    NotificationWindow notificationWindow(this, connectionStatus);
    notificationWindow.setModal(true);
    notificationWindow.setWindowTitle("Education Manager");
    notificationWindow.setFixedWidth(600);
    notificationWindow.setMaximumWidth(800);
    notificationWindow.setMaximumHeight(600);
    notificationWindow.setMinimumWidth(600);
    notificationWindow.setMinimumHeight(480);
    QList<QStringList> employeeData;
    QList<float> workPointsData;

    if(mUserInterface->actionXML_files->isChecked() == true)
    {
        mEmployeeList = mXmlParser.XmlParser_LoadEmployeeData();
        mWorkLabelList = mXmlParser.XmlParser_LoadWorkLabelData();
    }
    else
    {
        mEmployeeList = mDbParser.DbParser_LoadEmployeeData();
        mWorkLabelList = mDbParser.DbParser_LoadWorkLabelData();
    }

    for(int index = 0; index < mEmployeeList.count(); index++)
    {
        employeeData.append(mEmployeeList.at(index)->Employee_GetAll());
        workPointsData.append(mEmployeeList.at(index)->Employee_GetWorkPoints(mWorkLabelList));
    }

    notificationWindow.NotificationWindow_SetEmployeeContent(employeeData);
    notificationWindow.NotificationWindow_SetWorkPointsContent(workPointsData);
    notificationWindow.NotificationWindow_DrawNotificationContent();

    if(notificationWindow.exec() == QDialog::Accepted)
    {
        QPair<QList<int>, bool> results;
        QString messageHeader;
        QString messageBody;
        QMessageBox infoMsg;
        infoMsg.setIcon(QMessageBox::Information);
        infoMsg.setWindowIcon(QIcon(":/images/EM_logo_round_tiny.ico"));
        infoMsg.setWindowTitle(tr("Notification status"));
        results = notificationWindow.NotificationWindow_GetResults();

        if(results.first.count() > 0)
        {
            QString senderAddress = "info.educationmgr@gmail.com";
            for(int index = 0; index < results.first.count(); index++)
            {
                Smtp* smtp = new Smtp(senderAddress, "EduManTB", "smtp.gmail.com", 465);
                connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));
                messageHeader = QString(tr("Education Manager Status Report"));
                messageBody = QString(tr("Dear recipient, Your working schedule is currently worth %1 work points. "
                                         "Please don't respond to this message, it was generated automatically.")
                                      .arg(QString::number(workPointsData.at(results.first.at(index)))));

                if(results.second == true)
                {
                    QString filePath;
                    filePath = mXmlParser.XmlParser_SaveEmployeeStatus(mWorkLabelList, employeeData.at(results.first.at(index)).at(3));
                    if(QString::compare(filePath, QString(""), Qt::CaseSensitive) != 0)
                    {
                        smtp->sendMail(senderAddress, employeeData.at(results.first.at(index)).at(3), messageHeader, messageBody, QStringList({filePath}));
                    }
                }
                else
                {
                    smtp->sendMail(senderAddress, employeeData.at(results.first.at(index)).at(3), messageHeader, messageBody);
                }
            }
            infoMsg.setText("Notification e-mail was sent successfully.");
            infoMsg.exec();
        }
    }
}

void MainWindow::MainWindow_EnableAbout()
{
    AboutWindow aboutWindow(this);
    aboutWindow.setModal(true);
    aboutWindow.setWindowTitle("Education Manager");
    aboutWindow.setFixedSize(580, 360);
    aboutWindow.exec();
}

void MainWindow::MainWindow_UsingXmlFiles()
{
    QSettings applicationSettings;
    applicationSettings.setValue("general/source", "xml");

    mUserInterface->actionXML_files->setChecked(true);
    mUserInterface->actionLocal_database->setChecked(false);

    if(mUserInterface->actionCourses->isChecked() == true) MainWindow_EnableCourses();
    else if(mUserInterface->actionEmployees->isChecked() == true) MainWindow_EnableEmployees();
    else if(mUserInterface->actionWorkLabels->isChecked() == true) MainWindow_EnableWorkLabels();
    else if(mUserInterface->actionGroups->isChecked() == true) MainWindow_EnableGroups();
}

void MainWindow::MainWindow_UsingDatabase()
{
    QSettings applicationSettings;
    applicationSettings.setValue("general/source", "dbo");

    mUserInterface->actionXML_files->setChecked(false);
    mUserInterface->actionLocal_database->setChecked(true);

    if(mUserInterface->actionCourses->isChecked() == true) MainWindow_EnableCourses();
    else if(mUserInterface->actionEmployees->isChecked() == true) MainWindow_EnableEmployees();
    else if(mUserInterface->actionWorkLabels->isChecked() == true) MainWindow_EnableWorkLabels();
    else if(mUserInterface->actionGroups->isChecked() == true) MainWindow_EnableGroups();
}

void MainWindow::MainWindow_TranslateIntoCzech()
{
    QSettings applicationSettings;
    applicationSettings.setValue("general/language", "cs");

    qApp->removeTranslator(mTranslator);
    mTranslator->load(":/languages/EducationManager_cs.qm");
    qApp->installTranslator(mTranslator);
    mUserInterface->retranslateUi(this);

    mUserInterface->actionEnglish->setChecked(false);
    mUserInterface->actionCzech->setChecked(true);
    MainWindow_InitializeUI();
}

void MainWindow::MainWindow_TranslateIntoEnglish()
{
    QSettings applicationSettings;
    applicationSettings.setValue("general/language", "en");

    qApp->removeTranslator(mTranslator);
    mTranslator->load(":/languages/EducationManager_en.qm");
    qApp->installTranslator(mTranslator);
    mUserInterface->retranslateUi(this);

    mUserInterface->actionEnglish->setChecked(true);
    mUserInterface->actionCzech->setChecked(false);
    MainWindow_InitializeUI();
}
