/**
 * @file        workpoint.cpp
 * @author      Tomas Bartosik
 * @date        27.03.2021
 * @brief       definition file for class WorkPoint
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/workpoint.hpp"

/*Class definition: ---------------------------------------------------------*/
WorkPoint::WorkPoint() : mLectureCZ(0.0f), mLectureEN(0.0f), mPracticeCZ(0.0f),
    mPracticeEN(0.0f), mSeminarCZ(0.0f), mSeminarEN(0.0f), mAtelierCZ(0.0f),
    mAtelierEN(0.0f), mCreditTestCZ(0.0f), mCreditTestEN(0.0f), mClassifiedCreditTestCZ(0.0f),
    mClassifiedCreditTestEN(0.0f), mExamCZ(0.0f), mExamEN(0.0f){}
WorkPoint::~WorkPoint(){}

bool WorkPoint::WorkPoint_SetWorkPoints(QStringList workPointsLoaded)
{
    QVariant numberFromString;

    for(int item = 0; item < workPointsLoaded.count(); item++)
    {
        numberFromString = QVariant(workPointsLoaded.at(item));
        if(!numberFromString.canConvert<float>())
        {
            return false;
        }
    }

    mLectureCZ = workPointsLoaded.at(0).toFloat();
    mLectureEN = workPointsLoaded.at(1).toFloat();
    mPracticeCZ = workPointsLoaded.at(2).toFloat();
    mPracticeEN = workPointsLoaded.at(3).toFloat();
    mSeminarCZ = workPointsLoaded.at(4).toFloat();
    mSeminarEN = workPointsLoaded.at(5).toFloat();
    mAtelierCZ = workPointsLoaded.at(6).toFloat();
    mAtelierEN = workPointsLoaded.at(7).toFloat();
    mCreditTestCZ = workPointsLoaded.at(8).toFloat();
    mCreditTestEN = workPointsLoaded.at(9).toFloat();
    mClassifiedCreditTestCZ = workPointsLoaded.at(10).toFloat();
    mClassifiedCreditTestEN = workPointsLoaded.at(11).toFloat();
    mExamCZ = workPointsLoaded.at(12).toFloat();
    mExamEN = workPointsLoaded.at(13).toFloat();

    return true;
}
QList<float> WorkPoint::WorkPoint_GetWorkPoints()
{
    return {mLectureCZ, mLectureEN, mPracticeCZ, mPracticeEN, mSeminarCZ, mSeminarEN,
    mAtelierCZ, mAtelierEN, mCreditTestCZ, mCreditTestEN, mClassifiedCreditTestCZ,
                mClassifiedCreditTestEN, mExamCZ, mExamEN};
}

float WorkPoint::WorkPoint_CalculateWorkPoints(int scheduledHours, int labelType, int courseType)
{
    /*Apply standard rules for doctoral students (not specified otherwise): -*/
    if(courseType == 2) courseType = 0;

    if(labelType == 0 && courseType == 0) return (mLectureCZ * scheduledHours);
    else if(labelType == 0 && courseType == 1) return (mLectureEN * scheduledHours);
    else if(labelType == 1 && courseType == 0) return (mPracticeCZ * scheduledHours);
    else if(labelType == 1 && courseType == 1) return (mPracticeEN * scheduledHours);
    else if(labelType == 2 && courseType == 0) return (mSeminarCZ * scheduledHours);
    else if(labelType == 2 && courseType == 1) return (mSeminarEN * scheduledHours);
    else if(labelType == 3 && courseType == 0) return (mAtelierCZ * scheduledHours);
    else if(labelType == 3 && courseType == 1) return (mAtelierEN * scheduledHours);
    else if(labelType == 4 && courseType == 0) return (mCreditTestCZ);
    else if(labelType == 4 && courseType == 1) return (mCreditTestEN);
    else if(labelType == 5 && courseType == 0) return (mClassifiedCreditTestCZ);
    else if(labelType == 5 && courseType == 1) return (mClassifiedCreditTestEN);
    else if(labelType == 6 && courseType == 0) return (mExamCZ);
    else if(labelType == 6 && courseType == 1) return (mExamEN);
    else return 0.0f;
}

