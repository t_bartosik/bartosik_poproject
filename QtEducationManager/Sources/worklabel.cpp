/**
 * @file        worklabel.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file of class WorkLabel
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/worklabel.hpp"

/*Class definition: ---------------------------------------------------------*/
WorkLabel::WorkLabel() : mID(QString()), mLabelName(QString()), mWeekCount(0), mScheduledHours(0),
    mStudentCount(0), mWorkPoints(0.0f), mLabelType(LabelType::eLecture),
    mCourseType(CourseType::eStandard), mEmployee(nullptr), mCourse(nullptr){}
WorkLabel::WorkLabel(QString ID, QString labelName, int weekCount, int scheduledHours, int studentCount,
                     float workPoints, LabelType labelType, CourseType courseType, QSharedPointer<Employee> employee,
                     QSharedPointer<Course> course) : mID(ID), mLabelName(labelName), mWeekCount(weekCount),
    mScheduledHours(scheduledHours), mStudentCount(studentCount), mWorkPoints(workPoints),
    mLabelType(labelType), mCourseType(courseType), mEmployee(employee), mCourse(course){}
WorkLabel::~WorkLabel(){}

QString& WorkLabel::WorkLabel_GetWorkLabelID() {return mID;}
QString& WorkLabel::WorkLabel_GetLabelName() {return mLabelName;}
int WorkLabel::WorkLabel_GetWeekCount() {return mWeekCount;}
int WorkLabel::WorkLabel_GetScheduledHours() {return mScheduledHours;}
int WorkLabel::WorkLabel_GetStudentCount() {return mStudentCount;}
float WorkLabel::WorkLabel_GetWorkPoints() {return mWorkPoints;}
QString WorkLabel::WorkLabel_GetLabelType(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mLabelType));

    switch(static_cast<int>(mLabelType))
    {
    case 1: return QObject::tr("lecture");
    case 2: return QObject::tr("practice");
    case 3: return QObject::tr("seminar");
    case 4: return QObject::tr("atelier");
    case 5: return QObject::tr("credit test");
    case 6: return QObject::tr("classified credit test");
    case 7: return QObject::tr("exam");
    default: return QString();
    }
}
QString WorkLabel::WorkLabel_GetCourseType(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mCourseType));

    switch(static_cast<int>(mCourseType))
    {
    case 1: return QObject::tr("standard course");
    case 2: return QObject::tr("foreign language");
    case 3: return QObject::tr("doctoral course");
    default: return QString();
    }
}
QSharedPointer<Employee> WorkLabel::WorkLabel_GetEmployee() {return mEmployee;}
QSharedPointer<Course> WorkLabel::WorkLabel_GetCourse() {return mCourse;}
QStringList WorkLabel::WorkLabel_GetAll()
{
    return {mID, mLabelName, QString::number(mWeekCount), QString::number(mScheduledHours),
    QString::number(mStudentCount), QString::number(mWorkPoints), WorkLabel_GetLabelType(false),
    WorkLabel_GetCourseType(false)};
}

void WorkLabel::WorkLabel_SetEmployee(QSharedPointer<Employee> employee) {mEmployee = employee;}
void WorkLabel::WorkLabel_SetCourse(QSharedPointer<Course> course) {mCourse = course;}
