/**
 * @file        course.cpp
 * @author      Tomas Bartosik
 * @date        05.03.2021
 * @brief       definition file for Course
 * **********************************************************************
 * @par       COPYRIGHT NOTICE (c) 2021 TBU in Zlin. All rights reserved.
 */

/*Private includes: ---------------------------------------------------------*/
#include "../Headers/course.hpp"

/*Class definition: ---------------------------------------------------------*/
Course::Course() : mID(QString()), mCourseShortcut(QString()), mCourseName(QString()), mLesson(),
    mSemester(), mLanguage(Language::eCZ), mGroups(QList<QSharedPointer<Group>> ()){}
Course::Course(QString ID, QString courseShortcut, QString courseName, Lesson lesson, Semester semester,
               Language language, QList<QSharedPointer<Group>> groups) : mID(ID), mCourseShortcut(courseShortcut),
    mCourseName(courseName), mLesson(lesson), mSemester(semester),
    mLanguage(language), mGroups(groups){}
Course::~Course(){}

QString& Course::Course_GetCourseID() {return mID;}
QString& Course::Course_GetCourseShortcut() {return mCourseShortcut;}
QString& Course::Course_GetCourseName() {return mCourseName;}
int Course::Course_GetLectureCapacity() {return mLesson.Lesson_GetLectureCapacity();}
int Course::Course_GetPracticeCapacity() {return mLesson.Lesson_GetPracticeCapacity();}
int Course::Course_GetSeminarCapacity() {return mLesson.Lesson_GetSeminarCapacity();}
int Course::Course_GetAtelierCapacity() {return mLesson.Lesson_GetAtelierCapacity();}
int Course::Course_GetLectureHours() {return mSemester.Semester_GetLectureHours();}
int Course::Course_GetPracticeHours() {return mSemester.Semester_GetPracticeHours();}
int Course::Course_GetSeminarHours() {return mSemester.Semester_GetSeminarHours();}
int Course::Course_GetAtelierHours() {return mSemester.Semester_GetAtelierHours();}
int Course::Course_GetWeekCount() {return mSemester.Semester_GetWeekCount();}
int Course::Course_GetCredits() {return mSemester.Semester_GetCredits();}
QString Course::Course_GetSemesterType(bool getByID) {return mSemester.Semester_GetSemesterType(getByID);}
QString Course::Course_GetEnding(bool getByID) {return mSemester.Semester_GetEnding(getByID);}
QString Course::Course_GetLanguage(bool getByID)
{
    if(getByID == true) return QString::number(static_cast<int>(mLanguage));

    switch(static_cast<int>(mLanguage))
    {
    case 1: return QObject::tr("CZ");
    case 2: return QObject::tr("EN");
    default: return QString();
    }
}
QList<QSharedPointer<Group>>& Course::Course_GetGroups() {return mGroups;}
QStringList Course::Course_GetAll()
{
    return {mID, mCourseShortcut, mCourseName, QString::number(mLesson.Lesson_GetLectureCapacity()),
    QString::number(mLesson.Lesson_GetPracticeCapacity()), QString::number(mLesson.Lesson_GetSeminarCapacity()),
    QString::number(mLesson.Lesson_GetAtelierCapacity()), QString::number(mSemester.Semester_GetLectureHours()),
    QString::number(mSemester.Semester_GetPracticeHours()), QString::number(mSemester.Semester_GetSeminarHours()),
    QString::number(mSemester.Semester_GetAtelierHours()), QString::number(mSemester.Semester_GetWeekCount()),
    QString::number(mSemester.Semester_GetCredits()), mSemester.Semester_GetSemesterType(false),
    mSemester.Semester_GetEnding(false), Course_GetLanguage(false)};
}

void Course::Course_ResetGroups() {mGroups.clear();}
void Course::Course_AppendGroup(QSharedPointer<Group> group) {mGroups.append(group);}
void Course::Course_ReplaceGroup(int index, QSharedPointer<Group> group) {mGroups.replace(index, group);}
