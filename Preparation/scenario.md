# Barto��k ~ EducationManager #

Univerz�ln� desktopov� aplikace pro usnadn�n� spr�vy fakultn�ch �tvar�, studijn�ch obor�, p�edm�t�, zam�stnanc� a student�.

### T�ma a popis projektu ###

* Tvorba desktopov� aplikace pro spr�vu existuj�c�ch a tvorbu nov�ch z�znam� popisuj�c� fakultn� �tvary.
* Aplikace umo�n� p�idat nov� zam�stnance, kte�� jsou v�zna�n� sv�m jm�nem, tituly a kontaktn�mi �daji. Zam�stnanci jsou v�z�ni ke konkr�tn�mu existuj�c�mu �tvaru fakulty.
* Mo�nost za�len�n� nov�ch studijn�ch obor�, kter� jsou reprezentov�ny zkratkou (nap�. **Softwarov� in�en�rstv�** -> **SWI**).
* Mo�nost definice nov�ch u�ebn�ch p�edm�t� - ty jsou charakteristick� mimo jin� sv�m vyu�uj�c�m, rozsahem hodin v obdob� a maxim�ln�m po�tem student�.
* Aplikace ukl�d� data do lok�ln� datab�ze a do XML soubor�.
* *dal�� k dopln�n� dle postupu zpracov�n� aplikace*

### N�vrh vlastn�ho sc�n��e pou�it� aplikace ###

* Prim�rn�m u�ivatelem je *tajemn�k*, ten vyu��v� aplikaci pro prohl�en� sou�asn� platn�ch dat a k jejich �prav�.
* U�ivatel si vyb�r� na��t�n� dat z datab�ze nebo ze souboru XML.
* U�ivatel m��e vkl�dat nov� z�znamy fakultn�ch �tvar�, studijn�ch obor�, p�edm�t�, zam�stnanc� a student�, p�i�em� uv�d� v�echny povinn� �daje.
* U�ivatel si m��e upravit atributy existuj�c�ch z�znam� �tvar�, obor�, p�edm�t�, zam�stnanc� i student�. Aplikace ov��uje spr�vnost zadan�ch �daj� dle po�adavk�, u�ivatele upozorn� v p��pad� chybn�ho form�tu.
* U�ivatel m��e smazat z�znamy charakteristick� pro popis �tvar�, obor�, p�edm�t�, zam�stnatnc� a student�.
* Aplikace p�i b�hu pracuje s kopi� datab�ze, kter� je ukl�d�na p�i zav�en� aplikace. Stejn� i soubory XML jsou uchov�v�ny v kopii.

### N�vrh pou�it�ch technologi� ###

* Desktopov� aplikace, platforma Windows
* Z�m�r tvorby v Windows Forms/WPF
* Vyu�it� SQLite nebo MS SQL Serveru

### Ot�zky ###

* M� u�ivatel aplikace p��stup k informac�m studenta, tj. jsou informace nap�. o jm�nu studenta a kontaktn�ch �daj�ch vedeny v datab�zi?
* M� u�ivatel mo�nost prov�d�t zm�ny v z�znamech kdykoliv, nebo jen nap�. p�ed za��tkem nov�ho obdob� (semestru)?
* Zaznamen�v� aplikace napln�n� pracovn� kapacity zam�stnance i mimo pedagogickou �innost, tj. tv�r�� a administrativn� �innost?
* Je sou��st� funkcionality aplikace i autmatizovan� rozd�len� term�n� v�uky p�edm�t� b�hem t�dne, tj. tvorba rozvrhu?



