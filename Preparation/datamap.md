# EducationManager Datamap #




### Model p�edm�tu   <**Course**> ###

* zkratka p�edm�tu <*String*>
* n�zev p�edm�tu <*String*>
* jazyk <*enum(CZ, EN)*>
* kapacita student� <*Lesson*>
* semestr <*Semester*>
* seznam skupinek <*List<Group>*>



### Model zam�stnance   <**Employee**> ###

* jm�no s tituly <*String*>
* popis funkce <*String*>
* je doktorand? <*bool*>
* e-mail <*String*>
* mobil <*String*>
* kancel�� <*Workroom*>
* �vazky <*List<WorkLabel>*>



### Model pracovn�ho �t�tku (�vazkov� list)   <**WorkLabel**> ###

* n�zev �t�tku <*String*>
* typ �t�tku <*enum(p�edn�ka, cvi�en�, semin��, ateli�r, z�po�et, klasifikovan� z�po�et, zkou�ka)*>
* druh p�edm�tu <*enum(standardn�, ciz� jazyk, doktorsk�)*>
* t�dny <*int*>
* rozvrhov� hodiny <*int*>
* po�et student� <*int*>
* pracovn� body <*decimal*>
* zam�stnanec <*Employee, nullable*>
* p�edm�t <*Course, nullable*>



### Model skupinky   <**Group**> ###

* typ studia <*enum(Bc., Mgr., Ph. D.)*>
* forma studia <*enum(prezen�n�, kombinovan�)*>
* studijn� obor <*String*>
* ro�n�k <*Year*>
* jazyk <*enum(CZ, EN)*>



~~~


### Semestr   <**Semester**> ###

* hodin p�edn�ek <*int*>
* hodin cvi�en� <*int*>
* hodin semin��� <*int*>
* hodin ateli�ru <*int*>
* forma zakon�en� <*enum(kl, z, zk)*>
* po�et t�dn� <*int*>
* po�et kredit� <*int*>



### Kancel��   <**Workroom**> ###

* ozna�en� m�stnosti <*String*>
* telefon <*String*>



### V�uka   <**Lesson**> ###

* kapacita p�edn�ky <*int*>
* kapacita cvi�en� <*int*>
* kapacita semin��e <*int*>
* kapacita ateli�ru <*int*>



### Ro�n�k   <**Year**> ###

* ro�n�k <*int*>
* LS <*int*>
* ZS <*int*>
