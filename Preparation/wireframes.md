# EducationManager Wireframes #

(*grafick� zpracov�n� viz adres�� '**Wireframes**'*)


### Hlavn� menu ###

* Soubor -> p�echody mezi formul��i + mo�nost ukon�en� aplikace
* Nastaven� -> p�ep�n�n� mezi pou�it�m XML nebo lok�ln� DB



### Zobrazen� p�edm�tu   <**CourseViewer**> ###

-> p��stup z hlavn�ho menu

* naho�e p�ep�n�n� zobrazen� v�sledk� dle 'ZS' nebo 'LS' (dropdown menu)
* lze p�id�vat nov� ��dky po v�b�ru 'ZS' nebo 'LS' pomoc� tla��tka
* lze odstranit existuj�c� z�znamy po jejich v�b�ru my�� v tabulce pomoc� tla��tka



### P�id�n� p�edm�tu   <**CourseCreation**> ###

-> okno vyvol�no tla��tkem ve formul��i zobrazen� p�edm�tu (CourseViewer)

* zde volba (dropdown menu) p�id�n� nov�ho p�edm�tu pro ZS nebo LS



### Zobrazen� zam�stnance   <**EmployeeViewer**> ###

-> p��stup z hlavn�ho menu

* mo�nost filtrace z�znam� dle v�b�ru �stavu (Department) (dropdown menu)
* tla��tko pro p�id�n� nov�ho zam�stnance
* zde tak� funkce smaz�n� vybran�ho zam�stnance



### P�id�n� zam�stnance   <**EmployeeCreation**> ###

-> okno vyvol�no tla��tkem ve formul��i zobrazen� zam�stnance (EmployeeViewer)




### Zobrazen� pracovn�ho �t�tku (�vazk� zam�stnance)   <**WorkLabelViewer**> ###

-> p��stup z hlavn�ho menu

* v�b�r dle jm�na zam�stnance ze seznamu v�ech zam�stnanc� v�ech �stav� (dropdown menu)
* po v�b�ru zam�stnance je mo�n� vlo�it nov� z�znam -> tla��tkem vedouc� na formul�� (WorkLabelCreation)
* vybran� z�znam z tabulky �vazk� je mo�n� pomoc� tla��tka smazat



### P�id�n� pracovn�ho �t�tku (�vazku zam�stnance)   <**WorkLabelCreation**> ###

-> okno vyvol�no tla��tkem ve formul��i zobrazen� modelu skupinky (WorkLabelViewer)

* vyb�r� se propojen� existuj�c�ho zam�stnance s existuj�c�m profilem p�edm�tu
* zkratka p�edm�tu se dopln� automaticky



### Zobrazen� skupinky   <**GroupViewer**> ###

-> p��stup z hlavn�ho menu

* defaultn� se zobrazuj� v�echny ��dky, jde ale filtrovat zobrazen� dle typu studia, oboru a formy studia
* je mo�n� vlo�it nov� z�znam
* je mo�n� odstranit z�znam vybran�ho ��dku



### P�id�n� skupinky   <**GroupCreation**> ###

-> okno vyvol�no tla��tkem ve formul��i zobrazen� pracovn�ho �t�tku (GroupViewer)


